package com.addonis.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
/*
This class is responsible for how our Authority table has to look in the database.It
is connected with the User table and keeps the authority of the user.
 */
@Entity
@Table(name = "authorities", uniqueConstraints = @UniqueConstraint(
        columnNames = { "username", "authority" }))
@NoArgsConstructor
@Getter
@Setter
public class Authority implements Serializable {

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username", nullable = false)
    @JsonIgnore
    private User user;
    @Id
    @Column(name = "authority")
    private String authority;
}
