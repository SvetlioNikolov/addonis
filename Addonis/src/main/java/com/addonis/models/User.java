package com.addonis.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;
/*
This class is responsible for how our User table has to look in the database.
It contains the Id,name,enable information.It is connected with the UserDetails and Authority table.
 */
@Entity
@Table(name = "users")
@NoArgsConstructor
@Getter
@Setter
public class User {

    @Id
    @Size(max = 50)
    @Column(name = "username")
    private String username;

    @Size(max = 68)
    @Column(name = "password")
    private String password;

    @Column(name = "enabled")
    private boolean enabled;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "details_id")
    private UserDetails userDetails;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Authority> authorities = new HashSet<>();

    public boolean isAdmin(){
        for (Authority auth : getAuthorities()) {
            if (auth.getAuthority().equalsIgnoreCase("ROLE_ADMIN")) {
                return true;
            }
        }
        return false;
    }
}
