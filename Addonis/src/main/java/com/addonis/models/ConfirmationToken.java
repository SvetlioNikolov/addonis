package com.addonis.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;
/*
This class is responsible for how our Confirmation token table has to look in the database.
It contains the information we need about when the user tried to register..
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
public class ConfirmationToken {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="token_id")
    private long tokenid;

    @Column(name="confirmation_token")
    private String confirmationToken;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @OneToOne(targetEntity = UserDetails.class, fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(nullable = false, name = "details_id")
    private UserDetails userDetails;

    public ConfirmationToken(UserDetails userDetails) {
        this.userDetails = userDetails;
        createdDate = new Date();
        confirmationToken = UUID.randomUUID().toString();
    }

}
