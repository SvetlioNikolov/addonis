package com.addonis.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
/*
This class is responsible for how our GitHubData table has to look in the database.
It contains the Id and the name of the Ide.
 */
@Entity
@Table(name = "platforms")
@NoArgsConstructor
@Getter
@Setter
public class Ide {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ide_id")
    private int id;

    @Column(name = "name")
    private String name;

}
