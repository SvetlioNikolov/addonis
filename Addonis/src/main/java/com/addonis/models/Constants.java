package com.addonis.models;

public class Constants {

    public static final String ADDONS_TABLE_NAME = "addons";
    public static final String ADDONS_TABLE_ID_COLUMN_NAME = "id";
    public static final String ADDONS_TABLE_NAME_COLUMN = "name";
    public static final String ADDONS_TABLE_DESCRIPTION_COLUMN = "description";
    public static final String ADDONS_TABLE_DOWNLOAD_LINK_COLUMN = "download_link";
    public static final String ADDONS_TABLE_FILE_COLUMN = "addon_file";
    public static final String ADDONS_TABLE_FILE_EXTENSION_COLUMN = "file_extension";
    public static final String ADDONS_TABLE_ORIGIN_LINK_COLUMN = "origin_link";
    public static final String ADDONS_TABLE_DOWNLOADS_COLUMN = "downloads";
    public static final String ADDONS_TABLE_STATUS_ID_COLUMN = "status_id";
    public static final String ADDONS_TABLE_IDE_COLUMN = "ide_id";
    public static final String ADDONS_TABLE_IMAGE_COLUMN = "image";
    public static final String ADDONS_TABLE_GITHUB_DATA_ID_COLUMN = "github_data_id";
    public static final String ADDONS_TABLE_UPLOAD_DATE_COLUMN = "upload_date";
    public static final String ADDONS_TABLE_FEATURED_COLUMN = "featured";

    public static final String AUTHORITY_TABLE_USER_COLUMN = "username";
    public static final String AUTHORITY_ROLE_COLUMN = "authority";

    public static final String CONFIRMATION_TOKEN_ID_COLUMN = "token_id";
    public static final String CONFIRMATION_TOKEN_COLUMN = "confirmation_token";

    public static final String GITHUB_DATA_TABLE_NAME = "github_data";
    public static final String GITHUB_TABLE_ID_COLUMN = "github_data_id";
    public static final String GITHUB_DATA_TABLE_ISSUES_COLUMN = "issues";
    public static final String GITHUB_DATA_TABLE_LAST_COMMIT_TITLE = "lastCommitTitle";
    public static final String GITHUB_DATA_TABLE_LAST_COMMIT = "lastCommit";
    public static final String GITHUB_DATA_TABLE_PULLS_COLUMN = "pulls";
    public static final String GITHUB_DATA_TABLE_README_COLUMN = "readme";

    public static final String IDE_TABLE_NAME = "readme";
    public static final String IDE_TABLE_ID_COLUMN = "readme";
    public static final String IDE_TABLE_NAME_COLUMN = "readme";

    public static final String STATUS_TABLE_NAME = "statuses";
    public static final String STATUS_TABLE_ID_COLUMN = "status_id";
    public static final String STATUS_TABLE_NAME_COLUMN = "name";

    public static final String TAG_TABLE_NAME = "tags";
    public static final String TAG_TABLE_ID_COLUMN = "tag_id";
    public static final String TAG_TABLE_NAME_COLUMN = "name";

    public static final String RATING_TABLE_NAME = "ratings";
    public static final String RATING_TABLE_ID_COLUMN = "rating_id";
    public static final String RATING_TABLE_USERNAME_COLUMN = "username";
    public static final String RATING_TABLE_ADDON_COLUMN = "addon_id";
    public static final String RATING_TABLE_VALUE_COLUMN = "value";

    public static final String USER_TABLE_NAME = "users";
    public static final String USER_TABLE_USERNAME_COLUMN = "username";
    public static final String USER_TABLE_ADDON_COLUMN = "password";
    public static final String USER_TABLE_VALUE_COLUMN = "enabled";
    public static final String USER_TABLE_DETAILS_ID_COLUMN = "details_id";
    public static final String USER_TABLE_USER_ROLE_COLUMN = "user";
    public static final String USER_TABLE_ROLE_ADMIN_COLUMN = "ROLE_ADMIN";

    public static final String USER_DETAILS_TABLE_NAME = "user_details";
    public static final String USER_DETAILS_TABLE_ID_COLUMN = "details_id";
    public static final String USER_DETAILS_TABLE_FIRST_NAME_COLUMN = "first_name";
    public static final String USER_DETAILS_TABLE_LAST_NAME_COLUMN = "first_name";
    public static final String USER_DETAILS_TABLE_EMAIL_COLUMN = "email";
    public static final String USER_DETAILS_TABLE_IMAGE_COLUMN = "image";


    public static final String STATUS_PENDING = "pending";
    public static final String STATUS_DELETED = "deleted";
    public static final String STATUS_APPROVED = "approved";

    public static final String TAG = "Tag";
    public static final String ADDON = "Addon";
    public static final String USER = "User";

    public static final String DATE_SPECIFICATION_MODEL = "yyyy/MM/dd HH:mm:ss";
    public static final String AUTH_TOKEN = "d5a79bf9d74b2b313b851d130e62f4ad408c912d";


    public static final String NOT_AUTHORIZED_EXCEPTION_MESSAGE = "You are not authorized.";


    public static final String USER_FIRST_NAME_LIMIT_SIZE = "First name should be between 2 and 25 symbols";
    public static final String USER_LAST_NAME_LIMIT_SIZE = "Last name should be between 2 and 25 symbols";
    public static final String USER_USERNAME_LIMIT_SIZE = "Username should be between 4 and 50 symbols";
    public static final String USER_EMAIL_LIMIT_SIZE = "Email should be between 8 and 25 symbols";
    public static final String USER_PASSWORD_LIMIT_SIZE = "Password should be between 4 and 68 symbols";


    public static final String ADDON_NAME_LIMIT_SIZE = "Name should be between 2 and 25 symbols";
    public static final String ADDON_DESCRIPTION_LIMIT_SIZE = "Description should be between 2 and 70 symbols";
    public static final String ADDON_TAGS_LIMIT_SIZE = "Tags should be between 2 and 30 symbols";


}
