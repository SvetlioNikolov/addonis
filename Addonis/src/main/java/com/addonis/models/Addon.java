package com.addonis.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
/*
This class is responsible for how our Addon table has to look in the database.It
is connected with Status,GitHubData,Ide and Tags tables. It keeps the Addon name,description,download link,image and download count.
 */
@Entity
@Table(name = "addons")
@NoArgsConstructor
@Getter
@Setter
public class Addon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Lob
    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "creator")
    private User creator;

    @Column(name = "download_link")
    private String download_link;

    @Lob
    @Column(name = "addon_file")
    private byte[] addon_file;

    @Column(name = "file_extension")
    private String file_extension;

    @Column(name = "origin_link")
    private String origin_link;

    @Column(name = "downloads")
    private int downloads = 0;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "addontags",
            joinColumns = @JoinColumn(name = "id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tags = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "ide_id")
    private Ide ide;

    @Lob
    @Column(name = "image")
    private String image;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "github_data_id")
    private GitHubData gitHubData;

    @Column(name = "upload_date")
    private String upload_date;

    @Column(name = "featured")
    private boolean featured;
}
