package com.addonis.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
/*
This class is responsible for how our GitHubData table has to look in the database.
It contains all the information we need from GitHub about the add-on which is issue count,last commit title, last commit date and pull count.
 */
@Entity
@Table(name = "github_data")
@NoArgsConstructor
@Getter
@Setter
public class GitHubData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "github_data_id")
    private int id;
    @Column(name = "issues")
    private int issues;
    @Column(name = "lastCommitTitle")
    private String lastCommitTitle;
    @Column(name = "lastCommit")
    private Date lastCommit;
    @Column(name = "pulls")
    private int pulls;
    @Lob
    @Column(name = "readme")
    private String readme;
}
