package com.addonis.models.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Required;

import javax.persistence.Lob;
import javax.validation.constraints.Size;
/*
This class is responsible for what information we need when creating a new addon. It shows the restrictions of the fields.
 */
@NoArgsConstructor
@Getter
@Setter
public class AddonDto {

    @Size(min = 4, max = 30, message = "Name should be between 4 and 30 symbols")
    private String name;
    @Size(min = 10, max = 500, message = "Description should be between 10 and 500 symbols")
    private String description;
    @Size(min = 2, max = 50, message = "Tags should be between 2 and 50 symbols")
    private String tags;
    private byte[] data;
    private String file_extension;
    private String origin;
    private String image;
    private String ide;
}
