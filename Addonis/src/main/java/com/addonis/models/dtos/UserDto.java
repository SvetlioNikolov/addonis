package com.addonis.models.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Lob;
import javax.validation.constraints.Size;
/*
This class is responsible for what information we need when creating a new user. It shows the restrictions of the fields.
 */
@NoArgsConstructor
@Getter
@Setter
public class UserDto {

    @Size(min = 2, max = 25, message = "First name should be between 2 and 25 symbols")
    private String first_name;
    @Size(min = 2, max = 25, message = "Last name should be between 2 and 25 symbols")
    private String last_name;
    @Size(min = 4, max = 50, message = "Username should be between 4 and 50 symbols")
    private String username;
    @Size(min = 4, max = 25, message = "Email should be between 8 and 25 symbols")
    private String email;
    @Size(min = 4, max = 68, message = "Password should be between 4 and 68 symbols")
    private String password;
    @Lob
    private String image;

    private String passwordConfirmation;

}
