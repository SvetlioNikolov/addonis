package com.addonis.models;

import com.addonis.exceptions.EntityNotFoundException;
import com.addonis.exceptions.FileUploadException;
import com.addonis.models.Addon;
import com.addonis.models.dtos.AddonDto;
import com.addonis.models.dtos.UserDto;
import com.addonis.models.User;
import com.addonis.models.UserDetails;
import com.addonis.repositories.IdesRepository;
import com.addonis.repositories.StatusRepository;
import com.addonis.repositories.TagsRepository;
import com.addonis.repositories.UserRepository;
import com.addonis.services.contracts.AddonsService;
import com.sun.org.apache.xpath.internal.operations.Mult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Base64;
import java.util.Objects;

/*
This class is responsible for the two way mapping between objects.
 It maps objects of type User/Addon to UserDto/AddonDto and reverse.
 It merges two objects of type User or Addon.

 */
@Component
public class Mapper {
    private AddonsService addonsService;
    private IdesRepository idesRepository;
    private StatusRepository statusRepository;
    private UserRepository userRepository;
    @Value("classpath:static/img/face.png")
    File face;
    @Value("classpath:static/img/defaultlogopng.png")
    File defaultAddon;


    @Autowired
    public Mapper(AddonsService addonsService, IdesRepository idesRepository, StatusRepository statusRepository, UserRepository userRepository) {
        this.idesRepository = idesRepository;
        this.statusRepository = statusRepository;
        this.userRepository = userRepository;
        this.addonsService = addonsService;
    }

    public UserDetails toUserDetails(UserDetails userDetails, UserDto dto) {


        try {
            FileInputStream fileInputStreamReader = new FileInputStream(face);
            byte[] bytes = new byte[(int) face.length()];
            fileInputStreamReader.read(bytes);
            String defaultImage = Base64.getEncoder().encodeToString(bytes);

            userDetails.setFirstName(dto.getFirst_name());
            userDetails.setLastName(dto.getLast_name());
            userDetails.setEmail(dto.getEmail());
            if ( dto.getImage() != null && !dto.getImage().isEmpty()) {
                userDetails.setImage(dto.getImage());
            }else{
                userDetails.setImage(defaultImage);
            }

            return userDetails;
        }catch (IOException e){
            throw new FileUploadException();
        }
    }

    public UserDto toUserDTO(User user) {

        UserDto userDTO = new UserDto();
        userDTO.setUsername(user.getUsername());
        userDTO.setFirst_name(user.getUserDetails().getFirstName());
        userDTO.setLast_name(user.getUserDetails().getLastName());
        userDTO.setEmail(user.getUserDetails().getEmail());

        return userDTO;
    }

    public User toUser(UserDto userDTO) {
        User user = new User();
        UserDetails userDetails = toUserDetails(new UserDetails(), userDTO);
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        user.setUserDetails(userDetails);
        user.setEnabled(false);
        return user;
    }

    public AddonDto toAddonDTO(Addon addon) {
        AddonDto addonDTO = new AddonDto();
        addonDTO.setData(addon.getAddon_file());
        addonDTO.setName(addon.getName());
        addonDTO.setDescription(addon.getDescription());
        addonDTO.setIde(addon.getIde().getName());
        addonDTO.setTags(addon.getTags().toString().replaceAll("\\[", "").replaceAll("]", "").replaceAll(",", ""));
        addonDTO.setOrigin(addon.getOrigin_link());
        addonDTO.setImage(addon.getImage());
        return addonDTO;
    }

    public Addon toAddon(AddonDto addonDTO, String username, MultipartFile image, MultipartFile file) {
        Addon addon = new Addon();


        //TODO classpath
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(defaultAddon);
            byte[] bytes = new byte[(int)defaultAddon.length()];
            fileInputStreamReader.read(bytes);
            String defaultImage = Base64.getEncoder().encodeToString(bytes);
            if (!file.isEmpty()) {
                String extension = file.getOriginalFilename();
                assert extension != null;
                String[] fileContentType = extension.split("\\.");
                addon.setAddon_file(file.getBytes());
                addon.setFile_extension(fileContentType[fileContentType.length-1]);
            }
            if (!image.isEmpty()) {
                addon.setImage(Base64.getEncoder().encodeToString(image.getBytes()));
            }else{
                addon.setImage(defaultImage);
            }
        } catch (IOException e) {
            throw new FileUploadException();
        }
        addon.setName(addonDTO.getName());
        addon.setStatus(statusRepository.findByName("pending"));
        addon.setCreator(userRepository.findUserByUsername(username));
        addon.setDescription(addonDTO.getDescription());
        addon.setIde(idesRepository.findByName(addonDTO.getIde()));
        addon.setOrigin_link(addonDTO.getOrigin());
        addonsService.updateTag(addonDTO.getTags(), addon);

        return addon;
    }

    public Addon mergeAddons(AddonDto addonDTO, Addon addon, MultipartFile image, MultipartFile file) {

        try {
            addon.setName(addonDTO.getName());
            if (!file.isEmpty()) {
                String[] extension = file.getName().split("\\.");
                addon.setFile_extension(extension[extension.length-1]);
                addon.setAddon_file(file.getBytes());
            }
            addon.setDescription(addonDTO.getDescription());
            addon.setIde(idesRepository.findByName(addonDTO.getIde()));
            addon.setOrigin_link(addonDTO.getOrigin());
            if (!image.isEmpty()) {
                addon.setImage(Base64.getEncoder().encodeToString(image.getBytes()));
            }
            addonsService.updateTag(addonDTO.getTags(), addon);

            return addon;
        }catch (IOException e){
            throw new FileUploadException();
        }
    }

    public User mergeUsers(User user, UserDto userDTO, MultipartFile image) {
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(face);
            byte[] bytes = new byte[(int)face.length()];
            fileInputStreamReader.read(bytes);
            String defaultImage = Base64.getEncoder().encodeToString(bytes);
            String uploadedImage = Base64.getEncoder().encodeToString(image.getBytes());
            if (!uploadedImage.isEmpty()) {
                userDTO.setImage(uploadedImage);
            }else{
                if(user.getUserDetails().getImage().isEmpty()){
                    userDTO.setImage(defaultImage);
                }else{
                    userDTO.setImage(user.getUserDetails().getImage());
                }
            }
        } catch (IOException e) {
            throw new FileUploadException();
        }
        UserDetails userDetails = toUserDetails(user.getUserDetails(), userDTO);
        user.setUserDetails(userDetails);
        return user;
    }

}
