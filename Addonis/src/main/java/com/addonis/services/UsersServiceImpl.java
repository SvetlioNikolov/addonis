package com.addonis.services;

import com.addonis.exceptions.DuplicateEntityException;
import com.addonis.exceptions.EntityNotFoundException;
import com.addonis.models.User;
import com.addonis.models.UserDetails;
import com.addonis.repositories.UserDetailsRepository;
import com.addonis.repositories.UserRepository;
import com.addonis.services.contracts.UsersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.List;

/*
This class is responsible for User related operations such as creating,updating,
deleting,getting all users and validations on these operations,
 */
@Slf4j
@Service
public class UsersServiceImpl implements UsersService {
    private UserRepository userRepository;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private UserDetailsRepository userDetailsRepository;

    @Autowired
    public UsersServiceImpl(UserRepository userRepository, UserDetailsRepository userDetailsRepository, UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsRepository = userDetailsRepository;
    }


    @Override
    public List<User> getAll() {
        return userRepository.findAllByEnabledIsTrue();
    }

    @Override
    public List<User> getAllButCurrent(String username){
        return userRepository.findAllByEnabledIsTrueAndUsernameIsNot(username);
    }

    @Override
    public User getByUsername(String username) {
        User user = userRepository.findUserByUsernameAndEnabledIsTrue(username);
        if (user != null) {
            return user;
        }
        log.info("EntityNotFoundException: User with username: " + username);
        throw new EntityNotFoundException("User", "username", username);
    }

    @Override
    public void create(User user) {

        if (userDetailsManager.userExists(user.getUsername())) {
            log.info("DuplicateEntityException: User with username "+user.getUsername());
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }
        validateEmail(user);

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        passwordEncoder.encode(user.getPassword()),
                        authorities);


        userDetailsManager.createUser(newUser);


    }

    private void validateEmail(User user) {
        if (userDetailsRepository.existsByEmail(user.getUserDetails().getEmail())) {
            log.info("DuplicateEntityException: User email: " +user.getUserDetails().getEmail());
            throw new DuplicateEntityException("User", "email", user.getUserDetails().getEmail());
        }
    }


    @Override
    public void update(User user, String currentUser) {
        if (!userRepository.existsById(user.getUsername())) {
            log.info("EntityNotFoundException: User with username " + user.getUsername());
            throw new EntityNotFoundException("User", "username", user.getUsername());
        }
        //TODO validate email
        if(!canModify(user,currentUser)){
            throw new AccessDeniedException("You are not authorized.");
        }
        userDetailsRepository.save(user.getUserDetails());
        userRepository.save(user);
    }

    @Override
    public void delete(String username, String currentUser) {
        //TODO when we delete user what happens to his addons maybe have a user with name "User no longer has an account" and set the creator to that
        User userToDelete = userRepository.findUserByUsernameAndEnabledIsTrue(username);
        if (userToDelete == null) {
            log.info("EntityNotFoundException: User with username " + username);
            throw new EntityNotFoundException("User", "username", username);
        }

        if(canModify(userToDelete, currentUser)) {
            userToDelete.setEnabled(false);
            userRepository.save(userToDelete);
        }else{
            log.info("AccessDeniedException: You are not authorized. for user " + username);
            throw new AccessDeniedException("You are not authorized.");
        }
    }

    private boolean canModify(User user, String currentUser){
        User principal = userRepository.findUserByUsername(currentUser);
        return currentUser.equalsIgnoreCase(user.getUsername()) || principal.isAdmin();
    }
    @Override
    public User getByUserDetails(UserDetails userDetails) {
        User user = userRepository.findUserByUserDetails(userDetails);
        if (user != null){
            return user;
        }
        log.info("EntityNotFoundException: User email" + userDetails.getEmail());
        throw new EntityNotFoundException("User","email",userDetails.getEmail());
    }
}
