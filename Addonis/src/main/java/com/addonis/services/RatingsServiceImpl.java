package com.addonis.services;

import com.addonis.exceptions.DuplicateEntityException;
import com.addonis.exceptions.EntityNotFoundException;
import com.addonis.models.Addon;
import com.addonis.models.Rating;
import com.addonis.models.User;
import com.addonis.repositories.RatingsRepository;
import com.addonis.services.contracts.RatingsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/*
This class is responsible for Rating related operations such as creating,
getting a specific rating, getting an AVG rating and validations on these operations,
 */
@Slf4j
@Service
public class RatingsServiceImpl implements RatingsService {

    private RatingsRepository ratingsRepository;

    @Autowired
    public RatingsServiceImpl(RatingsRepository ratingsRepository) {
        this.ratingsRepository = ratingsRepository;
    }


    @Override
    public double getAverageRating(int addonId) {
        return ratingsRepository.findAverageRating(addonId);
    }

    @Override
    public boolean checkRatingExists(User user, Addon addon) {
        return ratingsRepository.existsByUserIsAndAddonIs(user, addon);
    }

    @Override
    public Rating getRating(User user, Addon addon) {
        if(!checkRatingExists(user, addon)){
            log.info("EntityNotFoundException: Addon rating form");
            throw new EntityNotFoundException("Addon", "rating from", user.getUsername());
        }
        return ratingsRepository.findRatingByUserIsAndAddonIs(user, addon);
    }

    @Override
    public Rating create(Rating rating) {
        if(checkRatingExists(rating.getUser(), rating.getAddon())){
            log.info("DuplicateEntityException: Addon rating form");
            throw new DuplicateEntityException("Addon", "rating from",rating.getUser().getUsername());
        }
        return ratingsRepository.save(rating);
    }
}
