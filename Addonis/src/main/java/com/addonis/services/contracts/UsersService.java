package com.addonis.services.contracts;

import com.addonis.models.User;
import com.addonis.models.UserDetails;

import java.util.List;
/*
This interface is responsible for proving a template for the User service and all of its methods.
 */
public interface UsersService {
    List<User> getAll();
    List<User> getAllButCurrent(String currentUser);
    User getByUsername(String username);
    User getByUserDetails(UserDetails userDetails);
    void create(User user);
    void update(User user, String principal);
    void delete(String username, String currentName);
}
