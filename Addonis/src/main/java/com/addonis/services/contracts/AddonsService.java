package com.addonis.services.contracts;

import com.addonis.models.*;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.util.List;
/*
This interface is responsible for proving a template for the Addon service and all of its methods.
 */
public interface AddonsService {
    List<Addon> getAllByStatus(String status,String sortBy);
    List<Addon> getByCreator(User creator);
    List<Addon> getByTag(String name,String sortBy, int page);
    List<Addon> getFeatured();
    List<Addon> getNewest();
    List<Addon> getMostPopular();
    Addon getById(int id);
    int getNumberOfPages(String name, String ide);
    void create(Addon addon);
    void updateDownloads(Addon addon);
    void update(Addon addon, User user);
    void delete(int id, String username);

    void updateFeatured(int id, String username);

    void createTag(Tag tag);
    void updateTag(String tags, Addon addon);

    List<Addon> getFilteredByNameAndIdeAndApproved(String name, String ide, String sortCriteria, int page);
}
