package com.addonis.services.contracts;

import com.addonis.models.Addon;
import com.addonis.models.Rating;
import com.addonis.models.User;
/*
This interface is responsible for proving a template for the Rating service and all of its methods.
 */
public interface RatingsService{

    double getAverageRating(int addonId);

    boolean checkRatingExists(User user, Addon addon);

    Rating getRating(User user, Addon addon);

    Rating create(Rating rating);

}
