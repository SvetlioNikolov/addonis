package com.addonis.services;

import com.addonis.exceptions.DuplicateEntityException;
import com.addonis.exceptions.EntityNotFoundException;
import com.addonis.exceptions.InvalidGithubException;
import com.addonis.models.*;
import com.addonis.repositories.*;
import com.addonis.services.contracts.AddonsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.kohsuke.github.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
This class is responsible for the Addon and its consisting elements related operations such as CRUD,
getting all addons and validations on these operations,
 */
@Slf4j
@Service
public class AddonsServiceImpl implements AddonsService {

    private AddonsRepository addonsRepository;
    private StatusRepository statusRepository;
    private TagsRepository tagsRepository;
    private GitHubDatasRepository gitHubDatasRepository;
    private UserRepository userRepository;

    @Autowired
    public AddonsServiceImpl(AddonsRepository addonsRepository, UserRepository userRepository, StatusRepository statusRepository, TagsRepository tagsRepository, GitHubDatasRepository gitHubDatasRepository) {
        this.addonsRepository = addonsRepository;
        this.statusRepository = statusRepository;
        this.tagsRepository = tagsRepository;
        this.gitHubDatasRepository = gitHubDatasRepository;
        this.userRepository = userRepository;
    }

    //For Admin to approve
    @Override
    public List<Addon> getAllByStatus(String status, String sortBy) {
        Status statusToFind = statusRepository.findByName(status);
        return addonsRepository.findByStatus(statusToFind, sortByAsc(sortBy));
    }

    @Override
    public List<Addon> getByCreator(User user) {
        Status approved = statusRepository.findByName("approved");
        return addonsRepository.findByCreatorAndStatus(user, approved);
    }


    @Override
    public List<Addon> getByTag(String name, String sortBy, int page) {
        return addonsRepository.findByTagsLikeAndStatus(name, PageRequest.of(page, 12, sortByAsc(sortBy)));
    }

    @Override
    public Addon getById(int id) {
        Addon addon = addonsRepository.findAddonById(id);
        if (addon != null) {
            return addon;
        }
        throw new EntityNotFoundException("Addon", "id", id);
    }


    //TODO log in controllers - centralezirano hvashtane
    @Override
    public void create(Addon addon) {
        checkAddonExistByName(addon);

        try {
            ExecutorService workers = Executors.newFixedThreadPool(5);
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            addon.setUpload_date(dtf.format(now));
            addonsRepository.save(addon);
            workers.submit(() -> {
                addon.setGitHubData(createGitHubData(addon.getOrigin_link()));
                addonsRepository.save(addon);
            });
        } catch (InvalidGithubException e) {
            log.info(e.getMessage());
            throw new InvalidGithubException();
        }
    }

    private void checkAddonExistByName(Addon addon) {
        if (addonsRepository.existsByNameAndIdNot(addon.getName(), addon.getId())) {
            throw new DuplicateEntityException("Addon", "name", addon.getName());
        }
    }

    @Override
    public void update(Addon addon, User user) {
        checkAddonExistsByID(addon.getId());
        checkAddonExistByName(addon);
        if (canModify(user, addon)) {
            addonsRepository.save(addon);
        } else {
            log.info("AccessDeniedException: You are not authorized. - User: " + user.getUsername());
            throw new AccessDeniedException("You are not authorized.");
        }
    }

    @Override
    public void updateDownloads(Addon addon) {
        checkAddonExistsByID(addon.getId());
        addonsRepository.save(addon);
    }

    @Override
    public void delete(int id, String username) {
        checkAddonExistsByID(id);
        User user = userRepository.findUserByUsername(username);
        Addon addonToDelete = addonsRepository.findByIdAndStatusNot(id, statusRepository.findByName("deleted"));
        if (canModify(user, addonToDelete)) {
            addonToDelete.setStatus(statusRepository.findByName("deleted"));
            addonsRepository.save(addonToDelete);
        } else {
            log.info("AccessDeniedException for user " + user.getUsername());
            throw new AccessDeniedException(user.getUsername());
        }
    }

    @Override
    public void createTag(Tag tag) {
        //TODO use saveAll
        if (tagsRepository.existsByName(tag.getName())) {
            log.info("DuplicateEntityException for tag: " + tag.getName());
            throw new DuplicateEntityException("Tag", "name", tag.getName());
        }
        tagsRepository.save(tag);
    }

    @Override
    public List<Addon> getFeatured() {
        Status approved = statusRepository.findByName("approved");
        return addonsRepository.findTop5ByStatusAndFeaturedIsTrue(approved);
    }

    @Override
    public List<Addon> getNewest() {
        return addonsRepository.findByStatusOrderByUpload_dateDesc();
    }

    @Override
    public List<Addon> getMostPopular() {
        Status approved = statusRepository.findByName("approved");
        return addonsRepository.findTop5ByStatusOrderByDownloadsDesc(approved);
    }

    @Override
    public int getNumberOfPages(String name, String ide){
        Page<Addon> result = addonsRepository.findAddonsByPages(name, ide, PageRequest.of(0, 12));
        return (int) ((result.getTotalElements() / 12));
    }

    @Override
    public List<Addon> getFilteredByNameAndIdeAndApproved(String name, String ide, String sortCriteria, int page) {
        if (name.startsWith("tag:")) {
            String[] input = name.split(":");
            String tag = input[1];
            return getByTag(tag, "name", page);
        }
        return addonsRepository.findAddonsByNameAndIdeAndApproved(name, ide, PageRequest.of(page, 12, sortByDesc(sortCriteria)));
    }

    @Override
    public void updateTag(String tags, Addon addon) {
        //TODO don't call repo 2 times , with query LIKE get all
        // tags corresponding to the wanted iterate them if some
        // dont match with database add it do a array for saveAll operation from repo
        Set<Tag> newTags = new HashSet<>();
        for (String tag : tags.split(" ")) {
            if (!tagsRepository.existsByName(tag)) {
                Tag newTag = new Tag();
                newTag.setName(tag);
                createTag(newTag);
                newTags.add(newTag);
            } else {
                newTags.add(tagsRepository.findByName(tag));
            }
        }
        addon.setTags(newTags);
    }

    @Override
    public void updateFeatured(int id, String username) {
        checkAddonExistsByID(id);
        Addon addon = addonsRepository.findAddonById(id);
        User user = userRepository.findUserByUsername(username);
        if (canModify(user, addon)) {
            if (addon.isFeatured()) {
                addon.setFeatured(false);
            } else {
                addon.setFeatured(true);
            }
        }
        addonsRepository.save(addon);
    }

    private boolean canModify(User user, Addon addon) {
        return user.isAdmin() || addon.getCreator().getUsername().equalsIgnoreCase(user.getUsername());
    }

    private void checkAddonExistsByID(int id) {
        if (addonsRepository.existsById(id) != 1) {
            log.info("EntityNotFound");
            throw new EntityNotFoundException("Addon", "id", id);
        }
    }

    private GitHubData createGitHubData(String link) {
        try {
            String[] providedLink = link.split("/");
            String repo = providedLink[providedLink.length - 2] + "/" + providedLink[providedLink.length - 1];
            GitHub gitHub = new GitHubBuilder().withOAuthToken("d5a79bf9d74b2b313b851d130e62f4ad408c912d").build();
            GHRepository repository = gitHub.getRepository(repo);
            GitHubData gitHubData = new GitHubData();
            gitHubData.setIssues(repository.listIssues(GHIssueState.OPEN).toList().size());
            gitHubData.setPulls(repository.getPullRequests(GHIssueState.ALL).size());
            gitHubData.setLastCommit(repository.listCommits().toList().get(0).getCommitShortInfo().getCommitDate());
            gitHubData.setReadme(IOUtils.toString(repository.getReadme().read()));
            gitHubData.setLastCommitTitle(repository.listCommits().toList().get(0).getCommitShortInfo().getMessage());
            gitHubDatasRepository.save(gitHubData);
            return gitHubData;
        } catch (IOException e) {
            log.info("InvalidGitHubException");
            throw new InvalidGithubException();
        }
    }

    private Sort sortByAsc(String columnName) {
        return Sort.by(columnName).ascending();
    }

    private Sort sortByDesc(String columnName) {
        return Sort.by(columnName).descending();
    }
}
