package com.addonis.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.sql.DataSource;
/*
This class is responsible for configuring Spring Security.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {


    private DataSource securityDataSource;

    @Autowired
    public SecurityConfiguration(DataSource securityDataSource) {
        this.securityDataSource = securityDataSource;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(securityDataSource);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers("/addons/{id}/edit")
                    .hasRole("USER")
                .antMatchers("/addons/{id}/delete")
                    .hasRole("USER")
                .antMatchers("/addons/{id}/rate")
                    .hasRole("USER")
                .antMatchers("/addons/{id}/featured")
                    .hasRole("ADMIN")
                .antMatchers("/user/{username}/delete")
                    .hasRole("ADMIN")
                .antMatchers("/admin/addon/{id}/approve")
                    .hasRole("ADMIN")
                .antMatchers("/")
                    .permitAll()
                .antMatchers("/confirm-account")
                    .permitAll()
                .antMatchers("/addons/{id}/addonimage")
                    .permitAll()
                .antMatchers("/user/{username}/userimage")
                    .permitAll()
                .antMatchers("/addons")
                    .permitAll()
                .antMatchers("/user/{username}")
                    .permitAll()
                .antMatchers("/addons/{id}/details")
                    .permitAll()
                .antMatchers("/addons/{id}/download")
                    .permitAll()
                .antMatchers("/api/users/{username}")
                    .permitAll()
                .antMatchers("/api/users")
                    .permitAll()
                .antMatchers("/api/addons")
                    .permitAll()
                .antMatchers("/api/addons/{id}")
                    .permitAll()
                .and()
                .formLogin()
                .loginPage("/auth")
                .loginProcessingUrl("/authenticate")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/access-denied");
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(securityDataSource);
        return jdbcUserDetailsManager;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/img/**");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
