package com.addonis.controllers;

import com.addonis.exceptions.DuplicateEntityException;
import com.addonis.exceptions.EntityNotFoundException;
import com.addonis.models.dtos.AddonDto;
import com.addonis.models.dtos.UserDto;
import com.addonis.models.User;
import com.addonis.services.contracts.UsersService;
import com.addonis.models.Mapper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
/*
This class is so that the user can use the User Api with all of its features like
CRUD operations and getting and filtering all users.
 */
@RestController
@RequestMapping("/api/users")
public class UsersAPI {
    private UsersService usersService;
    private Mapper mapper;

    @Autowired
    public UsersAPI(UsersService usersService, Mapper mapper) {
        this.usersService = usersService;
        this.mapper = mapper;
    }

    @GetMapping(value = "/{username}")
    public User getByUsername(@PathVariable String username) {
        try {
            return usersService.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping
    public List<User> getAll() {
        return usersService.getAll();
    }


    @PutMapping(value = "/{username}")
    public User updateUser(@PathVariable String username,
                           @RequestParam("dto") @Valid String userDto,
                           MultipartFile image,
                           Principal principal) {
        try {
            Gson gson = new Gson();
            UserDto dto = gson.fromJson(userDto, UserDto.class);
            User userToUpdate = usersService.getByUsername(username);
            userToUpdate = mapper.mergeUsers(userToUpdate, dto, image);
            usersService.update(userToUpdate, principal.getName());
            return userToUpdate;
        }catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping(value = "/{username}")
    public void delete(@PathVariable String username,
                       Principal principal) {
        try {
            usersService.delete(username, principal.getName());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AccessDeniedException e){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

}
