package com.addonis.controllers;

import com.addonis.exceptions.DuplicateEntityException;
import com.addonis.exceptions.EntityNotFoundException;
import com.addonis.exceptions.InvalidGithubException;
import com.addonis.models.Addon;
import com.addonis.models.User;
import com.addonis.models.dtos.AddonDto;
import com.addonis.services.contracts.AddonsService;
import com.addonis.services.contracts.UsersService;
import com.addonis.models.Mapper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.AccessControlException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
/*
This class is so that the user can use the Addon Api with all of its features like
CRUD operations and getting and filtering all addons.id
 */
@RestController
@RequestMapping("/api/addons")
public class AddonsAPI {
    private AddonsService addonsService;
    private UsersService usersService;
    private Mapper mapper;

    @Autowired
    public AddonsAPI(AddonsService addonsService, UsersService usersService, Mapper mapper) {
        this.addonsService = addonsService;
        this.usersService = usersService;
        this.mapper = mapper;
    }

    @GetMapping(value = "/{id}")
    public Addon getById(@PathVariable int id) {
        try {
            return addonsService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping
    public List<Addon> getAll(
            @RequestParam(defaultValue = "") String name,
            @RequestParam(defaultValue = "") String ide,
            @RequestParam(defaultValue = "name") String sortCriteria,
            @RequestParam(defaultValue = "0") int page) {

        return new ArrayList<>(addonsService.getFilteredByNameAndIdeAndApproved(name, ide, sortCriteria, page));
    }

    @PostMapping(value = "/add")
    public Addon create(@RequestParam("dto") @Valid String dto,
                        @RequestParam("image") MultipartFile image,
                        @RequestParam("file") MultipartFile file,
                        Principal principal) {
        try {
            Gson gson = new Gson();
            AddonDto addonDto = gson.fromJson(dto, AddonDto.class);
            Addon addonToCreate = mapper.toAddon(addonDto, principal.getName(), image, file);
            addonsService.create(addonToCreate);
            return addonToCreate;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (InvalidGithubException i) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, i.getMessage());
        }
    }

    @PutMapping(value = "/{id}")
    public Addon update(@PathVariable int id,
                        @RequestParam("dto") @Valid String dto,
                        @RequestParam("image") MultipartFile image,
                        @RequestParam("file") MultipartFile file,
                        Principal principal) {
        try {
            Gson gson = new Gson();
            AddonDto addonDto = gson.fromJson(dto, AddonDto.class);
            Addon addonToUpdate = addonsService.getById(id);
            User user = usersService.getByUsername(principal.getName());
            addonToUpdate = mapper.mergeAddons(addonDto, addonToUpdate, image, file);
            addonsService.update(addonToUpdate, user);
            return addonToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AccessControlException e){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable int id, Principal principal) {
        try {
            addonsService.delete(id, principal.getName());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AccessControlException e){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }
}
