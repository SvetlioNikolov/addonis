package com.addonis.controllers.mvc;

import com.addonis.exceptions.DuplicateEntityException;
import com.addonis.exceptions.EntityNotFoundException;
import com.addonis.exceptions.FileUploadException;
import com.addonis.exceptions.InvalidGithubException;
import com.addonis.models.*;
import com.addonis.models.dtos.AddonDto;
import com.addonis.repositories.IdesRepository;
import com.addonis.services.contracts.AddonsService;
import com.addonis.services.contracts.RatingsService;
import com.addonis.services.contracts.UsersService;
import com.addonis.models.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Principal;
import java.util.Base64;
import java.util.List;
/*
This class is used to connect the addon data and all its corresponding features like CRUD operations,
details, rating and download of the addon with the view.
 */
@Controller
public class AddonController {

    private AddonsService addonsService;
    private UsersService usersService;
    private IdesRepository idesRepository;
    private Mapper mapper;
    private RatingsService ratingsService;

    @Autowired
    public AddonController(AddonsService addonsService, Mapper mapper, IdesRepository idesRepository,
                           UsersService usersService, RatingsService ratingsService) {
        this.addonsService = addonsService;
        this.ratingsService = ratingsService;
        this.usersService = usersService;
        this.mapper = mapper;
        this.idesRepository = idesRepository;
    }

    @GetMapping("/addons/new")
    public String create(Model model) {
        model.addAttribute("addon", new AddonDto());
        model.addAttribute("ides",idesRepository.findAll());
        return "create";
    }

    @GetMapping("/addons")
    public String showAddons(@RequestParam(defaultValue = "") String name,
                             @RequestParam(defaultValue = "") String ide,
                             @RequestParam(defaultValue = "upload_date") String sortCriteria,
                             @RequestParam(defaultValue = "0") int page,
                             Model model) {
        int pages = addonsService.getNumberOfPages(name, ide);
        List<Addon> addons = addonsService.getFilteredByNameAndIdeAndApproved(name, ide, sortCriteria, page);

        model.addAttribute("searchName", name);
        model.addAttribute("searchIde", ide);
        model.addAttribute("searchSortCriteria", sortCriteria);
        model.addAttribute("currentPage", page);
        model.addAttribute("numberOfPages", pages);
        model.addAttribute("addons", addons);

        return "addons";
    }

    @PostMapping("/addons/new")
    public String createForm(@Valid @ModelAttribute(name = "addon") AddonDto addonDTO,
                             BindingResult error,
                             Model model,
                             Principal principal,
                             @RequestParam("datafile") MultipartFile file,
                             @RequestParam("imagefile") MultipartFile image) {
        if (error.hasErrors()) {
            return "create";
        }
        Addon addonToCreate = mapper.toAddon(addonDTO, principal.getName(), image, file);
        try {
            addonsService.create(addonToCreate);
        } catch (DuplicateEntityException | InvalidGithubException ex) {
            model.addAttribute("error", ex);
            return "create";
        }

        return "redirect:/addons";
    }


    @GetMapping("/addons/{id}/edit")
    public String showAddon(@PathVariable int id, Model model) {
        model.addAttribute("addon", mapper.toAddonDTO(addonsService.getById(id)));
        model.addAttribute("ides", idesRepository.findAll());
        model.addAttribute("currentId", id);
        return "edit-addon";
    }

    @PostMapping("/addons/{id}/edit")
    public String editAddon(@PathVariable int id, @Valid @ModelAttribute AddonDto addonDTO,
                            BindingResult errors,
                            Model model,
                            Principal principal,
                            @RequestParam("datafile") MultipartFile file,
                            @RequestParam("imagefile") MultipartFile image) {

        if (errors.hasErrors()) {
            return "edit-addon";
        }
        try {
            User user = usersService.getByUsername(principal.getName());
            Addon addonToUpdate = addonsService.getById(id);
            Addon addonUpdated = mapper.mergeAddons(addonDTO, addonToUpdate, image, file);
            addonsService.update(addonUpdated, user);
        } catch (EntityNotFoundException | DuplicateEntityException | AccessDeniedException | FileUploadException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
        return "redirect:/addons";
    }

    @PostMapping("/addons/{id}/delete")
    public String deleteAddon(@PathVariable int id,
                              Model model,
                              Principal principal) {

        try {
            addonsService.delete(id, principal.getName());
        } catch (EntityNotFoundException | AccessDeniedException e) {
            model.addAttribute("error", e.getMessage());
        }
        return "redirect:/addons";
    }

    @GetMapping("/addons/{id}/download")
    public ResponseEntity<byte[]> downloadAddon(@PathVariable int id) {
        Addon addon = addonsService.getById(id);

        if (addon != null) {
            addon.setDownloads(addon.getDownloads() + 1);
            addonsService.updateDownloads(addon);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + addon.getName() + "." + addon.getFile_extension() + "\"")
                    .body(addon.getAddon_file());
        }
        return ResponseEntity.status(404).body(null);
    }

    @GetMapping("/addons/{id}/details")
    public String showDetailsForm(@PathVariable int id, Model model, Principal principal) {
        try{
            Addon addon = addonsService.getById(id);

            model.addAttribute("addon", addon);
            model.addAttribute("tags", addon.getTags());
            model.addAttribute("avgRating", ratingsService.getAverageRating(id));
            if (principal != null) {
                User user = usersService.getByUsername(principal.getName());
                boolean canVote = !ratingsService.checkRatingExists(user, addon);
                model.addAttribute("canVote", canVote);
                if (!canVote) {
                    model.addAttribute("votedValue", ratingsService.getRating(user, addon).getRating_value());
                }
            }
        }catch (EntityNotFoundException e){
            model.addAttribute(e.getMessage());
            return "error";
        }

        return "addon-details";
    }


    @PostMapping("/addons/{id}/rate")
    public String rateAddonForm(@PathVariable int id,
                                @RequestParam("value") int value,
                                Principal principal) {

        Addon addon = addonsService.getById(id);
        User user = usersService.getByUsername(principal.getName());
        Rating rating = new Rating();

        rating.setAddon(addon);
        rating.setUser(user);
        rating.setRating_value(value);

        ratingsService.create(rating);

        return "redirect:/addons/" + id + "/details";
    }

    @PostMapping("/addons/{id}/featured")
    public String featuredAddonForm(@PathVariable int id,
                                    Principal principal){
        addonsService.updateFeatured(id, principal.getName());
        return "redirect:/user/" + principal.getName() + "#addonsToApprove";
    }

}
