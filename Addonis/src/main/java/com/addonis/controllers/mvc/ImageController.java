package com.addonis.controllers.mvc;

import com.addonis.models.Addon;
import com.addonis.models.User;
import com.addonis.services.contracts.AddonsService;
import com.addonis.services.contracts.UsersService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
/*
This class is responsible for displaying the images of users and addons on the view.
 */
@Controller
public class ImageController {

    private AddonsService addonsService;
    private UsersService usersService;

    @Autowired
    public ImageController(UsersService usersService, AddonsService addonsService) {
        this.usersService = usersService;
        this.addonsService = addonsService;
    }

    @GetMapping("/addons/{id}/image")
    public void showAddonUploadForm(@PathVariable int id, Model model){
        model.addAttribute("addon", addonsService.getById(id));
    }

    @GetMapping("/user/{username}/image")
    public void showUserUploadForm(@PathVariable String username, Model model) {
        model.addAttribute("user", usersService.getByUsername(username));
    }

    @GetMapping("/addons/{id}/addonimage")
    public void renderAddonImageFormDB(@PathVariable int id, HttpServletResponse response) throws IOException {
        Addon addon = addonsService.getById(id);
        checkImageExists(response, addon.getImage());
    }

    @GetMapping("/user/{username}/userimage")
    public void renderUserImageFormDB(@PathVariable String username, HttpServletResponse response) throws IOException {
        User user = usersService.getByUsername(username);
        checkImageExists(response, user.getUserDetails().getImage());
    }

    private void checkImageExists(HttpServletResponse response, String image) throws IOException {
        if (image != null) {
            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(image));
            IOUtils.copy(is, response.getOutputStream());
        }
    }

}