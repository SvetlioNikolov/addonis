package com.addonis.controllers.mvc;

import com.addonis.exceptions.DuplicateEntityException;
import com.addonis.exceptions.EntityNotFoundException;
import com.addonis.models.Addon;
import com.addonis.models.Status;
import com.addonis.models.dtos.UserDto;
import com.addonis.models.User;
import com.addonis.repositories.StatusRepository;
import com.addonis.services.contracts.AddonsService;
import com.addonis.services.contracts.UsersService;
import com.addonis.models.Mapper;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.security.Principal;

/*
This class is responsible for displaying on whats on the user profile page. It connects the updating,
deleting and admin approval data with the view.
 */
@Controller
public class ProfileController {

    private final UsersService userService;
    private AddonsService addonsService;
    private StatusRepository statusRepository;
    private Mapper mapper;


    @Autowired
    public ProfileController(UsersService userService, Mapper mapper, AddonsService addonsService, StatusRepository statusRepository) {
        this.userService = userService;
        this.addonsService = addonsService;
        this.statusRepository = statusRepository;
        this.mapper = mapper;
    }

    @GetMapping("/user/{username}")
    public String updateUser(Model model,
                             @PathVariable String username) {
        User user = userService.getByUsername(username);
        model.addAttribute("user", mapper.toUserDTO(user));
        model.addAttribute("addonFromUser", addonsService.getByCreator(user));
        model.addAttribute("users", userService.getAllButCurrent(username));
        model.addAttribute("addonsToApprove", addonsService.getAllByStatus("pending","name"));
        model.addAttribute("addons", addonsService.getAllByStatus("approved","name"));
        return "profile";
    }

    @PostMapping("/user/{username}")
    public String updateUser(@Valid @ModelAttribute UserDto userDTO,
                             @PathVariable String username,
                             BindingResult errors,
                             Model model,
                             Principal principal,
                             @RequestParam(value = "imagefile") MultipartFile file) {
        if (errors.hasErrors()) {
            return "profile";
        }
        try {
                User userProfile = userService.getByUsername(username);
                User updatedUser = mapper.mergeUsers(userProfile, userDTO, file);
                userService.update(updatedUser, principal.getName());
            return "redirect:/user/" + username + "#edit";
        }catch (DuplicateEntityException e){
            model.addAttribute("error", e.getMessage());
            return "profile";
        } catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "error";
        }

    }

    @PostMapping("/user/{username}/delete")
    public String deleteUser(@PathVariable String username,
                             Model model,
                             Principal principal) {
        try {
            User user = userService.getByUsername(principal.getName());
            userService.delete(username, principal.getName());
            if(user.isAdmin()){
                return "redirect:/user/" + user.getUsername() + "#usersAdmin";
            }else {
                return "redirect:/";
            }
        }catch (EntityNotFoundException | AccessDeniedException e){
            model.addAttribute("error",e.getMessage());
            return "error";
        }

    }

    @PostMapping("/admin/addon/{id}/approve")
    public String adminManageApprove(@PathVariable int id, Model model, Principal principal){
        try {
            User user = userService.getByUsername(principal.getName());
            Addon addon = addonsService.getById(id);
            Status status = statusRepository.findByName("approved");
            addon.setStatus(status);
            addonsService.update(addon, user);
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
        }catch (AccessDeniedException e){
            model.addAttribute("error", e.getMessage());
            return "error";
        }
        return "redirect:/user/" + principal.getName() + "#addonsToApprove";
    }


}
