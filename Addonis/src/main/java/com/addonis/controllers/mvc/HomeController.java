package com.addonis.controllers.mvc;


import com.addonis.models.Addon;
import com.addonis.services.contracts.AddonsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
/*
This class is responsible for displaying on whats on the front page. It connects the featured, popular and newest data with the view.
 */
@Controller
public class HomeController {

    private AddonsService addonsService;

    @Autowired
    public HomeController(AddonsService addonsService) {
        this.addonsService = addonsService;
    }

    @GetMapping("/")
    public String showHomePage(Model model) {
        List<Addon> addonsFeatured = addonsService.getFeatured();
        List<Addon> addonsPopular = addonsService.getMostPopular();
        List<Addon> addonsNew = addonsService.getNewest();
        model.addAttribute("featured", addonsFeatured);
        model.addAttribute("popular", addonsPopular);
        model.addAttribute("newest", addonsNew);
        return "index";
    }

    @RequestMapping("/access-denied")
    public String accessDenied() {
        return "access-denied";
    }

}
