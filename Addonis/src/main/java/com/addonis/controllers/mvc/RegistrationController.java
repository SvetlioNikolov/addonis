package com.addonis.controllers.mvc;

import com.addonis.exceptions.DuplicateEntityException;
import com.addonis.models.ConfirmationToken;
import com.addonis.models.dtos.UserDto;
import com.addonis.models.User;
import com.addonis.models.UserDetails;
import com.addonis.repositories.ConfirmationTokenRepository;
import com.addonis.repositories.UserDetailsRepository;
import com.addonis.repositories.UserRepository;
import com.addonis.services.contracts.UsersService;
import com.addonis.services.EmailSenderService;
import com.addonis.models.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/*
This class is responsible for displaying on whats on the registration/log in  page. It connects the
new or existing user information and transfers it to the service. Its main focus is welcoming new users
by sending an email verification or serve existing existing users so that they can log in with their credentials.
 */
@Controller
public class RegistrationController {
    private UsersService usersService;
    private ConfirmationTokenRepository confirmationTokenRepository;
    private UserDetailsRepository userDetailsRepository;
    private EmailSenderService emailSenderService;
    private Mapper mapper;
    private UserRepository userRepository;

    @Autowired
    public RegistrationController(UserRepository userRepository, UsersService usersService, UserDetailsRepository userDetailsRepository, ConfirmationTokenRepository confirmationTokenRepository, EmailSenderService emailSenderService, Mapper mapper) {
        this.usersService = usersService;
        this.confirmationTokenRepository = confirmationTokenRepository;
        this.emailSenderService = emailSenderService;
        this.mapper = mapper;
        this.userRepository = userRepository;
        this.userDetailsRepository = userDetailsRepository;
    }

    @GetMapping("/auth")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new UserDto());
        return "signIn_register";
    }

    @PostMapping("/auth")
    public String registerUser(@Valid @ModelAttribute(name = "user") UserDto userDTO, BindingResult error, Model model) {
        if (error.hasErrors()) {
            return "signIn_register";
        }
        try {
            if (!userDTO.getPassword().equals(userDTO.getPasswordConfirmation())) {
                model.addAttribute("error", "Password doesn't match");
                return "signIn_register";
            }
            usersService.create(mapper.toUser(userDTO));


            ConfirmationToken confirmationToken = new ConfirmationToken(mapper.toUserDetails(new UserDetails(), userDTO));
            confirmationTokenRepository.save(confirmationToken);

            UserDetails userDetails = confirmationToken.getUserDetails();
            User userUpdated = userRepository.findUserByUsernameAndEnabledIsTrue(userDTO.getUsername());
            userUpdated.setEnabled(false);
            userUpdated.setUserDetails(userDetails);
            userRepository.save(userUpdated);


            mailSender(userDTO, confirmationToken);

            model.addAttribute("email", userDTO.getEmail());
            return "successful-registration";

        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "signIn_register";
        }
    }

    private void mailSender(UserDto userDTO, ConfirmationToken confirmationToken) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(userDTO.getEmail());
        mailMessage.setSubject("Complete Registration!");
        mailMessage.setFrom("addonistelerik@gmail.com");
        mailMessage.setText("To confirm your account, please click here : "
                + "http://localhost:8090/confirm-account?token=" + confirmationToken.getConfirmationToken() +
                "\nWelcome to the team :)");

        emailSenderService.sendEmail(mailMessage);
    }

    @RequestMapping(value = "/confirm-account", method = {RequestMethod.GET, RequestMethod.POST})
    public String confirmUserAccount(Model model, @RequestParam("token") String confirmationToken) {
        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);

        if (token != null) {
            UserDetails userDetails = userDetailsRepository.findByEmailIgnoreCase(token.getUserDetails().getEmail());
            User user = usersService.getByUserDetails(userDetails);
            user.setEnabled(true);
            userRepository.save(user);
            return "account-verified";
        } else {
            model.addAttribute("message", "The link is invalid or broken!");
            return "verified-succesfully";
        }
    }

}
