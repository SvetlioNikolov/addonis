package com.addonis.exceptions;
/*
This class is responsible for displaying information when exceptions caused by an object already existing in the database occur.
 */
public class DuplicateEntityException extends RuntimeException {
    public DuplicateEntityException(String message) {
        super((message));
    }
    public DuplicateEntityException(String type, String format) {
        this(type, "id", String.valueOf(format));
    }

    public DuplicateEntityException(String type, int id) {
        this(type, "id", String.valueOf(id));
    }

    public DuplicateEntityException(String type, String attribute, String value) {
        super(String.format("%s with %s %s already exists", type, attribute, value));
    }
}
