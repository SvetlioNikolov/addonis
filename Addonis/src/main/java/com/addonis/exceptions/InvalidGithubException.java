package com.addonis.exceptions;
/*
This class is responsible for displaying information when exceptions caused by wrong second password when registering.
 */
public class InvalidGithubException extends RuntimeException {

    public InvalidGithubException() {
        super("Github repository does not exist!");
    }

}
