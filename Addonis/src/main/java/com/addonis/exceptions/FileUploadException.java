package com.addonis.exceptions;

public class FileUploadException extends RuntimeException {

    public FileUploadException() {
        super("File not supported!");
    }
}
