package com.addonis.repositories;

import com.addonis.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/*
This class is responsible for fetching and delivering s specific information about tags to and from the database.
 */
@Repository
public interface TagsRepository extends JpaRepository<Tag, Integer> {

    Tag findByName(String name);

    boolean existsByName(String name);
}
