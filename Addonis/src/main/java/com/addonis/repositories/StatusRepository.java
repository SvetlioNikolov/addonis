package com.addonis.repositories;

import com.addonis.models.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/*
This class is responsible for fetching specific information about statuses from the database.
 */
@Repository
public interface StatusRepository extends JpaRepository<Status, Integer> {

    Status findByName(String status);
}
