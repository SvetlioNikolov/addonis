package com.addonis.repositories;


import com.addonis.models.User;
import com.addonis.models.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.Size;
import java.util.List;
/*
This class is responsible for fetching and delivering s specific information about users to and from the database.
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {

    List<User> findAllByEnabledIsTrue();
    @Query(value = "select * from users u where u.username != :username and u.enabled is true", nativeQuery = true)
    List<User> findAllByEnabledIsTrueAndUsernameIsNot(String username);
    User findUserByUsernameAndEnabledIsTrue(String username);
    User findUserByUsername(String username);
    User findUserByUserDetails(UserDetails userDetails);
}
