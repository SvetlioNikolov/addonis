package com.addonis.repositories;

import com.addonis.models.Addon;
import com.addonis.models.Rating;
import com.addonis.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
/*
This class is responsible for fetching and delivering s specific information about ratings from the database.
 */
@Repository
public interface RatingsRepository extends JpaRepository<Rating, Integer> {

    @Query("SELECT coalesce( AVG(rating_value),0) from Rating where addon.id = :addonId ")
    double findAverageRating(@Param("addonId") int addonId);

    Rating findRatingByUserIsAndAddonIs(User user, Addon addon);

    boolean existsByUserIsAndAddonIs(User user, Addon addon);
}
