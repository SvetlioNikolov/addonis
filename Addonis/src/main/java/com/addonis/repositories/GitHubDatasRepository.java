package com.addonis.repositories;

import com.addonis.models.GitHubData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/*
This class is responsible for delivering specific information about the information taken from github to the database.
 */
@Repository
public interface GitHubDatasRepository extends JpaRepository<GitHubData, Integer> {


}
