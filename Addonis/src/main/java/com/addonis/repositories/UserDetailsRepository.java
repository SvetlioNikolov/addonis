package com.addonis.repositories;

import com.addonis.models.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/*
This class is responsible for fetching and delivering s specific information about user details to and from the database.
 */
@Repository
public interface UserDetailsRepository extends JpaRepository<UserDetails, Integer> {
    UserDetails findByEmailIgnoreCase(String email);

    boolean existsByEmail(String email);
}
