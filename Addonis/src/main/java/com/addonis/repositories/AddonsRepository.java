package com.addonis.repositories;

import com.addonis.models.Addon;
import com.addonis.models.Status;
import com.addonis.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
/*
This class is responsible for fetching and delivering specific information about addons to and from the database.
 */
@Repository
public interface AddonsRepository extends JpaRepository<Addon, Integer> {

    Addon findAddonById(int id);

    List<Addon> findByCreatorAndStatus(User creator, Status status);

    Addon findByIdAndStatusNot(int id, Status status);

    @Query("SELECT a FROM Addon a join fetch GitHubData gd on a.gitHubData.id = gd.id WHERE (a.name like %:name%) " +
            "and (a.ide.name like %:ide%) and (a.status.name = 'approved' or a.featured =true) ")
    List<Addon> findAddonsByNameAndIdeAndApproved(@Param("name") String name, @Param("ide") String ide, Pageable pageable);

    @Query("SELECT a FROM Addon a join fetch GitHubData gd on a.gitHubData.id = gd.id WHERE (a.name like %:name%) " +
            "and (a.ide.name like %:ide%) and (a.status.name = 'approved' or a.featured =true) ")
    Page<Addon> findAddonsByPages(@Param("name") String name, @Param("ide") String ide, Pageable pageable);

    List<Addon> findTop5ByStatusAndFeaturedIsTrue(Status status);

    @Query(value = "select * from addons a where a.status_id = 2 order by a.upload_date desc limit 5", nativeQuery = true)
    List<Addon> findByStatusOrderByUpload_dateDesc();

    List<Addon> findTop5ByStatusOrderByDownloadsDesc(Status status);

    @Query(value = "SELECT a FROM Addon a JOIN a.tags t WHERE a.status.id = 2 AND t.name like %:tag%")
    List<Addon> findByTagsLikeAndStatus(@Param("tag") String tag, Pageable pageable);

    List<Addon> findByStatus(Status status, Sort sort);

    @Query(value = "select count(a) from Addon a where a.status.name not like 'deleted' and a.id = :id")
    int existsById(@Param("id") int id);


    boolean existsByNameAndIdNot(String name, int id);


}
