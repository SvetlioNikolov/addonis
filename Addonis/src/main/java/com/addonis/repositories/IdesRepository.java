package com.addonis.repositories;

import com.addonis.models.Ide;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/*
This class is responsible for fetching specific information about platforms from the database.
 */
@Repository
public interface IdesRepository extends JpaRepository<Ide, Integer> {

    Ide findByName(String name);

    boolean existsByName(String name);
}
