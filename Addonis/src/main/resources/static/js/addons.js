// Sidenav
const sideNav = document.querySelector(".sidenav");
M.Sidenav.init(sideNav, {});

// Autocomplete
const ac = document.querySelector(".autocomplete");
M.Autocomplete.init(ac, {
  data: {
    IntelliJ: null,
    "Visual studio": null,
    Eclipse: null,
    "Visual Studio Code": null,
    proba: null,
    Svet: null,
    Kris: null,
  },
});

// Material Boxed
const mb = document.querySelectorAll(".materialboxed");
M.Materialbox.init(mb, {});

$(document).ready(function () {
  $(".collapsible").collapsible();
});
