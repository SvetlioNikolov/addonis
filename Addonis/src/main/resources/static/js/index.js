// Side Menu
const sideNav = document.querySelector(".sidenav");
M.Sidenav.init(sideNav, {});

// Slider
const slider = document.querySelector(".slider");
M.Slider.init(slider, {
  indicators: false,
  height: 500,
  transition: 500,
  interval: 6000,
});

// Scrollspy
const ss = document.querySelectorAll(".scrollspy");
M.ScrollSpy.init(ss, {});

// Material Boxed
const mb = document.querySelectorAll(".materialboxed");
M.Materialbox.init(mb, {});

// Auto Complete
const ac = document.querySelector(".autocomplete");
M.Autocomplete.init(ac, {
  data: {
    Aruba: null,
    "Cancun Mexico": null,
    Hawaii: null,
    Florida: null,
    California: null,
    Jamaica: null,
    Europe: null,
    "The Bahamas": null,
  },
});

//  NAVIGACIQ
var open = document.getElementById("hamburger");
var changeIcon = true;

open.addEventListener("click", function () {
  var overlay = document.querySelector(".overlay");
  var nav = document.querySelector("nav");
  var icon = document.querySelector(".menu-toggle i");

  overlay.classList.toggle("menu-open");
  nav.classList.toggle("menu-open");

  if (changeIcon) {
    icon.classList.remove("fa-bars");
    icon.classList.add("fa-times");

    changeIcon = false;
  } else {
    icon.classList.remove("fa-times");
    icon.classList.add("fa-bars");
    changeIcon = true;
  }
});
