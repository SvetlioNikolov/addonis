package com.addonis.servicetests;

import com.addonis.Factory;
import com.addonis.exceptions.DuplicateEntityException;
import com.addonis.exceptions.EntityNotFoundException;
import com.addonis.models.*;
import com.addonis.repositories.*;
import com.addonis.services.AddonsServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.addonis.Factory.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class AddonsServiceTests {

    @Mock
    AddonsRepository addonsRepository;

    @Mock
    StatusRepository statusRepository;

    @Mock
    TagsRepository tagsRepository;

    @Mock
    GitHubDatasRepository gitHubDatasRepository;

    @Mock
    UserRepository userRepository;

    @InjectMocks
    AddonsServiceImpl service;

    @Test
    public void getByIdShould_ReturnAddon_WhenAddonExists() {

        //Arrange
        Addon expectedAddon = createAddon();

        when(addonsRepository.findAddonById(anyInt()))
                .thenReturn(expectedAddon);
        //Act
        Addon returnedAddon = service.getById(1);
        //Assert
        Assert.assertSame(expectedAddon, returnedAddon);

    }

    @Test
    public void getByIdShould_Throw_WhenAddonDoesntExist(){

        when(addonsRepository.findAddonById(anyInt()))
                .thenReturn(null);

        Assert.assertThrows(EntityNotFoundException.class,
                () -> service.getById(anyInt()));

    }

    @Test
    public void getByIdShould_Throw_WhenBeerDoesNotExists() {


        //Arrange
        when(addonsRepository.findAddonById(anyInt()))
                .thenThrow(EntityNotFoundException.class);

        //Act,Assert
        Assert.assertThrows(EntityNotFoundException.class,
                () -> service.getById(anyInt()));

    }

    @Test
    public void getAllByStatusShould_ReturnAllAddons_WhenAddonsExist() {

        Status status = createStatus();
        Addon addon = createAddon();

        when(statusRepository.findByName(status.getName()))
                .thenReturn(status);
        when(addonsRepository.findByStatus(status, Sort.by("name")))
                .thenReturn(Collections.singletonList(addon));

        Assert.assertEquals(1, service.getAllByStatus(status.getName(), "name").size());
    }

    @Test
    public void getAllByCreatorShould_ReturnAllAddons_WhenAddonsExist() {
        Addon addon = createAddon();

        when(statusRepository.findByName(addon.getStatus().getName()))
                .thenReturn(addon.getStatus());
        when(addonsRepository.findByCreatorAndStatus(addon.getCreator(), addon.getStatus()))
                .thenReturn(Collections.singletonList(addon));
        List<Addon> expectedList = service.getByCreator(addon.getCreator());

        Assert.assertEquals(1, expectedList.size());
    }


    @Test
    public void getAllByTagShould_ReturnAllAddons_WhenAddonsExist() {

        when(addonsRepository.findByTagsLikeAndStatus("Tag1", Sort.by("name")))
                .thenReturn(Collections.singletonList(createAddon()));

        Assert.assertEquals(1, service.getByTag("Tag1", "name").size());
    }

    @Test
    public void getNewestShould_ReturnNewestAddons_WhenAddonsExist() {
        Addon addon = createAddon();

        when(addonsRepository.findByStatusOrderByUpload_dateDesc())
                .thenReturn(Collections.singletonList(addon));
        List<Addon> expectedList = service.getNewest();


        Assert.assertEquals(1, expectedList.size());
    }

    @Test
    public void getMostPopularShould_ReturnMostPopularAddons_WhenAddonsExist() {
        Addon addon = createAddon();
        Status status = createStatus();

        when(statusRepository.findByName(status.getName()))
                .thenReturn(status);
        when(addonsRepository.findTop5ByStatusOrderByDownloadsDesc(status))
                .thenReturn(Collections.singletonList(addon));
        List<Addon> expectedList = service.getMostPopular();

        Assert.assertEquals(1, expectedList.size());
    }

    @Test
    public void getFeaturedShould_ReturnFeaturedAddons_WhenAddonsExist() {
        Addon addon = createAddon();
        Status status = createStatus();

        when(statusRepository.findByName(status.getName()))
                .thenReturn(status);
        when(addonsRepository.findTop5ByStatusAndFeaturedIsTrue(status))
                .thenReturn(Collections.singletonList(addon));
        List<Addon> expectedList = service.getFeatured();

        Assert.assertEquals(1, expectedList.size());
    }


    //TODO TEST
//    @Test
//    public void getFilteredByNameAndIdeShould_ReturnAllAddonsFiltered_WhenAddonsExist() {
//        Addon addon = createAddon();
//
//        when(addonsRepository.findAddonsByNameAndIdeAndApproved(addon.getName(), addon.getIde().getName(), Sort.by("name")))
//                .thenReturn(Collections.singletonList(addon));
//        List<Addon> expectedList = service.getFilteredByNameAndIdeAndApproved(addon.getName(), addon.getIde().getName(), "name");
//
//        Assert.assertEquals(1, expectedList.size());
//    }

    @Test
    public void createTagShould_Throw_WhenTagAlreadyExist() {
        //Arrange
         when(tagsRepository.existsByName(anyString()))
                .thenReturn(true);

        //Act,Assert
        Assert.assertThrows(DuplicateEntityException.class,
                () -> service.createTag(createTag()));

    }

    @Test
    public void createTagShould_Create_WhenTagDoesNotExist() {
        when(tagsRepository.existsByName(anyString()))
                .thenReturn(false);
        service.createTag(createTag());

        verify(tagsRepository, times(1))
                .save(any(Tag.class));

    }

    @Test
    public void createAddonShould_Create_WhenAddonDoesNotExist(){
        Addon addon = createAddon();

        when(addonsRepository.existsByNameAndIdNot(addon.getName(), addon.getId()))
                .thenReturn(false);

//        when(gitHubDatasRepository.save(addon.getGitHubData()))
//                .thenReturn(addon.getGitHubData());

        service.create(addon);

        verify(addonsRepository, times(1))
                .save(any(Addon.class));
    }

    @Test
    public void createShould_ThrowException_WhenAddonAlreadyExist() {


        when(addonsRepository.existsByNameAndIdNot(anyString(), anyInt()))
                .thenReturn(true);

        Assert.assertThrows(DuplicateEntityException.class,
                () -> service.create(createAddon()));
    }

    @Test
    public void updateShould_Update_WhenAddonDoesntExist(){

        Addon addon = createAddon();

        when(addonsRepository.existsById(anyInt()))
                .thenReturn(1);
        when(addonsRepository.existsByNameAndIdNot(anyString(), anyInt()))
                .thenReturn(false);
        when(addonsRepository.save(addon))
                .thenReturn(addon);

        service.update(addon, addon.getCreator());
        verify(addonsRepository, times(1))
                .save(addon);

    }
    @Test
    public void updateShould_Throw_WhenAddonExist(){

        Addon addon = createAddon();
        User user = createUser();
        user.setUsername("fiki");

        when(addonsRepository.existsById(anyInt()))
                .thenReturn(1);
        when(addonsRepository.existsByNameAndIdNot(anyString(), anyInt()))
                .thenReturn(false);

        Assert.assertThrows(AccessDeniedException.class,
                () -> service.update(addon, user));
    }

    @Test
    public void updateDownloadsShould_UpdateDownloads_WhenAddonExists() {
        Addon addon = createAddon();

        when(addonsRepository.existsById(anyInt()))
                .thenReturn(1);
        service.updateDownloads(addon);

        verify(addonsRepository, times(1)).save(addon);

    }

    @Test
    public void updateDownloadsShould_Throw_WhenAddonDoesntExist() {
        Addon addon = createAddon();

        when(addonsRepository.existsById(anyInt()))
                .thenReturn(0);

        Assert.assertThrows(EntityNotFoundException.class,
                () -> service.updateDownloads(addon));

    }

    @Test
    public void deleteShould_Delete_WhenAddonExist(){
        Addon addon = createAddon();
        Status status = createStatus();
        status.setName("deleted");

        when(addonsRepository.existsById(addon.getId()))
                .thenReturn(1);
        when(userRepository.findUserByUsername(addon.getCreator().getUsername()))
                .thenReturn(addon.getCreator());
        when(statusRepository.findByName(status.getName()))
                .thenReturn(status);
        when(addonsRepository.findByIdAndStatusNot(addon.getId(), status))
                .thenReturn(addon);

        service.delete(addon.getId(), addon.getCreator().getUsername());
        verify(addonsRepository, times(1)).save(addon);
    }

    @Test
    public void deleteShould_Throw_WhenAccessDenied() {
        Addon addon = createAddon();
        User user = createUser();
        user.setUsername("gosho");
        Status status = createStatus();
        status.setName("deleted");

        when(addonsRepository.existsById(addon.getId()))
                .thenReturn(1);
        when(userRepository.findUserByUsername(user.getUsername()))
                .thenReturn(user);
        when(statusRepository.findByName(status.getName()))
                .thenReturn(status);
        when(addonsRepository.findByIdAndStatusNot(addon.getId(), status))
                .thenReturn(addon);

        Assert.assertThrows(AccessDeniedException.class,
                () -> service.delete(addon.getId(), user.getUsername()));

    }

}
