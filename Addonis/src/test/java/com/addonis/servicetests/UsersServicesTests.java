package com.addonis.servicetests;

import com.addonis.exceptions.DuplicateEntityException;
import com.addonis.exceptions.EntityNotFoundException;
import com.addonis.models.User;
import com.addonis.models.UserDetails;
import com.addonis.repositories.AddonsRepository;
import com.addonis.repositories.UserDetailsRepository;
import com.addonis.repositories.UserRepository;
import com.addonis.services.UsersServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.addonis.Factory.createUser;
import static com.addonis.Factory.createUserDetails;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class UsersServicesTests {

    @Mock
    UserRepository userRepository;

    @Mock
    UserDetailsRepository userDetailsRepository;

    @Mock
    PasswordEncoder passwordEncoder;

    @Mock
    UserDetailsManager userDetailsManager;

    @InjectMocks
    UsersServiceImpl mockService;


    @Test
    public void getAllShould_ReturnAllUsers_WhenUsersExist() {

        when(userRepository.findAllByEnabledIsTrue())
                .thenReturn(Collections.singletonList(createUser()));

        Assert.assertEquals(1, mockService.getAll().size());
    }

    @Test
    public void getUsers_ShouldReturnEmptyList_WhenUsersDoesNotExist() {

        when(userRepository.findAllByEnabledIsTrue())
                .thenReturn(Collections.emptyList());

        Assert.assertEquals(0, mockService.getAll().size());
    }

    @Test
    public void getByUsernameShould_ReturnUser_WhenUserExists() {

        //Arrange
        User expectedUser = createUser();

        when(userRepository.findUserByUsernameAndEnabledIsTrue(expectedUser.getUsername()))
                .thenReturn(expectedUser);

        User returnedUser = mockService.getByUsername(expectedUser.getUsername());

        Assert.assertEquals(expectedUser, returnedUser);

    }

    @Test
    public void getByUsernameShould_ThrowException_WhenUserDoesntExist() {

        //Arrange
        when(userRepository.findUserByUsernameAndEnabledIsTrue(anyString()))
                .thenReturn(null);
        //Act,Assert
        Assert.assertThrows(EntityNotFoundException.class,
                () -> mockService.getByUsername(anyString()));
    }

    @Test
    public void createShould_CreateUser_WhenUserDoesntExist() {
        //Arrange
        User user = createUser();
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.UserDetails userDetails =
                new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),authorities);

        when(userDetailsManager.userExists(user.getUsername()))
                .thenReturn(false);
//        when(userDetailsRepository.findByEmailIgnoreCase(user.getUserDetails().getEmail()))
//                .thenReturn(null);
        when(passwordEncoder.encode(user.getPassword()))
                .thenReturn(anyString());

        mockService.create(user);

        verify(userDetailsManager, times(1))
                .createUser(userDetails);

    }

    @Test
    public void createShould_ThrowException_WhenUserWithUsernameAlreadyExists() {

        when(userDetailsManager.userExists(anyString()))
                .thenReturn(true);

        Assert.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(createUser()));
    }

    @Test
    public void createShould_ThrowException_WhenUserWithEmailAlreadyExists() {

//        when(userDetailsRepository.findByEmailIgnoreCase(anyString()))
//                .thenReturn(createUserDetails());
        when(userDetailsRepository.existsByEmail(anyString()))
                .thenReturn(true);

        Assert.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(createUser()));
    }



    @Test
    public void updateShould_UpdateUserDetails_WhenUserExists() {
        User user = createUser();

        when(userRepository.existsById(anyString()))
                .thenReturn(true);

        mockService.update(user, "username");
        verify(userDetailsRepository, times(1)).save(user.getUserDetails());

    }


    @Test
    public void updateShould_UpdateUser_WhenUserExists() {
        User user = createUser();

        when(userRepository.existsById(anyString()))
                .thenReturn(true);

        mockService.update(user,"username");
        verify(userRepository, times(1)).save(user);

    }

    @Test
    public void updateShould_ThrowException_WhenUserDoesntExist() {


        when(userRepository.existsById(anyString()))
                .thenReturn(false);

        Assert.assertThrows(EntityNotFoundException.class,
                () -> mockService.update(createUser(), "username"));
    }

    @Test
    public void deleteShould_DeleteUser_WhenUserExists() {
        User user = createUser();

//        when(userRepository.existsById(user.getUsername()))
//                .thenReturn(true);
        when(userRepository.findUserByUsername(user.getUsername()))
                .thenReturn(user);
        when(userRepository.findUserByUsernameAndEnabledIsTrue(user.getUsername()))
                .thenReturn(user);

        mockService.delete(user.getUsername(),user.getUsername());
        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void deleteShould_ThrowException_WhenUserDoesntExist() {

        User user =createUser();

//        when(userRepository.existsById(user.getUsername()))
//                .thenReturn(false);

        Assert.assertThrows(EntityNotFoundException.class,
                () -> mockService.delete(user.getUsername(), user.getUsername()));
    }

    @Test
    public void deleteShould_ThrowException_WhenUserDoesntHaveRightsToDeleteExist() {

    }

    @Test
    public void getByUserDetailsShould_CallRepository() {

        //Arrange
        User expectedUser = createUser();

        when(userRepository.findUserByUserDetails(expectedUser.getUserDetails()))
                .thenReturn(expectedUser);

        User returnedUser = mockService.getByUserDetails(expectedUser.getUserDetails());

        Assert.assertEquals(expectedUser, returnedUser);
    }

    @Test
    public void getByUserDetailsShould_ThrowException_WhenUserDoesntExist() {

        User user = createUser();
        //Arrange
        when(userRepository.findUserByUserDetails(user.getUserDetails()))
                .thenReturn(null);
        //Act,Assert
        Assert.assertThrows(EntityNotFoundException.class,
                () -> mockService.getByUserDetails(user.getUserDetails()));
    }
}
