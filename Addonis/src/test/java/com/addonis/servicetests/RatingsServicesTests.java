package com.addonis.servicetests;

import com.addonis.exceptions.DuplicateEntityException;
import com.addonis.exceptions.EntityNotFoundException;
import com.addonis.models.Addon;
import com.addonis.models.Rating;
import com.addonis.models.User;
import com.addonis.repositories.RatingsRepository;
import com.addonis.services.RatingsServiceImpl;
import com.addonis.services.contracts.RatingsService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.addonis.Factory.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class RatingsServicesTests {

    @Mock
    RatingsRepository ratingRepository;

    @InjectMocks
    RatingsServiceImpl mockService;

    @Test
    public void getAverageRatingShould_CallRepository() {

        when(ratingRepository.findAverageRating(anyInt()))
                .thenReturn(createRating().getRating_value());

        mockService.getAverageRating(anyInt());
        //Assert
        verify(ratingRepository,
                times(1)).findAverageRating(anyInt());



    }

    @Test
    public void checkRatingExistsShould_CallRepository(){

        Rating rating = createRating();

        when(ratingRepository.existsByUserIsAndAddonIs(rating.getUser(),rating.getAddon()))
                .thenReturn(true);
        mockService.getRating(rating.getUser(),rating.getAddon());

        //Assert
        verify(ratingRepository,
                times(1)).existsByUserIsAndAddonIs(rating.getUser(),rating.getAddon());

    }

    @Test
    public void getRatingShould_ThrowException_WhenRatingExists(){

        User user = createUser();
        Addon addon =createAddon();

        when(ratingRepository.existsByUserIsAndAddonIs(user,addon))
                .thenReturn(false);
        //Act,Assert
        Assert.assertThrows(EntityNotFoundException.class,
                () -> mockService.getRating(user,addon));
    }

    @Test
    public void createShould_CallRepository(){

        Rating rating = createRating();

        when(ratingRepository.save(rating))
                .thenReturn(rating);

        mockService.create(rating);

        verify(ratingRepository, times(1))
                .save(any(Rating.class));
    }

    @Test
    public void createShould_ThrowException_WhenRatingAlreadyExists(){
        //Arrange
        Rating expectedRating = createRating();

        when(ratingRepository.existsByUserIsAndAddonIs(expectedRating.getUser(),expectedRating.getAddon()))
                .thenReturn(true);
        //Act,Assert
        Assert.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(expectedRating));


    }
}
