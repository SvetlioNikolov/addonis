package com.addonis;

import com.addonis.models.*;

import java.util.HashSet;
import java.util.Set;

public class Factory {

    public static Addon createAddon() {

        Addon addon = new Addon();
        addon.setId(1);
        addon.setName("Addon");
        addon.setDescription("Description");
        addon.setDownloads(1);
        addon.setOrigin_link("https://github.com/madskristensen/AddAnyFile");
        addon.setStatus(createStatus());
        addon.setCreator(createUser());
        addon.setGitHubData(createGitHubData());
        addon.setIde(createIde());
        addon.setTags(createTags());


        return addon;
    }

    public static GitHubData createGitHubData() {

        GitHubData gitHubData = new GitHubData();
        gitHubData.setId(1);
        gitHubData.setIssues(1);
        gitHubData.setLastCommitTitle("Title");
        gitHubData.setPulls(1);

        return gitHubData;
    }

    public static Ide createIde() {

        Ide ide = new Ide();
        ide.setId(1);
        ide.setName("Eclipse");

        return ide;
    }

    public static Status createStatus() {

        Status status = new Status();
        status.setId(1);
        status.setName("approved");

        return status;
    }

    public static Rating createRating() {

        Rating rating = new Rating();
        rating.setId(1);
        rating.setAddon(createAddon());
        rating.setUser(createUser());
        rating.setRating_value(1);

        return rating;
    }

    public static UserDetails createUserDetails() {

        UserDetails userDetails = new UserDetails();
        userDetails.setFirstName("Parvo ime");
        userDetails.setLastName("Posledno ime");
        userDetails.setEmail("Email");
        userDetails.setId(1);

        return userDetails;
    }
    public static User createUser() {

        User user = new User();
        user.setUsername("username");
        user.setEnabled(true);
        user.setPassword("1234");
       // user.setAuthorities(createAuthority());
        user.setUserDetails(createUserDetails());

        return user;
    }


    public static Set<Tag> createTags() {
        Set<Tag> tags = new HashSet<>();
        Tag tag = new Tag();
        tag.setId(1);
        tag.setName("Tag");
        tags.add(tag);

        return tags;
    }

    public static Tag createTag() {
        Tag tag = new Tag();

        tag.setId(1);
        tag.setName("Tag");

        return tag;
    }

    public static  Set<Authority> createAuthority() {
        Set<Authority> authorities = new HashSet<>();
        Authority authority = new Authority();
        authority.setUser(createUser());
        authority.setAuthority("ROLE_USER");

        return authorities;
    }
}
