# Addonis

Addonis is an Addons Registry web application. A web application, which helps users 
manage their IDE add-ons. The system has three parts : 

1.   Admin: he approves new add-ons and manages user and their add-ons. 
2.   User: publishes, rates and downloads add-ons from the landing page
3.   Guest: browses, filters,sorts and downloads add-ons.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The minimum JDK version is [1.8](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html)


### Deployment

Step 1) Make sure you have at least JDK 1.8

Step 2) Copy the repository on your machine

Step 3) Import it on your IDE (IntelliJ, Eclipse,etc..)

Step 4) Run both script (for creating and importing the date to the database)

Step 5) Enjoy and expand the project :)

## Running the tests

Right click on the folder tests and click Run or Run with Coverage.

### And coding style tests

These JUnit tests test each of the services. Their aim is to make sure that the 

service as a single usit works as it is supposed to.

## Built With

* [Spring](https://spring.io/) - The framework used
* [Gradle](https://gradle.org/) - Dependency Management
* [Hibernate](http://hibernate.org/) - Mapping an object-oriented domain model to a relational database.
* [JPA](https://spring.io/projects/spring-data-jpa) - Used to store and get data into/from a database.
* [Thymeleaf](https://www.thymeleaf.org/) - Bring elegant natural templates to your development workflow

## Screenshots
![index](/screenshots/index.png/)
![index2](/screenshots/index2.png/)
![logIN](/screenshots/log_in.png/)
![register](/screenshots/register.png/)
![addons](/screenshots/addons.png/)
![create](/screenshots/create_addon.png/)
![editAddon](/screenshots/edit_addon.png/)
![details](/screenshots/addon_details.png/)
![details2](/screenshots/addon_details2.png/)
![profile](/screenshots/profile_addons.png/)
![editProfile](/screenshots/edit_profile.png/)
![admin](/screenshots/admin_all_addons.png/)
![admin2](/screenshots/admin_all_users.png/)
## Authors

* **Svetlozar Nikolov** 
* **Kristiyan Mihov**
