INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (1, 12, '2020-03-11 19:15:37', 'Merge pull request #81 from nicolasstucki/fix-#79-2

Fix #79: Revert vsix files on release', 43, '# Scala Syntax (official)

[![Install extension](https://img.shields.io/badge/scala-vscode-blue.png)](vscode:extension/scala-lang.scala)
[![Version](https://img.shields.io/github/package-json/v/scala/vscode-scala-syntax.svg)](https://marketplace.visualstudio.com/items?itemName=scala-lang.scala)
[![Build Status](https://travis-ci.org/scala/vscode-scala-syntax.svg?branch=master)](https://travis-ci.org/scala/vscode-scala-syntax)

Visual Studio Code extension providing syntax highlighting for Scala source
files.

![Syntax Highlighting Demo](https://i.imgur.com/TDx0mC3.png)

This repo also powers the Scala syntax highlighting on GitHub. (It is vendored in [github/linguist](https://github.com/github/linguist).)

### Team

The current maintainers (people who can merge pull requests) are:

- Olafur Pall Geirsson - [`@olafurpg`](https://github.com/olafurpg)
- Nicolas Stucki- [`@nicolasstucki`](https://github.com/nicolasstucki)
- Vitalii Voloshyn - [`@PanAeon`](https://github.com/PanAeon)
- all other members of the Scala organization on GitHub

## Based on

- Plugin: https://github.com/daltonjorge/vscode-scala
- Template: https://github.com/sellmerfud/scala.tmbundle
  (https://github.com/mads-hartmann/scala.tmbundle)
- Textmate JSON schema:
  https://github.com/Septh/tmlanguage/blob/master/tmLanguage.schema.json

## License

[MIT](LICENSE.md)
');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (2, 17, '2020-02-21 21:26:57', 'Create file system watcher for Java files', 3, '# Kotlin IDE for Visual Studio Code
Smart code completion, linting, debugging, formatting and more for Kotlin in VSCode using the [Kotlin language server](https://github.com/fwcd/kotlin-language-server) and the [Kotlin debug adapter](https://github.com/fwcd/kotlin-debug-adapter).

[![Version](https://img.shields.io/visual-studio-marketplace/v/fwcd.kotlin)](https://marketplace.visualstudio.com/items?itemName=fwcd.kotlin)
[![Build Status](https://travis-ci.org/fwcd/vscode-kotlin.svg?branch=master)](https://travis-ci.org/fwcd/vscode-kotlin)
[![Downloads](https://img.shields.io/visual-studio-marketplace/d/fwcd.kotlin)](https://marketplace.visualstudio.com/items?itemName=fwcd.kotlin)
[![Installs](https://img.shields.io/visual-studio-marketplace/i/fwcd.kotlin)](https://marketplace.visualstudio.com/items?itemName=fwcd.kotlin)

To use, open a Kotlin file inside a Gradle or Maven project. The language server will then automatically launch in the background.

## Features
* Code completion
* Linting
* Debugging
* Go-to-definition
* Signature help
* Hover
* Formatting
* Document symbols
* Find references

## Usage

### Debugging
* Setup:
    * Open the `launch.json` file in your project and invoke code completion to create a new launch configuration (or select `Add Configuration...` in the debug tab)
* Launch:
    * Build your project (before every launch)
	* Click the `Run` button in the `Debug` tab or press `F5`
');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (3, 89, '2020-05-02 02:18:14', 'Provide unique id for treeitem (#1935)', 684, '## Docker for Visual Studio Code

[![Version](https://vsmarketplacebadge.apphb.com/version/ms-azuretools.vscode-docker.svg)](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker) [![Installs](https://vsmarketplacebadge.apphb.com/installs-short/ms-azuretools.vscode-docker.svg)](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker) [![Build Status](https://dev.azure.com/ms-azuretools/AzCode/_apis/build/status/Nightly/vscode-docker-nightly-2?branchName=master)](https://dev.azure.com/ms-azuretools/AzCode/_build/latest?definitionId=22&branchName=master)

The Docker extension makes it easy to build, manage and deploy containerized applications from Visual Studio Code.

**Check out [Working with containers](https://aka.ms/AA7arez) topic on Visual Studio Code documentation site to get started**.

[The Docker extension wiki](https://github.com/Microsoft/vscode-docker/wiki) has troubleshooting tips and additional technical information.

## Installation

[Install Docker](https://docs.docker.com/install/) on your machine and add it to the system path.

On Linux, you should also [enable Docker CLI for the non-root user account](https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user) that will be used to run VS Code.

To install the extension, open the Extensions view, search for `docker` to filter results and select Docker extension authored by Microsoft.

## Overview of the extension features

### Editing Docker files

You can get IntelliSense when editing your `Dockerfile` and `docker-compose.yml` files, with completions and syntax help for common commands.

![IntelliSense for Dockerfiles](resources/readme/dockerfile-intellisense.png)

In addition, you can use the Problems panel (<kbd>Ctrl+Shift+M</kbd> on Windows/Linux, <kbd>Shift+Command+M</kbd> on Mac) to view common errors for `Dockerfile` and `docker-compose.yml` files.

### Generating Docker files

You can add Docker files to your workspace by opening the Command Palette (<kbd>F1</kbd>) and using **Docker: Add Docker Files to Workspace** command. The command will generate `Dockerfile` and `.dockerignore` files and add them to your workspace. The command will also query you if you want the Docker Compose files added as well; this is optional.

The extension recognizes workspaces that use most popular development languages (C#, Node.js, Python, Ruby, Go, and Java) and customizes generated Docker files accordingly.

### Docker view

The Docker extension contributes a Docker view to VS Code. The Docker view lets you examine and manage Docker assets: containers, images, volumes, networks, and container registries. If the Azure Account extension is installed, you can browse your Azure Container Registries as well.

The right-click menu provides access to commonly used commands for each type of asset.

![Docker view context menu](resources/readme/docker-view-context-menu.gif)

You can rearrange the Docker view panes by dragging them up or down with a mouse and use the context menu to hide or show them.

![Customize Docker view](resources/readme/docker-view-rearrange.gif)

### Docker commands

Many of the most common Docker commands are built right into the Command Palette:

![Docker commands](resources/readme/command-palette.png)

You can run Docker commands to manage [images](https://docs.docker.com/engine/reference/commandline/image/), [networks](https://docs.docker.com/engine/reference/commandline/network/), [volumes](https://docs.docker.com/engine/reference/commandline/volume/), [image registries](https://docs.docker.com/engine/reference/commandline/push/), and [Docker Compose](https://docs.docker.com/compose/reference/overview/). In addition, the **Docker: Prune System** command will remove stopped containers, dangling images, and unused networks and volumes.


### Docker Compose

[Docker Compose](https://docs.docker.com/compose/) lets you define and run multi-container applications with Docker. Visual Studio Code''s experience for authoring `docker-compose.yml` is very rich, providing IntelliSense for valid Docker compose directives:

 ![Docker Compose IntelliSense](resources/readme/dockercomposeintellisense.png)

For the `image` directive, you can press <kbd>ctrl+space</kbd> and VS Code will query the Docker Hub index for public images:

 ![Docker Compose image suggestions](resources/readme/dockercomposeimageintellisense.png)

VS Code will first show a list of popular images along with metadata such as the number of stars and description. If you continue typing, VS Code will query the Docker Hub index for matching images, including searching public profiles. For example, searching for ''Microsoft'' will show you all the public Microsoft images.

 ![Docker Compose Microsoft image suggestions](resources/readme/dockercomposesearch.png)


### Using image registries

You can display the content and push/pull/delete images from [Docker Hub](https://hub.docker.com/) and [Azure Container Registry](https://docs.microsoft.com/azure/container-registry/):

![Azure Container Registry content](resources/readme/container-registry.png)

An image in an Azure Container Registry can be deployed to Azure App Service directly from VS Code; see [Deploy images to Azure App Service](https://aka.ms/AA7arf8) page. For more information about how to authenticate to and work with registries see [Using container registries](https://aka.ms/AA7arf9) page.

### Debugging services running inside a container

You can debug services built using .NET (C#) and Node.js that are running inside a container. The extension offers custom tasks that help with launching a service under the debugger and with attaching the debugger to a running service instance. For more information see [Debug container application](https://aka.ms/AA7arfb)  and [Extension Properties and Tasks](https://aka.ms/AA7ay8l) pages.

### Azure CLI integration

You can start Azure CLI (command-line interface) in a standalone, Linux-based container with **Docker Images: Run Azure CLI** command. This allows access to full Azure CLI command set in an isolated environment. See [Get started with Azure CLI](https://docs.microsoft.com/cli/azure/get-started-with-azure-cli?view=azure-cli-latest#sign-in) page for more information on available commands.

## Contributing

See [the contribution guidelines](CONTRIBUTING.md) for ideas and guidance on how to improve the extension. Thank you!

### Code of Conduct

This project has adopted the [Microsoft Open Source Code of Conduct](https://opensource.microsoft.com/codeofconduct/). For more information see the [Code of Conduct FAQ](https://opensource.microsoft.com/codeofconduct/faq/) or contact [opencode@microsoft.com](mailto:opencode@microsoft.com) with any additional questions or comments.

## Telemetry

VS Code collects usage data and sends it to Microsoft to help improve our products and services. Read our [privacy statement](https://go.microsoft.com/fwlink/?LinkID=528096&clcid=0x409) to learn more. If you don’t wish to send usage data to Microsoft, you can set the `telemetry.enableTelemetry` setting to `false`. Learn more in our [FAQ](https://code.visualstudio.com/docs/supporting/faq#_how-to-disable-telemetry-reporting).

## License

[MIT](LICENSE.md)
');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (4, 0, '2020-05-01 16:16:18', 'Prepare v3.10.0', 2, '## Introduction

This [VS Code](https://code.visualstudio.com/) extension adds support for
effectively editing, refactoring, running, and reloading [Flutter](https://flutter.io/)
mobile apps, as well as support for the [Dart](https://www.dartlang.org/) programming
language.

![Flutter hot reload example](https://dartcode.org/images/marketplace/flutter_hot_reload.gif)

Note: Projects should be run using `F5` or the `Debug` menu for full debugging functionality. Running from the built-in terminal will not provide all features.

## Installation

[Install from the Visual Studio Code Marketplace](https://marketplace.visualstudio.com/items?itemName=Dart-Code.flutter) or by [searching within VS Code](https://code.visualstudio.com/docs/editor/extension-gallery#_search-for-an-extension).

## Documentation

Please see the [Flutter documentation for using VS Code](https://flutter.io/using-ide-vscode/).

## Reporting Issues

Issues should be reported in the [Dart-Code issue tracker](https://github.com/Dart-Code/Dart-Code/issues).
');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (5, 846, '2020-05-03 00:13:04', 'bump version', 1709, '<h2 align="center"><img src="https://raw.githubusercontent.com/VSCodeVim/Vim/master/images/icon.png" height="128"><br>VSCodeVim</h2>
<p align="center"><strong>Vim emulation for Visual Studio Code</strong></p>

[![http://aka.ms/vscodevim](https://vsmarketplacebadge.apphb.com/version/vscodevim.vim.svg)](http://aka.ms/vscodevim)
[![](https://vsmarketplacebadge.apphb.com/installs-short/vscodevim.vim.svg)](https://marketplace.visualstudio.com/items?itemName=vscodevim.vim)
[![https://travis-ci.org/VSCodeVim/Vim](https://travis-ci.org/VSCodeVim/Vim.svg?branch=master)](https://travis-ci.org/VSCodeVim/Vim)
[![https://vscodevim.herokuapp.com/](https://img.shields.io/badge/vscodevim-slack-blue.svg?logo=slack)](https://vscodevim.herokuapp.com/)

VSCodeVim is a Vim emulator for [Visual Studio Code](https://code.visualstudio.com/).

- ? For a full list of supported Vim features, please refer to our [roadmap](ROADMAP.md).
- ? Our [change log](CHANGELOG.md) outlines the breaking/major/minor updates between releases.
- ? If you need to ask any questions, join us on [Slack](https://vscodevim.herokuapp.com/)
- Report missing features/bugs on [GitHub](https://github.com/VSCodeVim/Vim/issues).

<details>
 <summary><strong>Table of Contents</strong> (click to expand)</summary>

- [Installation](#-installation)
  - [Mac setup](#mac)
  - [Windows setup](#windows)
  - [Linux setup](#linux-setup)
- [Settings](#%EF%B8%8F-settings)
  - [VSCodeVim settings](#vscodevim-settings)
  - [Neovim Integration](#neovim-integration)
  - [Key remapping](#key-remapping)
  - [Vim settings](#vim-settings)
- [Multi-Cursor mode](#%EF%B8%8F-multi-cursor-mode)
- [Emulated plugins](#-emulated-plugins)
  - [vim-airline](#vim-airline)
  - [vim-easymotion](#vim-easymotion)
  - [vim-surround](#vim-surround)
  - [vim-commentary](#vim-commentary)
  - [vim-indent-object](#vim-indent-object)
  - [vim-sneak](#vim-sneak)
  - [CamelCaseMotion](#camelcasemotion)
  - [Input Method](#input-method)
  - [ReplaceWithRegister](#replacewithregister)
  - [vim-textobj-entire](#vim-textobj-entire)
- [VSCodeVim tricks](#-vscodevim-tricks)
- [F.A.Q / Troubleshooting](#-faq)
- [Contributing](#?-contributing)

</details>

## ? Installation

VSCodeVim is automatically enabled following [installation](https://marketplace.visualstudio.com/items?itemName=vscodevim.vim) and reloading of VS Code.

### Mac

To enable key-repeating execute the following in your Terminal and restart VS Code:

```sh
$ defaults write com.microsoft.VSCode ApplePressAndHoldEnabled -bool false         # For VS Code
$ defaults write com.microsoft.VSCodeInsiders ApplePressAndHoldEnabled -bool false # For VS Code Insider
$ defaults delete -g ApplePressAndHoldEnabled                                      # If necessary, reset global default
```

We also recommend increasing Key Repeat and Delay Until Repeat settings in _System Preferences -> Keyboard_.

### Windows

Like real vim, VSCodeVim will take over your control keys. This behaviour can be adjusted with the [`useCtrlKeys`](#vscodevim-settings) and [`handleKeys`](#vscodevim-settings) settings.

## ?? Settings

The settings documented here are a subset of the supported settings; the full list is described in the `Contributions` tab of VSCodeVim''s [extension details page](https://code.visualstudio.com/docs/editor/extension-gallery#_extension-details), which can be found in the [extensions view](https://code.visualstudio.com/docs/editor/extension-gallery) of VS Code.

### Quick Example

Below is an example of a [settings.json](https://code.visualstudio.com/Docs/customization/userandworkspace) file with settings relevant to VSCodeVim:

```json
{
  "vim.easymotion": true,
  "vim.incsearch": true,
  "vim.useSystemClipboard": true,
  "vim.useCtrlKeys": true,
  "vim.hlsearch": true,
  "vim.insertModeKeyBindings": [
    {
      "before": ["j", "j"],
      "after": ["<Esc>"]
    }
  ],
  "vim.normalModeKeyBindingsNonRecursive": [
    {
      "before": ["<leader>", "d"],
      "after": ["d", "d"]
    },
    {
      "before": ["<C-n>"],
      "commands": [":nohl"]
    }
  ],
  "vim.leader": "<space>",
  "vim.handleKeys": {
    "<C-a>": false,
    "<C-f>": false
  }
}
```

### VSCodeVim settings

These settings are specific to VSCodeVim.

| Setting                          | Description                                                                                                                                                                                                                                                                                                                                                                                                                         | Type    | Default Value                         |
| -------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------- | ------------------------------------- |
| vim.changeWordIncludesWhitespace | Include trailing whitespace when changing word. This configures the <kbd>cw</kbd> action to act consistently as its siblings (<kbd>yw</kbd> and <kbd>dw</kbd>) instead of acting as <kbd>ce</kbd>.                                                                                                                                                                                                                                  | Boolean | false                                 |
| vim.cursorStylePerMode._{Mode}_  | Configure a specific cursor style for _{Mode}_. Omitted modes will use [default cursor type](https://github.com/VSCodeVim/Vim/blob/4a6fde6dbd4d1fac1f204c0dc27c32883651ef1a/src/mode/mode.ts#L34) Supported cursors: line, block, underline, line-thin, block-outline, and underline-thin.                                                                                                                                          | String  | None                                  |
| vim.digraphs._{shorthand}_       | Set custom digraph shorthands that can override the default ones. Entries should map a two-character shorthand to a descriptive string and one or more UTF16 code points. Example: `"R!": ["?", [55357, 56960]]`                                                                                                                                                                                                                   | Object  | `{"R!": ["?", [0xD83D, 0xDE80]]`     |  |
| vim.debug.silent                 | Boolean indicating whether log messages will be suppressed.                                                                                                                                                                                                                                                                                                                                                                         | Boolean | false                                 |
| vim.debug.loggingLevelForConsole | Maximum level of messages to log to console. Logs are visible in the [developer tools](https://code.visualstudio.com/docs/extensions/developing-extensions#_developer-tools-console). Supported values: ''error'', ''warn'', ''info'', ''verbose'', ''debug'').                                                                                                                                                                               | String  | error                                 |
| vim.debug.loggingLevelForAlert   | Maximum level of messages to present as VS Code information window. Supported values: ''error'', ''warn'', ''info'', ''verbose'', ''debug'').                                                                                                                                                                                                                                                                                                 | String  | error                                 |
| vim.disableExtension             | Disable VSCodeVim extension. This setting can also be toggled using `toggleVim` command in the Command Palette                                                                                                                                                                                                                                                                                                                      | Boolean | false                                 |
| vim.handleKeys                   | Delegate configured keys to be handled by VS Code instead of by the VSCodeVim extension. Any key in `keybindings` section of the [package.json](https://github.com/VSCodeVim/Vim/blob/master/package.json) that has a `vim.use<C-...>` in the `when` argument can be delegated back to VS Code by setting `"<C-...>": false`. Example: to use `ctrl+f` for find (native VS Code behaviour): `"vim.handleKeys": { "<C-f>": false }`. | String  | `"<C-d>": true`                       |
| vim.overrideCopy                 | Override VS Code''s copy command with our own, which works correctly with VSCodeVim. If cmd-c/ctrl-c is giving you issues, set this to false and complain [here](https://github.com/Microsoft/vscode/issues/217).                                                                                                                                                                                                                    | Boolean | false                                 |
| vim.searchHighlightColor         | Set the color of search highlights                                                                                                                                                                                                                                                                                                                                                                                                  | String  | `editor.findMatchHighlightBackground` |
| vim.startInInsertMode            | Start in Insert mode instead of Normal Mode                                                                                                                                                                                                                                                                                                                                                                                         | Boolean | false                                 |
| vim.gdefault                     | `/g` flag in a substitute command replaces all occurrences in the line. Without this flag, replacement occurs only for the first occurrence in each line. With this setting enabled, the `g` is on by default.                                                                                                                                                                                                                      | Boolean | false                                 |
| vim.useCtrlKeys                  | Enable Vim ctrl keys overriding common VS Code operations such as copy, paste, find, etc.                                                                                                                                                                                                                                                                                                                                           | Boolean | true                                  |
| vim.visualstar                   | In visual mode, start a search with `*` or `#` using the current selection                                                                                                                                                                                                                                                                                                                                                          | Boolean | false                                 |
| vim.highlightedyank.enable       | Enable highlighting when yanking                                                                                                                                                                                                                                                                                                                                                                                                    | Boolean | false                                 |
| vim.highlightedyank.color        | Set the color of yank highlights                                                                                                                                                                                                                                                                                                                                                                                                    | String  | rgba(250, 240, 170, 0.5)              |
| vim.highlightedyank.duration     | Set the duration of yank highlights                                                                                                                                                                                                                                                                                                                                                                                                 | Number  | 200                                   |

### Neovim Integration

> :warning: Experimental feature. Please leave feedback on neovim integration [here](https://github.com/VSCodeVim/Vim/issues/1735).

To leverage neovim for Ex-commands,

1.  Install [neovim](https://github.com/neovim/neovim/wiki/Installing-Neovim)
2.  Modify the following configurations:

| Setting          | Description                                                                                                             | Type    | Default Value |
| ---------------- | ----------------------------------------------------------------------------------------------------------------------- | ------- | ------------- |
| vim.enableNeovim | Enable Neovim                                                                                                           | Boolean | false         |
| vim.neovimPath   | Full path to neovim executable. If left empty, PATH environment variable will be automatically checked for neovim path. | String  |               |

Here''s some ideas on what you can do with neovim integration:

- [The power of g](http://vim.wikia.com/wiki/Power_of_g)
- [The :normal command](https://vi.stackexchange.com/questions/4418/execute-normal-command-over-range)
- Faster search and replace!

### Key Remapping

Custom remappings are defined on a per-mode basis.

#### `"vim.insertModeKeyBindings"`/`"vim.normalModeKeyBindings"`/`"vim.visualModeKeyBindings"`

- Keybinding overrides to use for insert, normal, and visual modes.
- Bind `jj` to `<Esc>` in insert mode:

```json
    "vim.insertModeKeyBindings": [
        {
            "before": ["j", "j"],
            "after": ["<Esc>"]
        }
    ]
```

- Bind `£` to goto previous whole word under cursor

```json
    "vim.normalModeKeyBindings": [
        {
            "before": ["£"],
            "after": ["#"]
        }
    ]
```

- Bind `:` to show the command palette:

```json
    "vim.normalModeKeyBindingsNonRecursive": [
        {
            "before": [":"],
            "commands": [
                "workbench.action.showCommands",
            ]
        }
    ]
```

- Bind `<leader>m` to add a bookmark and `<leader>b` to open the list of all bookmarks (using the [Bookmarks](https://marketplace.visualstudio.com/items?itemName=alefragnani.Bookmarks) extension):

```json
    "vim.normalModeKeyBindingsNonRecursive": [
        {
            "before": ["<leader>", "m"],
            "commands": [
                "bookmarks.toggle"
            ]
        },
        {
            "before": ["<leader>", "b"],
            "commands": [
                "bookmarks.list"
            ]
        }
    ]
```

- Bind `ZZ` to the vim command `:wq` (save and close the current file):

```json
    "vim.normalModeKeyBindingsNonRecursive": [
        {
            "before": ["Z", "Z"],
            "commands": [
                ":wq"
            ]
        }
    ]
```

- Bind `ctrl+n` to turn off search highlighting and `<leader>w` to save the current file:

```json
    "vim.normalModeKeyBindingsNonRecursive": [
        {
            "before":["<C-n>"],
            "commands": [
                ":nohl",
            ]
        },
        {
            "before": ["leader", "w"],
            "commands": [
                "workbench.action.files.save",
            ]
        }
    ]
```

- Bind `p` in visual mode to paste without overriding the current register

```json
    "vim.visualModeKeyBindingsNonRecursive": [
        {
            "before": [
                "p",
            ],
            "after": [
                "p",
                "g",
                "v",
                "y"
            ]
        }
    ],
```

- Bind `>` and `<` in visual mode to indent/outdent lines (repeatable)

```json
    "vim.visualModeKeyBindingsNonRecursive": [
        {
            "before": [
                ">"
            ],
            "commands": [
                "editor.action.indentLines"
            ]
        },
        {
            "before": [
                "<"
            ],
            "commands": [
                "editor.action.outdentLines"
            ]
        },
    ]
```

- Bind `<leader>vim` to clone this repository to the selected location.

```json
    "vim.visualModeKeyBindingsNonRecursive": [
        {
            "before": [
                "<leader>", "v", "i", "m"
            ],
            "commands": [
                {
                    "command": "git.clone",
                    "args": [ "https://github.com/VSCodeVim/Vim.git" ]
                }
            ]
        }
    ]
```

#### `"vim.insertModeKeyBindingsNonRecursive"`/`"normalModeKeyBindingsNonRecursive"`/`"visualModeKeyBindingsNonRecursive"`

- Non-recursive keybinding overrides to use for insert, normal, and visual modes
- _Example:_ Bind `j` to `gj`. Notice that if you attempted this binding normally, the j in gj would be expanded into gj, on and on forever. Stop this recursive expansion using insertModeKeyBindingsNonRecursive and/or normalModeKeyBindingNonRecursive.

```json
    "vim.normalModeKeyBindingsNonRecursive": [
        {
            "before": ["j"],
            "after": ["g", "j"]
        }
    ]
```

#### Debugging Remappings

1.  Are your configurations correct?

    Adjust the extension''s [logging level](#vscodevim-settings) to ''debug'', restart VS Code. As each remapped configuration is loaded, it is outputted to console. In the Developer Tools console, do you see any errors?

    ```console
    debug: Remapper: normalModeKeyBindingsNonRecursive. before=0. after=^.
    debug: Remapper: insertModeKeyBindings. before=j,j. after=<Esc>.
    error: Remapper: insertModeKeyBindings. Invalid configuration. Missing ''after'' key or ''command''. before=j,k.
    ```

    Misconfigured configurations are ignored.

2.  Does the extension handle the keys you are trying to remap?

    VSCodeVim explicitly instructs VS Code which key events we care about through the [package.json](https://github.com/VSCodeVim/Vim/blob/9bab33c75d0a53873880a79c5d2de41c8be1bef9/package.json#L62). If the key you are trying to remap is a key in which vim/vscodevim generally does not handle, then it''s most likely that this extension does not receive those key events from VS Code. With [logging level](#vscodevim-settings) adjusted to ''debug'', as you press keys, you should see output similar to:

    ```console
    debug: ModeHandler: handling key=A.
    debug: ModeHandler: handling key=l.
    debug: ModeHandler: handling key=<BS>.
    debug: ModeHandler: handling key=<C-a>.
    ```

    As you press the key that you are trying to remap, do you see it outputted here? If not, it means we don''t subscribe to those key events.

### Vim settings

Configuration settings that have been copied from vim. Vim settings are loaded in the following sequence:

1.  `:set {setting}`
2.  `vim.{setting}` from user/workspace settings.
3.  VS Code settings
4.  VSCodeVim default values

| Setting          | Description                                                                                                                                                                                                                                                           | Type    | Default Value |
| ---------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------- | ------------- |
| vim.autoindent   | Copy indent from current line when starting a new line                                                                                                                                                                                                                | Boolean | true          |
| vim.hlsearch     | Highlights all text matching current search                                                                                                                                                                                                                           | Boolean | false         |
| vim.ignorecase   | Ignore case in search patterns                                                                                                                                                                                                                                        | Boolean | true          |
| vim.incsearch    | Show the next match while entering a search                                                                                                                                                                                                                           | Boolean | true          |
| vim.leader       | Defines key for `<leader>` to be used in key remappings                                                                                                                                                                                                               | String  | `\\`           |
| vim.showcmd      | Show (partial) command in status bar                                                                                                                                                                                                                                  | Boolean | true          |
| vim.showmodename | Show name of current mode in status bar                                                                                                                                                                                                                               | Boolean | true          |
| vim.smartcase    | Override the ''ignorecase'' setting if search pattern contains uppercase characters                                                                                                                                                                                     | Boolean | true          |
| vim.textwidth    | Width to word-wrap when using `gq`                                                                                                                                                                                                                                    | Number  | 80            |
| vim.timeout      | Timeout in milliseconds for remapped commands                                                                                                                                                                                                                         | Number  | 1000          |
| vim.whichwrap    | Controls wrapping at beginning and end of line. Comma-separated set of keys that should wrap to next/previous line. Arrow keys are represented by `[` and `]` in insert mode, `<` and `>` in normal and visual mode. To wrap "everything", set this to `h,l,<,>,[,]`. | String  | ``            |
| vim.report       | Threshold for reporting number of lines changed.                                                                                                                                                                                                                      | Number  | 2             |

## .vimrc support

> :warning: .vimrc support is currently experimental. Only remaps are supported, and you may experience bugs. Please [report them](https://github.com/VSCodeVim/Vim/issues/new?template=bug_report.md)!

Set `vim.vimrc.enable` to `true` and set `vim.vimrc.path` appropriately.

## ?? Multi-Cursor Mode

> :warning: Multi-Cursor mode is experimental. Please report issues in our [feedback thread.](https://github.com/VSCodeVim/Vim/issues/824)

Enter multi-cursor mode by:

- On OSX, `cmd-d`. On Windows, `ctrl-d`.
- `gb`, a new shortcut we added which is equivalent to `cmd-d` (OSX) or `ctrl-d` (Windows). It adds another cursor at the next word that matches the word the cursor is currently on.
- Running "Add Cursor Above/Below" or the shortcut on any platform.

Once you have multiple cursors, you should be able to use Vim commands as you see fit. Most should work; some are unsupported (ref [PR#587](https://github.com/VSCodeVim/Vim/pull/587)).

- Each cursor has its own clipboard.
- Pressing Escape in Multi-Cursor Visual Mode will bring you to Multi-Cursor Normal mode. Pressing it again will return you to Normal mode.

## ? Emulated Plugins

### vim-airline

> :warning: There are performance implications to using this plugin. In order to change the status bar, we override the configurations in your workspace settings.json which results in increased latency and a constant changing diff in your working directory (see [issue#2124](https://github.com/VSCodeVim/Vim/issues/2124)).

Change the color of the status bar based on the current mode. Once enabled, configure `"vim.statusBarColors"`. Colors can be defined for each mode either as `string` (background only), or `string[]` (background, foreground).

```json
    "vim.statusBarColorControl": true,
    "vim.statusBarColors.normal": ["#8FBCBB", "#434C5E"],
    "vim.statusBarColors.insert": "#BF616A",
    "vim.statusBarColors.visual": "#B48EAD",
    "vim.statusBarColors.visualline": "#B48EAD",
    "vim.statusBarColors.visualblock": "#A3BE8C",
    "vim.statusBarColors.replace": "#D08770",
    "vim.statusBarColors.commandlineinprogress": "#007ACC",
    "vim.statusBarColors.searchinprogressmode": "#007ACC",
    "vim.statusBarColors.easymotionmode": "#007ACC",
    "vim.statusBarColors.easymotioninputmode": "#007ACC",
    "vim.statusBarColors.surroundinputmode": "#007ACC",
```

### vim-easymotion

Based on [vim-easymotion](https://github.com/easymotion/vim-easymotion) and configured through the following settings:

| Setting                                    | Description                                                                                                                                                                                                                                                       | Type           | Default Value  |
| ------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------- | -------------- |
| vim.easymotion                             | Enable/disable easymotion plugin                                                                                                                                                                                                                                  | Boolean        | false          |
| vim.easymotionMarkerBackgroundColor        | The background color of the marker box.                                                                                                                                                                                                                           |
| vim.easymotionMarkerForegroundColorOneChar | The font color for one-character markers.                                                                                                                                                                                                                         |
| vim.easymotionMarkerForegroundColorTwoChar | The font color for two-character markers, used to differentiate from one-character markers.                                                                                                                                                                       |
| vim.easymotionMarkerWidthPerChar           | The width in pixels allotted to each character.                                                                                                                                                                                                                   |
| vim.easymotionMarkerHeight                 | The height of the marker.                                                                                                                                                                                                                                         |
| vim.easymotionMarkerFontFamily             | The font family used for the marker text.                                                                                                                                                                                                                         |
| vim.easymotionMarkerFontSize               | The font size used for the marker text.                                                                                                                                                                                                                           |
| vim.easymotionMarkerFontWeight             | The font weight used for the marker text.                                                                                                                                                                                                                         |
| vim.easymotionMarkerYOffset                | The distance between the top of the marker and the text (will typically need some adjusting if height or font size have been changed).                                                                                                                            |
| vim.easymotionKeys                         | The characters used for jump marker name                                                                                                                                                                                                                          |
| vim.easymotionJumpToAnywhereRegex          | Custom regex to match for JumpToAnywhere motion (analogous to `Easymotion_re_anywhere`). Example setting (which also matches start & end of line, as well as Javascript comments in addition to the regular behavior (note the double escaping required): ^\\\\s\\*. | \\\\b[A-Za-z0-9] | [A-Za-z0-9]\\\\b | \\_. | \\\\#. | [a-z][a-z] | // | .\\$" |

Once easymotion is active, initiate motions using the following commands. After you initiate the motion, text decorators/markers will be displayed and you can press the keys displayed to jump to that position. `leader` is configurable and is `\\` by default.

| Motion Command                      | Description                                                                                                    |
| ----------------------------------- | -------------------------------------------------------------------------------------------------------------- |
| `<leader><leader> s <char>`         | Search character                                                                                               |
| `<leader><leader> f <char>`         | Find character forwards                                                                                        |
| `<leader><leader> F <char>`         | Find character backwards                                                                                       |
| `<leader><leader> t <char>`         | Til character forwards                                                                                         |
| `<leader><leader> T <char>`         | Til character backwards                                                                                        |
| `<leader><leader> w`                | Start of word forwards                                                                                         |
| `<leader><leader> b`                | Start of word backwards                                                                                        |
| `<leader><leader> l`                | Matches beginning & ending of word, camelCase, after `_`, and after `#` forwards                               |
| `<leader><leader> h`                | Matches beginning & ending of word, camelCase, after `_`, and after `#` backwards                              |
| `<leader><leader> e`                | End of word forwards                                                                                           |
| `<leader><leader> ge`               | End of word backwards                                                                                          |
| `<leader><leader> j`                | Start of line forwards                                                                                         |
| `<leader><leader> k`                | Start of line backwards                                                                                        |
| `<leader><leader> / <char>... <CR>` | Search n-character                                                                                             |
| `<leader><leader><leader> bdt`      | Til character                                                                                                  |
| `<leader><leader><leader> bdw`      | Start of word                                                                                                  |
| `<leader><leader><leader> bde`      | End of word                                                                                                    |
| `<leader><leader><leader> bdjk`     | Start of line                                                                                                  |
| `<leader><leader><leader> j`        | JumpToAnywhere motion; default behavior matches beginning & ending of word, camelCase, after `_` and after `#` |

`<leader><leader> (2s|2f|2F|2t|2T) <char><char>` and `<leader><leader><leader> bd2t <char>char>` are also available.
The difference is character count required for search.
For example, `<leader><leader> 2s <char><char>` requires two characters, and search by two characters.
This mapping is not a standard mapping, so it is recommended to use your custom mapping.

### vim-surround

Based on [surround.vim](https://github.com/tpope/vim-surround), the plugin is used to work with surrounding characters like parentheses, brackets, quotes, and XML tags.

| Setting      | Description                 | Type    | Default Value |
| ------------ | --------------------------- | ------- | ------------- |
| vim.surround | Enable/disable vim-surround | Boolean | true          |

`t` or `<` as `<desired char>` or `<existing char>` will do tags and enter tag entry mode. Using `<CR>` instead of `>` to finish changing a tag will preserve any existing attributes.

| Surround Command                     | Description                                                           |
| ------------------------------------ | --------------------------------------------------------------------- |
| `d s <existing char>`                | Delete existing surround                                              |
| `c s <existing char> <desired char>` | Change surround existing to desired                                   |
| `y s <motion> <desired char>`        | Surround something with something using motion (as in "you surround") |
| `S <desired char>`                   | Surround when in visual modes (surrounds full selection)              |

Some examples:

- `"test"` with cursor inside quotes type `cs"''` to end up with `''test''`
- `"test"` with cursor inside quotes type `ds"` to end up with `test`
- `"test"` with cursor inside quotes type `cs"t` and enter `123>` to end up with `<123>test</123>`

### vim-commentary

Similar to [vim-commentary](https://github.com/tpope/vim-commentary), but uses the VS Code native _Toggle Line Comment_ and _Toggle Block Comment_ features.

Usage examples:

- `gc` - toggles line comment. For example `gcc` to toggle line comment for current line and `gc2j` to toggle line comments for the current line and the next two lines.
- `gC` - toggles block comment. For example `gCi)` to comment out everything within parentheses.

### vim-indent-object

Based on [vim-indent-object](https://github.com/michaeljsmith/vim-indent-object), it allows for treating blocks of code at the current indentation level as text objects. Useful in languages that don''t use braces around statements (e.g. Python).

Provided there is a new line between the opening and closing braces / tag, it can be considered an agnostic `cib`/`ci{`/`ci[`/`cit`.

| Command        | Description                                                                                          |
| -------------- | ---------------------------------------------------------------------------------------------------- |
| `<operator>ii` | This indentation level                                                                               |
| `<operator>ai` | This indentation level and the line above (think `if` statements in Python)                          |
| `<operator>aI` | This indentation level, the line above, and the line after (think `if` statements in C/C++/Java/etc) |

### vim-sneak

Based on [vim-sneak](https://github.com/justinmk/vim-sneak), it allows for jumping to any location specified by two characters.

| Setting                            | Description                                                 | Type    | Default Value |
| ---------------------------------- | ----------------------------------------------------------- | ------- | ------------- |
| vim.sneak                          | Enable/disable vim-sneak                                    | Boolean | false         |
| vim.sneakUseIgnorecaseAndSmartcase | Respect `vim.ignorecase` and `vim.smartcase` while sneaking | Boolean | false         |

Once sneak is active, initiate motions using the following commands. For operators sneak uses `z` instead of `s` because `s` is already taken by the surround plugin.

| Motion Command            | Description                                                             |
| ------------------------- | ----------------------------------------------------------------------- |
| `s<char><char>`           | Move forward to the first occurrence of `<char><char>`                  |
| `S<char><char>`           | Move backward to the first occurrence of `<char><char>`                 |
| `<operator>z<char><char>` | Perform `<operator>` forward to the first occurrence of `<char><char>`  |
| `<operator>Z<char><char>` | Perform `<operator>` backward to the first occurrence of `<char><char>` |

### CamelCaseMotion

Based on [CamelCaseMotion](https://github.com/bkad/CamelCaseMotion), though not an exact emulation. This plugin provides an easier way to move through camelCase and snake_case words.

| Setting                    | Description                    | Type    | Default Value |
| -------------------------- | ------------------------------ | ------- | ------------- |
| vim.camelCaseMotion.enable | Enable/disable CamelCaseMotion | Boolean | false         |

Once CamelCaseMotion is enabled, the following motions are available:

| Motion Command         | Description                                                                |
| ---------------------- | -------------------------------------------------------------------------- |
| `<leader>w`            | Move forward to the start of the next camelCase or snake_case word segment |
| `<leader>e`            | Move forward to the next end of a camelCase or snake_case word segment     |
| `<leader>b`            | Move back to the prior beginning of a camelCase or snake_case word segment |
| `<operator>i<leader>w` | Select/change/delete/etc. the current camelCase or snake_case word segment |

By default, `<leader>` is mapped to `\\`, so for example, `d2i\\w` would delete the current and next camelCase word segment.

### Input Method

Disable input method when exiting Insert Mode.

| Setting                                 | Description                                                                                      |
| --------------------------------------- | ------------------------------------------------------------------------------------------------ |
| `vim.autoSwitchInputMethod.enable`      | Boolean denoting whether autoSwitchInputMethod is on/off.                                        |
| `vim.autoSwitchInputMethod.defaultIM`   | Default input method.                                                                            |
| `vim.autoSwitchInputMethod.obtainIMCmd` | The full path to command to retrieve the current input method key.                               |
| `vim.autoSwitchInputMethod.switchIMCmd` | The full path to command to switch input method, with `{im}` a placeholder for input method key. |

Any third-party program can be used to switch input methods. The following will walkthrough the configuration using [im-select](https://github.com/daipeihust/im-select).

1.  Install im-select (see [installation guide](https://github.com/daipeihust/im-select#installation))
1.  Find your default input method key

    - Mac:

      Switch your input method to English, and run the following in your terminal: `/<path-to-im-select-installation>/im-select` to output your default input method. The table below lists the common English key layouts for MacOS.

      | Key                            | Description |
      | ------------------------------ | ----------- |
      | com.apple.keylayout.US         | U.S.        |
      | com.apple.keylayout.ABC        | ABC         |
      | com.apple.keylayout.British    | British     |
      | com.apple.keylayout.Irish      | Irish       |
      | com.apple.keylayout.Australian | Australian  |
      | com.apple.keylayout.Dvorak     | Dvorak      |
      | com.apple.keylayout.Colemak    | Colemak     |

    - Windows:

      Refer to the [im-select guide](https://github.com/daipeihust/im-select#to-get-current-keyboard-locale) on how to discover your input method key. Generally, if your keyboard layout is en_US the input method key is 1033 (the locale ID of en_US). You can also find your locale ID from [this page](https://www.science.co.il/language/Locale-codes.php), where the `LCID Decimal` column is the locale ID.

1.  Configure `vim.autoSwitchInputMethod`.

    - MacOS:

      Given the input method key of `com.apple.keylayout.US` and `im-select` located at `/usr/local/bin`. The configuration is:

      ```json
      "vim.autoSwitchInputMethod.enable": true,
      "vim.autoSwitchInputMethod.defaultIM": "com.apple.keylayout.US",
      "vim.autoSwitchInputMethod.obtainIMCmd": "/usr/local/bin/im-select",
      "vim.autoSwitchInputMethod.switchIMCmd": "/usr/local/bin/im-select {im}"
      ```

    - Windows:

      Given the input method key of `1033` (en_US) and `im-select.exe` located at `D:/bin`. The configuration is:

      ```json
      "vim.autoSwitchInputMethod.enable": true,
      "vim.autoSwitchInputMethod.defaultIM": "1033",
      "vim.autoSwitchInputMethod.obtainIMCmd": "D:\\\\bin\\\\im-select.exe",
      "vim.autoSwitchInputMethod.switchIMCmd": "D:\\\\bin\\\\im-select.exe {im}"
      ```

The `{im}` argument above is a command-line option that will be passed to `im-select` denoting the input method to switch to. If using an alternative program to switch input methods, you should add a similar option to the configuration. For example, if the program''s usage is `my-program -s imKey` to switch input method, the `vim.autoSwitchInputMethod.switchIMCmd` should be `/path/to/my-program -s {im}`.

### ReplaceWithRegister

Based on [ReplaceWithRegister](https://github.com/vim-scripts/ReplaceWithRegister), an easy way to replace existing text with the contents of a register.

| Setting                 | Description                        | Type    | Default Value |
| ----------------------- | ---------------------------------- | ------- | ------------- |
| vim.replaceWithRegister | Enable/disable ReplaceWithRegister | Boolean | false         |

Once active, type `gr` (say "go replace") followed by a motion to describe the text you want replaced by the contents of the register.

| Motion Command          | Description                                                                             |
| ----------------------- | --------------------------------------------------------------------------------------- |
| `[count]["a]gr<motion>` | Replace the text described by the motion with the contents of the specified register    |
| `[count]["a]grr`        | Replace the \\[count\\] lines or current line with the contents of the specified register |
| `{Visual}["a]gr`        | Replace the selection with the contents of the specified register                       |

### vim-textobj-entire

Similar to [vim-textobj-entire](https://github.com/kana/vim-textobj-entire).

Adds two useful text-objects:

- `ae` which represents the entire content of a buffer.
- `ie` which represents the entire content of a buffer without the leading and trailing spaces.

Usage examples:

- `dae` - delete the whole buffer content.
- `yie` - will yank the buffer content except leading and trailing blank lines.
- `gUae` - transform the whole buffer to uppercase.

## ? VSCodeVim tricks!

VS Code has a lot of nifty tricks and we try to preserve some of them:

- `gd` - jump to definition.
- `gq` - on a visual selection reflow and wordwrap blocks of text, preserving commenting style. Great for formatting documentation comments.
- `gb` - adds another cursor on the next word it finds which is the same as the word under the cursor.
- `af` - visual mode command which selects increasingly large blocks of text. For example, if you had "blah (foo [bar ''ba|z''])" then it would select ''baz'' first. If you pressed `af` again, it''d then select [bar ''baz''], and if you did it a third time it would select "(foo [bar ''baz''])".
- `gh` - equivalent to hovering your mouse over wherever the cursor is. Handy for seeing types and error messages without reaching for the mouse!

## ? F.A.Q.

- None of the native Visual Studio Code `ctrl` (e.g. `ctrl+f`, `ctrl+v`) commands work

  Set the [`useCtrlKeys` setting](#vscodevim-settings) to `false`.

- Moving `j`/`k` over folds opens up the folds

  Try setting `vim.foldfix` to `true`. This is a hack; it works fine, but there are side effects (see [issue#22276](https://github.com/Microsoft/vscode/issues/22276)).

- Key repeat doesn''t work

  Are you on a Mac? Did you go through our [mac-setup](#mac) instructions?

- There are annoying intellisense/notifications/popups that I can''t close with `<esc>`! Or I''m in a snippet and I want to close intellisense

  Press `shift+<esc>` to close all of those boxes.

- How can I use the commandline when in Zen mode or when the status bar is disabled?

  This extension exposes a remappable command to show a VS Code style quick-pick version of the commandline, with more limited functionality. This can be remapped as follows in VS Code''s keybindings.json settings file.

  ```json
  {
    "key": "shift+;",
    "command": "vim.showQuickpickCmdLine",
    "when": "editorTextFocus && vim.mode != ''Insert''"
  }
  ```

  Or for Zen mode only:

  ```json
  {
    "key": "shift+;",
    "command": "vim.showQuickpickCmdLine",
    "when": "inZenMode && vim.mode != ''Insert''"
  }
  ```

- How can I move the cursor by each display line with word wrapping?

  If you have word wrap on and would like the cursor to enter each wrapped line when using <kbd>j</kbd>, <kbd>k</kbd>, <kbd>?</kbd> or <kbd>?</kbd>, set the following in VS Code''s keybindings.json settings file.

  <!-- prettier-ignore -->
  ```json
  {
    "key": "up",
    "command": "cursorUp",
    "when": "editorTextFocus && vim.active && !inDebugRepl && !suggestWidgetMultipleSuggestions && !suggestWidgetVisible"
  },
  {
    "key": "down",
    "command": "cursorDown",
    "when": "editorTextFocus && vim.active && !inDebugRepl && !suggestWidgetMultipleSuggestions && !suggestWidgetVisible"
  },
  {
    "key": "k",
    "command": "cursorUp",
    "when": "editorTextFocus && vim.active && !inDebugRepl && vim.mode == ''Normal'' && !suggestWidgetMultipleSuggestions && !suggestWidgetVisible"
  },
  {
    "key": "j",
    "command": "cursorDown",
    "when": "editorTextFocus && vim.active && !inDebugRepl && vim.mode == ''Normal'' && !suggestWidgetMultipleSuggestions && !suggestWidgetVisible"
  }
  ```

  **Caveats:** This solution restores the default VS Code behavior for the <kbd>j</kbd> and <kbd>k</kbd> keys, so motions like `10j` [will not work](https://github.com/VSCodeVim/Vim/pull/3623#issuecomment-481473981). If you need these motions to work, [other, less performant options exist](https://github.com/VSCodeVim/Vim/issues/2924#issuecomment-476121848).

- I''ve swapped Escape and Caps Lock with setxkbmap and VSCodeVim isn''t respecting the swap

  This is a [known issue in VS Code](https://github.com/microsoft/vscode/issues/23991), as a workaround you can set `"keyboard.dispatch": "keycode"` and restart VS Code.

## ?? Contributing

This project is maintained by a group of awesome [people](https://github.com/VSCodeVim/Vim/graphs/contributors) and contributions are extremely welcome :heart:. For a quick tutorial on how you can help, see our [contributing guide](/.github/CONTRIBUTING.md).

<a href="https://www.buymeacoffee.com/jasonpoon" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" alt="Buy Us A Coffee" style="height: auto !important;width: auto !important;" ></a>

### Special shoutouts to:

- Thanks to @xconverge for making over 100 commits to the repo. If you''re wondering why your least favorite bug packed up and left, it was probably him.
- Thanks to @Metamist for implementing EasyMotion!
- Thanks to @sectioneight for implementing text objects!
- Special props to [Kevin Coleman](http://kevincoleman.io), who created our awesome logo!
- Shoutout to @chillee aka Horace He for his contributions and hard work.
');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (6, 130, '2020-05-01 01:35:01', 'Fix Milan showing up as ''Europe Milan'' instead of just ''Milan'' (#1738)', 1112, '![Build Status](https://codebuild.eu-west-1.amazonaws.com/badges?uuid=eyJlbmNyeXB0ZWREYXRhIjoiekhxeERIMmNLSkNYUktnUFJzUVJucmJqWnFLMGlpNXJiNE1LLzVWV3B1QUpSSkhCS04veHZmUGxZZ0ZmZlRzYjJ3T1VtVEs1b3JxbWNVOHFOeFJDOTAwPSIsIml2UGFyYW1ldGVyU3BlYyI6ImZXNW5KaytDRGNLdjZuZDgiLCJtYXRlcmlhbFNldFNlcmlhbCI6MX0%3D&branch=master) 
[![Coverage](https://img.shields.io/codecov/c/github/aws/aws-toolkit-jetbrains/master.svg)](https://codecov.io/gh/aws/aws-toolkit-jetbrains/branch/master) 
[![Gitter](https://badges.gitter.im/aws/aws-toolkit-jetbrains.svg)](https://gitter.im/aws/aws-toolkit-jetbrains?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)
[![Downloads](https://img.shields.io/jetbrains/plugin/d/11349-aws-toolkit.svg)](https://plugins.jetbrains.com/plugin/11349-aws-toolkit) 
[![Version](https://img.shields.io/jetbrains/plugin/v/11349.svg?label=version)](https://plugins.jetbrains.com/plugin/11349-aws-toolkit)
[![LGTM Grade](https://img.shields.io/lgtm/grade/java/github/aws/aws-toolkit-jetbrains)](https://lgtm.com/projects/g/aws/aws-toolkit-jetbrains/)
 
# AWS Toolkit for JetBrains

AWS Toolkit for JetBrains - a plugin for interacting with AWS from JetBrains IDEs. The plugin includes features that 
make it easier to write applications on [Amazon Web Services](https://aws.amazon.com/) using a JetBrains IDE.

This is an open source project because we want you to be involved. We love issues, feature requests, code reviews, pull 
requests or any positive contribution. See [CONTRIBUTING](CONTRIBUTING.md) for how to help.

## Feedback

We want your feedback!

- Upvote ? [feature requests](https://github.com/aws/aws-toolkit-jetbrains/issues?q=is%3Aissue+is%3Aopen+label%3Afeature-request+sort%3Areactions-%2B1-desc)
- [Ask a question](https://github.com/aws/aws-toolkit-jetbrains/issues/new?labels=guidance&template=guidance_request.md)
- [Request a new feature](https://github.com/aws/aws-toolkit-jetbrains/issues/new?labels=feature-request&template=feature_request.md)
- [File an issue](https://github.com/aws/aws-toolkit-jetbrains/issues/new?labels=bug&template=bug_report.md)

## Requirements
Supported IDEs:
* IntelliJ Community/Ultimate 2019.2+
* PyCharm Community/Professional 2019.2+
* Rider 2019.2+
* WebStorm 2019.2+

## Installation

See [Installing the AWS Toolkit for JetBrains](https://docs.aws.amazon.com/console/toolkit-for-jetbrains/install) in the AWS Toolkit for JetBrains User Guide.

To use this AWS Toolkit, you will first need an AWS account, a user within that account, and an access key for that 
user. To use the AWS Toolkit to do AWS serverless application development and to run/debug AWS Lambda functions locally,
you will also need to install the AWS CLI, Docker, and the AWS SAM CLI. The preceding link covers setting up all of 
these prerequisites.

### EAP Builds
We also offer opt-in Early Access Preview builds that are built automatically.

In order to opt-in:
* Add the URL `https://plugins.jetbrains.com/plugins/eap/aws.toolkit` to your IDE''s plugin repository preferences by 
going to **Plugins->Gear Icon->Manage Plugin Repositories** and adding the URL to the list
* Check for updates.

### From Source
Please see [CONTRIBUTING](CONTRIBUTING.md#building-from-source) for instructions.

## Features

### General

Features that don''t relate to a specific AWS service.

* **Credential management** - the ability to select how you want to authenticate with AWS, management of several 
credential types and the ability to easily switch between profiles. 
[Learn More](https://docs.aws.amazon.com/console/toolkit-for-jetbrains/credentials)
* **Region management** - the ability to switch between viewing resources in different AWS regions.
[Learn More](https://docs.aws.amazon.com/console/toolkit-for-jetbrains/regions)
* **AWS Resource Explorer** - tree-view of AWS resources available in your 
selected account/region. This does not represent all resources available in your account, only a sub-set of those 
resource types supported by the plugin.
[Learn More](https://docs.aws.amazon.com/console/toolkit-for-jetbrains/aws-explorer)

### Services

#### ![AWS Lambda][lambda-icon] AWS Lambda

Many of these features require the [AWS SAM CLI](https://github.com/awslabs/aws-sam-cli) to be installed, see the 
Serverless Application Model ([SAM](https://aws.amazon.com/serverless/sam/)) website for more information on 
installation of the SAM CLI.

**SAM features support Java, Python, Node.js, and .NET Core**

* **New Project Wizard** - Get started quickly by using one of the quickstart serverless application templates.
[Learn More](https://docs.aws.amazon.com/console/toolkit-for-jetbrains/new-project)
* **Run/Debug Local Lambda Functions** - Locally test and step-through debug functions in a Lambda-like execution 
environment provided by the SAM CLI.
[Learn More](https://docs.aws.amazon.com/console/toolkit-for-jetbrains/lambda-local)
* **Invoke Remote Lambda Functions** - Invoke remote functions using a sharable run-configuration
[Learn More](https://docs.aws.amazon.com/console/toolkit-for-jetbrains/lambda-remote)
* **Package & Deploy Lambda Functions** - Ability to package a Lambda function zip and create a remote lambda
[Learn More](https://docs.aws.amazon.com/console/toolkit-for-jetbrains/lambda-deploy)
* **Deploy SAM-based Applications** - Package, deploy & track SAM-based applications
[Learn More](https://docs.aws.amazon.com/console/toolkit-for-jetbrains/sam-deploy)

*NB: Python-only features are available in both PyCharm and IntelliJ with the 
[Python Plugin](https://www.jetbrains.com/help/idea/plugin-overview.html) installed.*

## Roadmap

The best view of our long-term road-map is by looking the upcoming Release 
[Milestones](https://github.com/aws/aws-toolkit-jetbrains/milestones). 

In addition to GitHub''s built-in [Projects](https://github.com/aws/aws-toolkit-jetbrains/projects) and 
[Milestones](https://github.com/aws/aws-toolkit-jetbrains/milestones) we use [ZenHub](https://www.zenhub.com) to help:
* manage our back-log
* prioritize features
* estimate issues
* create sprint-boards

To enable these enhanced views can sign-up for ZenHub (using your GitHub account - it''s free), install 
the ZenHub [extension](https://www.zenhub.com/extension) for your browser and then navigate to the 
[ZebHub](https://github.com/aws/aws-toolkit-jetbrains#zenhub) tab in the toolkit repository. 

## Licensing

The plugin is distributed according to the terms outlined in our [LICENSE](LICENSE).

[lambda-icon]: jetbrains-core/resources/icons/resources/LambdaFunction.svg
');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (7, 110, '2020-04-15 13:51:38', 'Improve README warning about IntelliJ 2020.1 (#784)

Reformatted markdown, added separating line and emojis for more readability of the warning about IntelliJ 2020.1 incompatibility bug', 153, 'lombok-intellij-plugin [![Donate](https://www.paypal.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=3F9HXD7A2SMCN)
======================
[![Build Status][badge-travis-img]][badge-travis] [![Code Coverage](https://img.shields.io/codecov/c/github/mplushnikov/lombok-intellij-plugin/master.svg)](https://codecov.io/github/mplushnikov/lombok-intellij-plugin?branch=master)

[![JetBrains Plugins](https://img.shields.io/jetbrains/plugin/v/6317-lombok-plugin.svg)](https://plugins.jetbrains.com/plugin/6317-lombok-plugin)
[![Downloads](https://img.shields.io/jetbrains/plugin/d/6317-lombok-plugin.svg)](https://plugins.jetbrains.com/plugin/6317-lombok-plugin)
[![Downloads last month](http://phpstorm.espend.de/badge/6317/last-month)](https://plugins.jetbrains.com/plugin/6317-lombok-plugin)

[![Gitter][badge-gitter-img]][badge-gitter] [![Donate][badge-paypal-img]][badge-paypal]


## Plugin for [IntelliJ IDEA](http://plugins.jetbrains.com/plugin/6317-lombok-plugin) to support [Lombok](https://projectlombok.org) annotations.

Provides support for lombok annotations to write great Java code with IntelliJ IDEA.

**Last version (0.29) released on 01.03.2020**

---

:collision: **The latest IntelliJ IDEA version (2020.1) contains a BUG :beetle: for several plugins (including plugin for Lombok) were shown as incompatible with the updated version of the IDE.** :collision:

You can find the issue with a detailed explanation here: https://youtrack.jetbrains.com/issue/IDEA-237113.

It has already been resolved and the fix will be in the nearest IDE patch (presumably 2020.1.1).

:hammer_and_wrench: **FIX:** As of now, the workaround is to re-install the plugin in the IDE settings.

---

34th version of plugin released.

Install it automatically from IntelliJ Idea plugin repository.

Tested and supports IntelliJ versions: 2016.2, 2016.3, 2017.X, 2018.X, 2019.1, 2019.2 and 2019.3

Last support for IntelliJ 15.0.6 and 2016.1 by plugin version 0.19!

Last support for IntelliJ 14.1.7 by plugin version 0.14!

Last support for IntelliJ 11.1.5, 12.1.7, 13.1.6 by plugin version 0.11

Last support for IntelliJ 10.5.4 by plugin version 0.8.7

With this plugin your IntelliJ can recognize all of generated getters, setters and some other things from lombok project, so that you get code completion and are able to work without errors stating the methods don''t exists.


Features / Supports
--------
- [@Getter and @Setter](http://projectlombok.org/features/GetterSetter.html)
- [@ToString](http://projectlombok.org/features/ToString.html)
- [@EqualsAndHashCode](http://projectlombok.org/features/EqualsAndHashCode.html)
- [@AllArgsConstructor, @RequiredArgsConstructor and @NoArgsConstructor](http://projectlombok.org/features/Constructor.html)
- [@Log, @Log4j, @Log4j2, @Slf4j, @XSlf4j, @CommonsLog, @JBossLog, @Flogger, @CustomLog](http://projectlombok.org/features/Log.html)
- [@Data](https://projectlombok.org/features/Data.html)
- [@Builder](https://projectlombok.org/features/Builder.html)
- [@Singular](https://projectlombok.org/features/Builder.html#singular)
- [@Delegate](https://projectlombok.org/features/Delegate.html)
- [@Value](https://projectlombok.org/features/Value.html)
- [@Accessors](https://projectlombok.org/features/experimental/Accessors.html)
- [@With](https://projectlombok.org/features/With.html)
- [@SneakyThrows](https://projectlombok.org/features/SneakyThrows.html)
- [@val](https://projectlombok.org/features/val.html) available from IntelliJ 14.1 (improved in 2016.2)
- [@UtilityClass](https://projectlombok.org/features/experimental/UtilityClass.html) available from IntelliJ 2016.2
- [lombok config files syntax highlighting](https://projectlombok.org/features/configuration.html)
- code inspections
- refactoring actions (lombok and delombok)
- project configuration inspection (missing/out-of-date Lombok dependency, annotation processing disabled)

Many features of the plugin (including warnings) could be disabled through per-project settings.

Installation
------------
### Plugin Installation
- Using IDE built-in plugin system on Windows:
  - <kbd>File</kbd> > <kbd>Settings</kbd> > <kbd>Plugins</kbd> > <kbd>Browse repositories...</kbd> > <kbd>Search for "lombok"</kbd> > <kbd>Install Plugin</kbd>
- Using IDE built-in plugin system on MacOs:
  - <kbd>Preferences</kbd> > <kbd>Settings</kbd> > <kbd>Plugins</kbd> > <kbd>Browse repositories...</kbd> > <kbd>Search for "lombok"</kbd> > <kbd>Install Plugin</kbd>
- Manually:
  - Download the [latest release](https://github.com/mplushnikov/lombok-intellij-plugin/releases/latest) and install it manually using <kbd>Preferences</kbd> > <kbd>Plugins</kbd> > <kbd>Install plugin from disk...</kbd>

Restart IDE.

### Required IntelliJ Configuration
In your project: Click <kbd>Preferences</kbd> -> <kbd>Build, Execution, Deployment</kbd> -> <kbd>Compiler, Annotation Processors</kbd>. Click <kbd>Enable Annotation Processing</kbd>

Afterwards you might need to do a complete rebuild of your project via <kbd>Build</kbd> -> <kbd>Rebuild Project</kbd>.

### Lombok project dependency
Make sure you have Lombok dependency added to your project. This plugin **does not** automatically add it for you.

**Please Note:** Using newest version of the Lombok dependency is recommended, but does not guarantee that all the features introduced will be available. See [Lombok changelog](https://projectlombok.org/changelog.html) for more details.

If you are using Gradle/Maven/Ivy, see example below:

##### Gradle
In your `build.gradle`:
```groovy
repositories {
	mavenCentral()
}

dependencies {
	compileOnly ''org.projectlombok:lombok:1.18.12''
	annotationProcessor ''org.projectlombok:lombok:1.18.12''

	testCompileOnly ''org.projectlombok:lombok:1.18.12''
	testAnnotationProcessor ''org.projectlombok:lombok:1.18.12''
}
```

##### Maven
In your `pom.xml`:
```xml
<dependencies>
	<dependency>
		<groupId>org.projectlombok</groupId>
		<artifactId>lombok</artifactId>
		<version>1.18.12</version>
		<scope>provided</scope>
	</dependency>
</dependencies>
```

##### Ivy
In your `ivy.xml`:
```xml
<dependency org="org.projectlombok" name="lombok" rev="1.18.12" conf="build" />
```

IntelliJ and Eclipse compiler
-----------------------------
If you''re using Eclipse compiler with lombok, try this setup:
- install plugin
- make sure Lombok dependency is added to the project
- change compiler setting:
  - <kbd>...</kbd> > <kbd>Compiler</kbd> > <kbd>Java Compiler</kbd> > <kbd>Use Compiler: Eclipse</kbd>
  - <kbd>...</kbd> > <kbd>Compiler</kbd> > <kbd>Annotation Processors</kbd> > <kbd>Enable annotation processing: checked (default configuration)</kbd>
  - <kbd>...</kbd> > <kbd>Compiler</kbd> > <kbd>Additional build process VM options: -javaagent:lombok.jar</kbd>

Developed By
------------
[**@mplushnikov** Michail Plushnikov](https://github.com/mplushnikov)

**Contributors**
- [**@adamarmistead** adamarmistead](https://github.com/adamarmistead)
- [**@akozlova** Anna Kozlova](https://github.com/akozlova)
- [**@alanachtenberg** Alan Achtenberg](https://github.com/alanachtenberg)
- [**@AlexejK** Alexej Kubarev](https://github.com/AlexejK)
- [**@bulgakovalexander** Alexander Bulgakov](https://github.com/bulgakovalexander)
- [**@jeromewaibel** Jérôme Waibel](https://github.com/jeromewaibel)
- [**@Jessevanbekkum** Jesse van Bekkum](https://github.com/Jessevanbekkum)
- [**@juriad** Adam Juraszek](https://github.com/juriad)
- [**@krzyk** Krzysztof Kraso?](https://github.com/krzyk)
- [**@Lekanich** Aleksandr Zhelezniak](https://github.com/Lekanich)
- [**@mg6maciej** Maciej Górski](https://github.com/mg6maciej)
- [**@mlueders** Mike Lueders](https://github.com/mlueders)
- [**@RohanTalip** Rohan Talip](https://github.com/RohanTalip)
- [**@ruurd** Ruurd Pels](https://github.com/ruurd)
- [**@Sheigutn** Florian Böhm](https://github.com/Sheigutn)
- [**@siosio** siosio](https://github.com/siosio)
- [**@Siriah** Iris Hupkens](https://github.com/Siriah)
- [**@tlinkowski** Tomasz Linkowski](https://github.com/tlinkowski)
- [**@toadzky** David Harris](https://github.com/toadzky)
- [**@twillouer** William Delanoue](https://github.com/twillouer)
- [**@uvpoblotzki** Ulrich von Poblotzki](https://github.com/uvpoblotzki)
- [**@vanam** Martin Vá?a](https://github.com/vanam)
- [**@yiftizur** Yiftach Tzur](https://github.com/yiftizur)
- [**@sluongng** Son Luong Ngoc](https://github.com/sluongng)
- [**@tagae** Sebastián González](https://github.com/tagae)
- [**@wyhasany** Micha? Rowicki](https://github.com/wyhasany)
- [**@ocadaruma** Haruki Okada](https://github.com/ocadaruma)
- [**@mackatozis** Alexandros Efthymiadis](https://github.com/mackatozis)

Supporters
--------
[<img src="https://www.yourkit.com/images/yklogo.png" />](https://www.yourkit.com/)

YourKit supports open source projects with its full-featured Java Profiler.
YourKit, LLC is the creator of [YourKit Java Profiler](https://www.yourkit.com/java/profiler/index.jsp) and [YourKit .NET Profiler](https://www.yourkit.com/.net/profiler/index.jsp), innovative and intelligent tools for profiling Java and .NET applications.


License
-------
Copyright (c) 2011-2020 Michail Plushnikov. See the [LICENSE](./LICENSE) file for license rights and limitations (BSD).

[badge-gitter-img]:       https://badges.gitter.im/mplushnikov/lombok-intellij-plugin.svg
[badge-gitter]:           https://gitter.im/mplushnikov/lombok-intellij-plugin
[badge-travis-img]:       https://travis-ci.org/mplushnikov/lombok-intellij-plugin.svg
[badge-travis]:           https://travis-ci.org/mplushnikov/lombok-intellij-plugin
[badge-coveralls-img]:    https://coveralls.io/repos/github/mplushnikov/lombok-intellij-plugin/badge.svg?branch=master
[badge-coveralls]:        https://coveralls.io/github/mplushnikov/lombok-intellij-plugin?branch=master
[badge-paypal-img]:       https://img.shields.io/badge/donate-paypal-yellow.svg
[badge-paypal]:           https://www.paypal.me/mplushnikov
');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (8, 0, '2020-03-17 14:55:42', 'Fixes for light selection and light text without bold. Return dark selection for color-blind scheme', 0, 'IntelliJ Light is a new light theme for IntelliJ-based IDEs starting 2019.3. This plugin is a preview of this theme and can be used with versions 2019.1 and 2019.2.
  
The new light theme features two notable changes:
* A cleaner editor scheme with regular font styles instead of bolds
* A unified theme for Windows, macOS and Linux

The goal of these changes is to make working with the code easier and to provide better UI support on all platforms.

Note that the current light themes for Windows and macOS that mimic the OS look will still be available after the new theme is introduced.

Please <a href="https://youtrack.jetbrains.com/issues?q=project:%20%7BIntelliJ%20IDEA%7D">share your feedback in YouTrack</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://youtrack.jetbrains.com/tag/IntelliJ%20LaF-153736?q=project:%20%7BIntelliJ%20IDEA%7D">Known issues</a>
<br/><br/>

To install:
1. Go to **Settings | Plugins**, find the theme plugin and install it
2. Restart the IDE
3. Go to **Settings | Appearance & Behavior | Appearance** and select **IntelliJ Light Preview** in the Theme dropdown

![IntelliJ Light theme main window](/screenshots/intellijlight-main-window.png)  
![IntelliJ Light theme settings](/screenshots/intellijlight-settings.png)');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (9, 5, '2017-10-30 19:06:54', 'Update README.md', 5, '# Obsolete

The development moved to [intellij-rust](https://github.com/intellij-rust/intellij-rust/tree/master/intellij-toml) repository.
');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (10, 2, '2020-02-11 16:57:46', 'fixed links', 8, '# Angular Essentials - Extension Pack for VS Code

[![Badge for version for Visual Studio Code extension](https://vsmarketplacebadge.apphb.com/version-short/johnpapa.angular-essentials.svg?color=blue&style=?style=for-the-badge&logo=visual-studio-code)](https://marketplace.visualstudio.com/items?itemName=johnpapa.angular-essentials&wt.mc_id=angular_essentials-github-jopapa) [![Installs](https://vsmarketplacebadge.apphb.com/installs-short/johnpapa.angular-essentials.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=johnpapa.angular-essentials)
[![Rating](https://vsmarketplacebadge.apphb.com/rating-short/johnpapa.angular-essentials.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=johnpapa.angular-essentials) [![The MIT License](https://img.shields.io/badge/license-MIT-orange.svg?color=blue&style=flat-square)](http://opensource.org/licenses/MIT)

This extension pack for Visual Studio Code adds extensions that are amazingly useful for Angular development.

See the [CHANGELOG](CHANGELOG.md) for the latest changes

I am often asked, "What are you favorite VS Code extensions for Angular?". I decided it was time to share them via an extension pack.

Introducing my [Angular Essentials extension pack for VS Code](https://marketplace.visualstudio.com/items?itemName=johnpapa.angular-essentials&wt.mc_id=angular_essentials-github-jopapa). By installing this extension pack you get a set of great extensions that are helpful with Angular development. You can check out the initial list below.

> As web tools evolve, the usefulness of extensions come and go. I reserve the right to update the extension pack''s contents up to my own discretion.

## Recommended Settings

Here are some of my recommended settings. These are optional, but I get asked a lot for them, so here they are.

### Editor settings

```json
  "editor.autoIndent": "full",
  "editor.codeLens": false,
  "editor.cursorBlinking": "solid",
  "editor.cursorSmoothCaretAnimation": true,
  "editor.cursorStyle": "line",
  "editor.fontSize": 16,
  "editor.fontFamily": "Dank Mono, Operator Mono, Fira Code, Inconsolata",
  "editor.fontLigatures": true,
  "editor.formatOnPaste": true,
  "editor.formatOnType": false,
  "editor.formatOnSave": true,
  "editor.letterSpacing": 0.5,
  "editor.lineHeight": 25,
  "editor.minimap.enabled": false,
  "editor.multiCursorModifier": "ctrlCmd",
  "editor.suggestSelection": "first",
  "editor.tabCompletion": "on",
  "editor.tabSize": 2,
  "editor.wordWrap": "on",
```

### File settings

```json
  "files.autoSave": "afterDelay",
  "files.autoSaveDelay": 1000,
  "files.hotExit": "onExit",
  "files.defaultLanguage": "",
  "files.trimTrailingWhitespace": true,
```

### Prettier settings

```json
  "prettier.printWidth": 120,
  "prettier.bracketSpacing": true,
  "prettier.singleQuote": true,
```

## Included

This extension pack includes the following extensions:

| Extension                | Stats                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Angular Snippets         | [![Badge for version for Visual Studio Code extension](https://vsmarketplacebadge.apphb.com/version-short/johnpapa.Angular2.svg?color=blue&style=?style=for-the-badge&logo=visual-studio-code)](https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2&wt.mc_id=angular_essentials-github-jopapa) [![Installs](https://vsmarketplacebadge.apphb.com/installs-short/johnpapa.Angular2.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2) [![Rating](https://vsmarketplacebadge.apphb.com/rating-short/johnpapa.Angular2.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2)                                                                   |
| Angular Language Service | [![Badge for version for Visual Studio Code extension](https://vsmarketplacebadge.apphb.com/version-short/Angular.ng-template.svg?color=blue&style=?style=for-the-badge&logo=visual-studio-code)](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template&wt.mc_id=angular_essentials-github-jopapa) [![Installs](https://vsmarketplacebadge.apphb.com/installs-short/Angular.ng-template.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template) [![Rating](https://vsmarketplacebadge.apphb.com/rating-short/Angular.ng-template.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)                                                       |
| Angular Console          | [![Badge for version for Visual Studio Code extension](https://vsmarketplacebadge.apphb.com/version-short/nrwl.angular-console.svg?color=blue&style=?style=for-the-badge&logo=visual-studio-code)](https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console&wt.mc_id=angular_essentials-github-jopapa) [![Installs](https://vsmarketplacebadge.apphb.com/installs-short/nrwl.angular-console.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console) [![Rating](https://vsmarketplacebadge.apphb.com/rating-short/nrwl.angular-console.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console)                                                 |
| Chrome Debugger          | [![Badge for version for Visual Studio Code extension](https://vsmarketplacebadge.apphb.com/version-short/msjsdiag.debugger-for-chrome.svg?color=blue&style=?style=for-the-badge&logo=visual-studio-code)](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome&wt.mc_id=angular_essentials-github-jopapa) [![Installs](https://vsmarketplacebadge.apphb.com/installs-short/msjsdiag.debugger-for-chrome.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome) [![Rating](https://vsmarketplacebadge.apphb.com/rating-short/msjsdiag.debugger-for-chrome.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome) |
| Edge Debugger            | [![Badge for version for Visual Studio Code extension](https://vsmarketplacebadge.apphb.com/version-short/msjsdiag.debugger-for-edge.svg?color=blue&style=?style=for-the-badge&logo=visual-studio-code)](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-edge&wt.mc_id=angular_essentials-github-jopapa) [![Installs](https://vsmarketplacebadge.apphb.com/installs-short/msjsdiag.debugger-for-edge.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-edge) [![Rating](https://vsmarketplacebadge.apphb.com/rating-short/msjsdiag.debugger-for-edge.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-edge)             |
| Editor Config            | [![Badge for version for Visual Studio Code extension](https://vsmarketplacebadge.apphb.com/version-short/EditorConfig.EditorConfig.svg?color=blue&style=?style=for-the-badge&logo=visual-studio-code)](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig&wt.mc_id=angular_essentials-github-jopapa) [![Installs](https://vsmarketplacebadge.apphb.com/installs-short/EditorConfig.EditorConfig.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig) [![Rating](https://vsmarketplacebadge.apphb.com/rating-short/EditorConfig.EditorConfig.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)                   |
| ESlint                   | [![Badge for version for Visual Studio Code extension](https://vsmarketplacebadge.apphb.com/version-short/dbaeumer.vscode-eslint.svg?color=blue&style=?style=for-the-badge&logo=visual-studio-code)](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint&wt.mc_id=angular_essentials-github-jopapa) [![Installs](https://vsmarketplacebadge.apphb.com/installs-short/dbaeumer.vscode-eslint.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) [![Rating](https://vsmarketplacebadge.apphb.com/rating-short/dbaeumer.vscode-eslint.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)                                     |
| Material Icon Theme      | [![Badge for version for Visual Studio Code extension](https://vsmarketplacebadge.apphb.com/version-short/pkief.material-icon-theme.svg?color=blue&style=?style=for-the-badge&logo=visual-studio-code)](https://marketplace.visualstudio.com/items?itemName=pkief.material-icon-theme&wt.mc_id=angular_essentials-github-jopapa) [![Installs](https://vsmarketplacebadge.apphb.com/installs-short/pkief.material-icon-theme.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=pkief.material-icon-theme) [![Rating](https://vsmarketplacebadge.apphb.com/rating-short/pkief.material-icon-theme.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=pkief.material-icon-theme)                   |
| npm                      | [![Badge for version for Visual Studio Code extension](https://vsmarketplacebadge.apphb.com/version-short/eg2.vscode-npm-script.svg?color=blue&style=?style=for-the-badge&logo=visual-studio-code)](https://marketplace.visualstudio.com/items?itemName=eg2.vscode-npm-script&wt.mc_id=angular_essentials-github-jopapa) [![Installs](https://vsmarketplacebadge.apphb.com/installs-short/eg2.vscode-npm-script.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=eg2.vscode-npm-script) [![Rating](https://vsmarketplacebadge.apphb.com/rating-short/eg2.vscode-npm-script.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=eg2.vscode-npm-script)                                           |
| Peacock                  | [![Badge for version for Visual Studio Code extension](https://vsmarketplacebadge.apphb.com/version-short/johnpapa.vscode-peacock.svg?color=blue&style=?style=for-the-badge&logo=visual-studio-code)](https://marketplace.visualstudio.com/items?itemName=johnpapa.vscode-peacock&wt.mc_id=angular_essentials-github-jopapa) [![Installs](https://vsmarketplacebadge.apphb.com/installs-short/johnpapa.vscode-peacock.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=johnpapa.vscode-peacock) [![Rating](https://vsmarketplacebadge.apphb.com/rating-short/johnpapa.vscode-peacock.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=johnpapa.vscode-peacock)                               |
| Prettier                 | [![Badge for version for Visual Studio Code extension](https://vsmarketplacebadge.apphb.com/version-short/esbenp.prettier-vscode.svg?color=blue&style=?style=for-the-badge&logo=visual-studio-code)](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode&wt.mc_id=angular_essentials-github-jopapa) [![Installs](https://vsmarketplacebadge.apphb.com/installs-short/esbenp.prettier-vscode.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) [![Rating](https://vsmarketplacebadge.apphb.com/rating-short/esbenp.prettier-vscode.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)                                     |
| Winter is Coming         | [![Badge for version for Visual Studio Code extension](https://vsmarketplacebadge.apphb.com/version-short/johnpapa.winteriscoming.svg?color=blue&style=?style=for-the-badge&logo=visual-studio-code)](https://marketplace.visualstudio.com/items?itemName=johnpapa.winteriscoming&wt.mc_id=angular_essentials-github-jopapa) [![Installs](https://vsmarketplacebadge.apphb.com/installs-short/johnpapa.winteriscoming.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=johnpapa.winteriscoming) [![Rating](https://vsmarketplacebadge.apphb.com/rating-short/johnpapa.winteriscoming.svg?color=blue&style=flat-square)](https://marketplace.visualstudio.com/items?itemName=johnpapa.winteriscoming)                               |
');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (11, 1130, '2020-05-01 04:40:20', '0.28.0-insiders2 changelog. (#5394)', 931, '# C/C++ for Visual Studio Code

#### [Repository](https://github.com/microsoft/vscode-cpptools)&nbsp;&nbsp;|&nbsp;&nbsp;[Issues](https://github.com/microsoft/vscode-cpptools/issues)&nbsp;&nbsp;|&nbsp;&nbsp;[Documentation](https://code.visualstudio.com/docs/languages/cpp)&nbsp;&nbsp;|&nbsp;&nbsp;[Code Samples](https://github.com/microsoft/vscode-cpptools/tree/master/Code%20Samples)&nbsp;&nbsp;|&nbsp;&nbsp;[Offline Installers](https://github.com/microsoft/vscode-cpptools/releases)

[![Badge](https://aka.ms/vsls-badge)](https://aka.ms/vsls)

This preview release of the C/C++ extension adds language support for C/C++ to Visual Studio Code, including features such as IntelliSense and debugging.

## Overview and tutorials
* [C/C++ extension overview](https://code.visualstudio.com/docs/languages/cpp)

C/C++ extension tutorials per compiler and platform
* [Microsoft C++ compiler (MSVC) on Windows](https://code.visualstudio.com/docs/cpp/config-msvc)
* [GCC and Mingw-w64 on Windows](https://code.visualstudio.com/docs/cpp/config-mingw)
* [GCC on Windows Subsystem for Linux (WSL)](https://code.visualstudio.com/docs/cpp/config-wsl)
* [GCC on Linux](https://code.visualstudio.com/docs/cpp/config-linux)
* [Clang on macOS](https://code.visualstudio.com/docs/cpp/config-clang-mac)

## Quick links
* [Editing features (IntelliSense)](https://code.visualstudio.com/docs/cpp/cpp-ide) 
* [IntelliSense configuration](https://code.visualstudio.com/docs/cpp/customize-default-settings-cpp)
* [Enhanced colorization](https://code.visualstudio.com/docs/cpp/colorization-cpp)
* [Debugging](https://code.visualstudio.com/docs/cpp/cpp-debug)
* [Debug configuration](https://code.visualstudio.com/docs/cpp/launch-json-reference)
* [Enable logging for IntelliSense or debugging](https://code.visualstudio.com/docs/cpp/enable-logging-cpp)

## Questions and feedback

**[FAQs](https://code.visualstudio.com/docs/cpp/faq-cpp)**
<br>
Check out the FAQs before filing a question.
<br>

**[Provide feedback](https://github.com/microsoft/vscode-cpptools/issues/new/choose)**
<br>
File questions, issues, or feature requests for the extension.
<br>

**[Known issues](https://github.com/Microsoft/vscode-cpptools/issues)**
<br>
If someone has already filed an issue that encompasses your feedback, please leave a ? or ? reaction on the issue to upvote or downvote it to help us prioritize the issue.
<br>

**[Quick survey](https://www.research.net/r/VBVV6C6)**
<br>
Let us know what you think of the extension by taking the quick survey.

## Offline installation

The extension has platform-specific binary dependencies, therefore installation via the Marketplace requires an Internet connection in order to download additional dependencies. If you are working on a computer that does not have access to the Internet or is behind a strict firewall, you may need to use our platform-specific packages and install them by running VS Code''s `"Install from VSIX..."` command. These "offline'' packages are available at: https://github.com/Microsoft/vscode-cpptools/releases.

 Package | Platform
:--- | :---
`cpptools-linux.vsix` | Linux 64-bit
`cpptools-linux32.vsix` | Linux 32-bit ([available up to version 0.27.0](https://github.com/microsoft/vscode-cpptools/issues/5346))
`cpptools-osx.vsix` | macOS
`cpptools-win32.vsix` | Windows 64-bit & 32-bit

## Contribution

Contributions are always welcome. Please see our [contributing guide](CONTRIBUTING.md) for more details.

## Microsoft Open Source Code of Conduct

This project has adopted the [Microsoft Open Source Code of Conduct](https://opensource.microsoft.com/codeofconduct/). For more information see the [Code of Conduct FAQ](https://opensource.microsoft.com/codeofconduct/faq/) or contact opencode@microsoft.com with any additional questions or comments.

## Data and telemetry

This extension collects usage data and sends it to Microsoft to help improve our products and services. Collection of telemetry is controlled via the same setting provided by Visual Studio Code: `"telemetry.enableTelemetry"`. Read our [privacy statement](https://privacy.microsoft.com/en-us/privacystatement) to learn more.');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (12, 67, '2020-05-01 23:28:41', 'style fixes', 263, '# Eclipse Groovy Development Tools

This project provides Eclipse and Maven tooling support for the [Apache Groovy](http://groovy-lang.org/) programming language.

## Users

More information, including how to use and install is available on the [wiki](https://github.com/groovy/groovy-eclipse/wiki).

## Issues

Please report improvement idea''s, possible bugs, etc. as [GitHub issues](https://github.com/groovy/groovy-eclipse/issues?q=is%3Aissue+is%3Aopen+sort%3Aupdated-desc).

## Questions and Answers

Check out [groovy-eclipse](https://stackoverflow.com/questions/tagged/groovy+eclipse?tab=newest) on stackoverflow.

## Contributors

If you wish to contribute to this project, see the [Getting Started with Groovy Eclipse Source Code Guide](docs/Getting-Started-with-Groovy-Eclipse-Source-Code.md) for detailed information.
');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (13, 453, '2020-05-01 12:51:06', 'Merge pull request #2814 from hhellyer/quicklabs_socket_and_react

Cope with paths being re-written in the perf dashboard.', 873, '# Codewind
![platforms](https://img.shields.io/badge/runtime-Java%20%7C%20Swift%20%7C%20Node-yellow.svg)
[![License](https://img.shields.io/badge/License-EPL%202.0-red.svg?label=license&logo=eclipse)](https://www.eclipse.org/legal/epl-2.0/)
[![Build Status](https://ci.eclipse.org/codewind/buildStatus/icon?job=Codewind%2Fcodewind%2Fmaster)](https://ci.eclipse.org/codewind/job/Codewind/job/codewind/job/master/)
[![Chat](https://img.shields.io/static/v1.svg?label=chat&message=mattermost&color=145dbf)](https://mattermost.eclipse.org/eclipse/channels/eclipse-codewind)
[![codecov](https://codecov.io/gh/eclipse/codewind/branch/master/graph/badge.svg)](https://codecov.io/gh/eclipse/codewind)

Build high-quality cloud-native applications for Kubernetes, regardless of your IDE or language.

Codewind enables you to create applications from a template or sample and includes support for launching, updating, testing, and debugging in  Docker containers on the desktop. Codewind also supports these features on Kubernetes. You can use Codewind to move existing applications to Docker and Kuberenetes. Codewind provides validation to ensure that applications follow best practices.

## Getting started
Use the following instructions to install Codewind with your choice of editor:
1. [VS Code extension](https://github.com/eclipse/codewind-vscode)
2. [Eclipse plugin](https://github.com/eclipse/codewind-eclipse)
3. [Eclipse Che plugin](https://github.com/eclipse/codewind-che-plugin)

## Feedback and community
Have any questions or comments on Codewind? Want to get involved? You can get in touch with the team:
- **Support:** Ask questions, report bugs, and request features with [GitHub issues](https://github.com/eclipse/codewind/issues).
- **Public chat:** Join the public [eclipse-codewind](https://mattermost.eclipse.org/eclipse/channels/eclipse-codewind) and [eclipse-codewind-dev](https://mattermost.eclipse.org/eclipse/channels/eclipse-codewind-dev) Mattermost channels.
- **Twitter:** Follow Codewind on Twitter at [@EclipseCodewind](https://twitter.com/EclipseCodewind).
- **Mailing list:** Join the mailing list from [codewind-dev@eclipse.org](https://accounts.eclipse.org/mailing-list/codewind-dev).
- **Weekly meetings:** The Codewind team holds [weekly calls](https://github.com/eclipse/codewind/wiki/Codewind-Calls) every Tuesday and Thursday at 9:00 AM Eastern Time / 14:00 UTC.

## Contributing
We welcome submitting issues and contributions.
- :bug: [Submit bugs.](https://github.com/eclipse/codewind/issues)
- :pencil2: [Contribute.](CONTRIBUTING.md)
- :mag_right: [View the API documentation.](https://eclipse.github.io/codewind/)
- :memo: [Improve the docs.](https://github.com/eclipse/codewind-docs)
- :building_construction: [View the Codewind architecture.](https://github.com/codewind-resources/design-documentation)
- :octocat: [See the Codewind repositories.](https://github.com/eclipse?utf8=%E2%9C%93&q=codewind&type=&language=)
- :sparkles: [Check out good first issues for new contributors.](https://github.com/eclipse/codewind/issues?q=is%3Aissue+is%3Aopen+label%3A%22good+first+issue%22)

## Building Codewind from the source
1. Clone the `codewind` repository.
2. Run the `./script/build.sh` script to run the Codewind build, or run the `./run.sh` script to build and start Codewind.

After you build Codewind with the build scripts, you can build one of the IDEs for Codewind:
- For Eclipse, see "Building" in the [`codewind-eclipse` repository](https://github.com/eclipse/codewind-eclipse/blob/master/README.md).
- For VS Code, see "Building Codewind from the source" in the [`codewind-vscode` repository](https://github.com/eclipse/codewind-vscode/blob/master/README.md).

## Developing - Attaching a Node.js debugger in VSCode
Codewind contains two debugging tools for VSCode in the `.vscode/launch.json` file.
To use these you should:
1. Clone the `codewind` repository.
2. Copy the `src/pfe/devbuild-example.env` file to `src/pfe/devbuild.env` to turn on the Node.js inspect (See `src/pfe/portal/package.json`).
3. Run the `./run.sh` script to build and start Codewind.
4. Open the Codewind directory in VSCode (Something like `code github/codewind`).
5. Open the debugging tab and select one of the debugging options.

## License
[EPL 2.0](https://www.eclipse.org/legal/epl-2.0/)
');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (14, 0, '2020-04-29 17:15:16', 'Updated information.', 0, '# EclipseImageJ1Plugin
A repository for an Eclipse ImageJ1 plugin contributed from the Bio7 project.

### New in 1.53.a (1.53a21):

1. Updated ImageJ plugin to version 1.53.a
2. Added new default macro editor templates.

### New in 1.52.v (1.52v19):

1. Updated ImageJ plugin to version 1.52v19
2. Added new default macro editor templates.

### New in 1.52.u (1.52u33):

1. Updated ImageJ plugin to version 1.52u33
2. Fixed a version number display bug.

### New in 1.52.u (1.52u29):

1. Updated ImageJ plugin to version 1.52u29
2. A change of the ImageJ plugin location (''Plugins Path'') in the preferences now automatically updates the plugins classpath and menus (avoids to use the action ''Help->Refresh Menus'' in a second step).
3. Fixed a Compiler path bug for Linux and MacOSX when using the Plugins->''Compile Java Eclipse'' action to compile an ImageJ plugin from the current opened Java editor.

### New in 1.52.u (1.52u28):

1. Updated ImageJ plugin to version 1.52u28
2. Fixed a SWT memory bug not releasing native children of image tabs.
 
### New in 1.52.u (1.52u24):

1. Added a new thumbnail action to the context menu of the ''Navigator'' view to open image and LUT files of a selected directory.
2. LUT''s can now be displayed (preview) in the ''Thumbnails'' view. The selected LUT will be applied on the current supported ImageJ image (grayscale).
3. The Image info is now displayed as a popup in the ''Thumbnails'' view (if you hoover over an item).
4. Added a ''Detach All Images'' action (menu ''Window'') to transfer all opened tab images to a view (which can be moved and arranged like a separate window)
5. Improved dark theme for AWT on Windows, MacOSX and Linux.
6. Added custom text properties to allow future dark themes or disable dark theme color schemes.
7. Improved the canvas layout for HighDPI displays on Windows.
8. Added new default macro editor templates.
9. Improved the default AWT fonts (using OS fonts).

### New in 1.52.q (1.52q48):

1. Fixed a deadlock occuring  on MacoSX when detaching image tabs.
2. Fixed a PlotWindow deadlock when using the action "Close All".

### New in 1.52.q (1.52q46):

1. New debug view toolbar actions availabe to easily debug ImageJ macros (without loosing editor key focus on MacOSX when an image is created or opened).
2. Implemented all default ImageJ debug actions as toolbar actions (one action for ''Trace'' and ''Fast Trace'')
3. New debug functions available to set a variable value in debug mode.
4. New breakpoint actions available to set, edit, delete breakpoints and simple expression breakpoints (evaluate  boolean variable expression) in the ruler menu of the editor. Created breakpoints are automatically stored, too.
5. Extended the editor hoover to display the variable values when you hoover the editor. Array values will be displayed, too.
6. The default array value display table will be opened if you click on the debug view table column.
7. Implemented several text editor and toolbar changes to visualize the debugging process.
8. Fixed several bugs.

### New in 1.52q (1.52q45):

1. Fixed a PlotWindow deadlock

### New in 1.52q (1.52q44):

1. Updated ImageJ plugin to version 1.52q44
2. Improved the embedded and detached PlotWindows. Now all plot windows are simultaneously resized according to their parent canvas (embedded or detached).
3. Fixed a bug in the detach menu action (wrong id and panel settings)
4. Removed the ''*'' tab icon (usually indicating an unsaved editor file) from the detached custom view by deleting the ''ISaveablePart2'' interface

### New in 1.52q (1.52q34):

1. Fixed a toolbar exception when the toolbar was embedded in the Image perspective and not saved detached (eclipse restart)
2. Fixed wrong tab colors

### New in 1.52q (1.52q30):

1. Updated ImageJ plugin to version 1.52q30
2. A new integrated Eclipse ImageJ macro debug view is available as an replacement for the debug dialog (close and reopen the perspective if you update this plugin to add the new view!)
3. Improved the Linux dark theme GUI for embedded AWT and Swing dialogs
4. Improved the ImageJ macro code completion in general
5. Editor defined variables are added to the code completion and shown according to their scope (function or macro defined variables are added when opening the code completion inside this scope. Function arguments are added, too)
6. Editor defined functions are added to code completion.
7. Editor defined function arguments are added as selectable templates
8. Comments and multiline comments of editor defined functions are shown in the context code completion view (must be defined above the function)


### New in 1.52p (1.52p61):

1. Updated ImageJ plugin to version 1.52p61
2. Added Java 12 compiler target
3. Added new macro code completion templates.

### New in 1.52p (1.52p36):

1. Updated ImageJ plugin to version 1.52p36
2. Improved code completion to find and set functions with fixed arguments.
3. Improved hoover information to list all functions with identical name.
4. Added new macro code completion templates.

### New in 1.52p (1.52p30):

1. Updated ImageJ plugin to version 1.52p30
2. Improved automatic layout correction for synchronized detached views (e.g., orthogonal views)
3. Improved titles of detached views to show correct titles and updates (e.g., coordinates of synchronized orthogonal views)
4. Improved the centering of dialogs in a multi-monitor setup (dialogs are centered in the monitor which contains the ImageJ view tabfolder)
5. Improved the code completion cursor location and arguments are now templates selectable with the tab key
6. Improved the code completion sorting of templates
7. Added a preference option to change the critera for code completion search to suggest function templates which contain the prefix
8. Added new file, directory dialogs to insert paths at the current cursor location.
9. Added a color and hex color dialog to select and insert color strings into the editor. Selected hex colors are displayed 
in the color dialog (if created before with the dialog)
10. Added new macro templates
11. Fixed several bugs (e.g., opened dialog option popups in macros, deadlock with SWT dialog in a running macro)

### New in 1.52m (1.52m28):

1. Updated ImageJ plugin to version 1.52m28
2. Implemented dynamic plot scaling when the image tabsize changes
3. Reset Plot action scales plot to default values (context menu "Plot Size")
4. Plot fullscreen scaling support
5. Detached tab plot scaling support
6. Now tabs are visible selected when a window or image is selected (before the selection occured, too but invisible for speed reasons)
7. Implemented the Window components menu to make hidden components visible again (ROI Manager, Threshold, etc. - apparently works on MacOSX and Windows only)
8. Implemented Refresh Menus (if you create and compile a new plugin in the plugins folder with Eclipse (dynamically without restart) you can refresh the menu to update the plugins menu dynamically without a restart!)
9. Implemented the "Help->Examples->Autorun Examples" checkbox menu (true or false)
10. Restored the modal dialogs on Windows
11. Added latest macro API templates

### New in 1.52m (1.52m20):

1. Updated ImageJ version 1.52m (1.52m20)
2. Added new macro function definitions
3. Zoom factor is displayed in the image tabs
4. Workaround a SWT key focus loss with ImageJ on Linux (occurred on Ubuntu 18.10)
5. Fixed an exception when adding an image ("Add Slice")to a stack of size 1
6. Detached images are now closed, too when using the "File->Close All" view menu action

### New in 1.52m (1.52m12):

1. Updated ImageJ version 1.52m (1.52m12)
2. Improved the ImageJ canvas (Plot canvas and Image canvas are now opened as intended)
3. The ImageJ canvas replace method now works as intended (see, e.g., IJ_webcam capture plugin)
4. Improved plot canvas (zooming, actions, etc. are now working as intended)
5. Plot action buttons are now added below each plot (see screenshot below)
6. Added new actions (in the plugins menu) to interpret the current opened Eclipse editor sources (BeanShell, JavaScript, Jython and ImageJ macro) using the ImageJ interpreters (using, e.g., PyDev for Jython).
7. Added a new Java compile action (in the plugins menu) to compile the current opened Eclipse editor source (see screenshot below). To compile dynamically with ImageJ please use or adjust the ImageJ plugins path in the Eclipse preferences.
8. MouseWheel actions are now working as intended (use STRG+MouseWheel to zoom!)
9. Improved stability for drag and drop actions of multiple image files
10. Improved stability for MacOSX
11. Reorganized Eclipse plugins and features
12. ImageJ2 can now be optionally installed and is not bundled by default (now optional dependent of ImageJ). Customization is possible (plugin can be populated with maven command in Eclipse)
13. Added the latest ImageJ macro function templates
14. JavaFX (embed Swing in JavaFX and SWT panel) can now be optionally installed (but is not necessary by default)
15. Removed the outdated Albireo plugin dependency
16. Added key listeners for the tab when all images are closed (ImageJ keys!)
17. Improved the ImageJ macro hoover (workaround for SWT_AWT)
18. Added a workaround for repaint issues in MacOSX when switching perspectives with SWT_AWT
19. Added a workaround for MacOSX to receive key events again after focus lost (sometimes you need to press the mouse two times when the perspectives have been switched)
20. Fixed several rare occuring deadlock events on MacOSX

### New in 1.52h (1.52h5):

1. Updated to ImageJ 1.52h (1.52h5)
2. Added a new rename action (rename all words like the current selection - multiple cursor)
3. Improved dark theme with popular Eclipse darkest dark theme and default dark theme
4. Editor colors are automatically switched when using the dark theme (or darkest dark theme)
5. ImageJ toolbar improved for the dark theme
6. Added a new ImageJ detach image menu in the ?Window? menu of the ImageJ-Canvas view
7. Added all new ImageJ macro API functions to the code completion of the macro editor
8. Improved the ImageJ MacOSX touchpad resizing of the ImageJ panel
9. Improved the default font size for Swing components on Retina displays (e.g., ImageJ components - can be changed in the preferences)
10. Added some new macro source actions (Block Selection, Find And Replace, etc. - submenu Text ) to the macro editor

### New in 1.52c (1.52c12):

1. Updated to ImageJ 1.52c (1.52c12)
2. Updated ImageJ2 libraries (pom-scijava 20.0.0)
3. Added recent macro API commands to code completion
4. Fixed some minor bugs (hoover offset, Linux awt dialogs etc.)

### New in 1.51u (1.51u54):

1. Updated to ImageJ 1.51u (1.51u54)
2. Fixed lost keyboard focus of the ImageJ panel on MacOSX
3. Improved preferences for Swing fonts (to decrease on Retina displays)
4. Several other fixes for MacOSX
5. Added description for font corrections for MacOSX (see bottom of this page!)
6. Improved drag and drop of images on the ImageJ panel

### New in 1.51u20:

1. Cluttered MACOSX menus are now closed (added from opened application windows and frames). Some default added menus are disabled in the Bio7 preferences by default
2. Code completion (template) action now displays the macro documentation and context information when typing
3. Improved code completion layout (context information) 
4. Added debug context menu actions to the macro editor
5. Added some other context menu actions, too (''Get Macro Recorder Text'', ''Evaluate Line'')



### New in 1.51s24:

1. Code completion (templates) action now displays the macro documentation when typing
2. Images from macros are now opened fast (before had a 2s delay)
3. Macros can now be executed within an external ImageJ instance in a separate Java process if enabled in the preferences (Windows only!)
4. Added JavaFX library reference for Java9

### New in 1.51p3:
1. Improved grammar for nested functions in loop and if statements
2. Added more code completion templates
3. Added a new action to insert text of the macro recorder (CTRL+SHIFT+R or CMD+SHIFT+R)

### New in 1.51p (new macro editor available!):

In this relase a macro editor and ImageJ perspective have been added to the ImageJ plugin. The editor has the following features:

1. Dynamic error detection (grammar derived from the ECMA grammar by Bart Kiers)
2. Toolbar action to execute ImageJ macros
3. Syntax highlightening
4. Debugging support (reusing and implementing interfaces and methods from @Wayne - same keyboard shortcuts - see animation below)
5. Code completion (with browser help)
6. Code templates (add you own templates)
7. Info popups (text hoover)
8. Outline view (var variables, variables, methods and macros)
9. Automatic closing of braces, parentheses and strings
Automatic indention of functions,loops, etc.
10. Code folding
11. Code formatting (based on the Eclipse-javascript-formatter by Sebastian Moran)
12. Resize fonts functions (with Ctrl++ or CMD++, CTRL+- or CMD +-)
13. Font preferences
14. Mark occurences (scope independant)
15. A copy function for this forum

### New in 1.51n:

1. Updated ImageJ to 1.51k.
2. Added a fake editor to register and open standard image files from the Navigator or Project Explorer, etc.
3. Fixed several bugs to convert image types

Please note that the update site has changed (due to a cert. problem with on sourceforge) to:

https://bio7.github.io/imagej/

Uninstall the old plugins before you install from the new location!

### New in 1.51k:

1. Updated ImageJ to 1.51k.

2. Reworked the ImageJ plugin view menus. Now all Imagej menus and nested submenus are displayed correctly!

3. Added a menu switch (menu ''Window'') to open images in a Swing in JavaFX in SWT tab (future replacement for SWT_AWT). Images can subsequently be opened in fullscreen with: F1 = primary monitor true fullscreen, F2 = primary monitor fullscreen, F3 = secondary monitor fullscreen, F4 = tertiary monitor fullscreen, F5 = quartary monitor fullscreen.

4. Plugins, scripts and macros are now displayed in their defined menus or submenus.

5. All ImageJ Menus are updated dynamically to recognize copied scripts and macros instantly.

6. Added two preferences to define the install location of plugins and macros.

7. Improved plugin compatibility by going back to AWT in some dialogs.

### Installation:

**Important for MacOSX : Please add the option -Dprism.order=sw in the Eclipse.ini to avoid a crash when using JavaFX!**

**Important for Linux : Please add the option --launcher.GTK_version 2 (don''t forget the linebreak before the version number) in the Eclipse.ini to avoid a crash when using JavaFX (JavaFX doesn''t run with GTK3 on Linux)!**

Add the following URL as update site: 

**https://bio7.github.io/imagej/**

The important views to open are the ImageJ-Canvas which essentially implements the ImageJ interface and the toolbar. You find them in Eclipse under: Window->Show View->Other. First open the ImageJ-Canvas!

**Window->Show View->Other->ImageJ->ImageJ-Canvas (ImageJ panel and menu)**

**Window->Show View->Other->ImageJ->ImageJ-Toolbar (ImageJ toolbar)**

The ImageJ toolbar can also be opened in the ImageJ-Canvas (ImageJ ''Window'' menu).

There is also a self-created thumbnails browser (not speed optimized) for ImageJ default formats included (Thumbnails):

***Window->Show View->Other->ImageJ Extra->Thumbnails***

Ignore the ''Image'' view in the same section which is used to show a detached image (opened with a mouse right-click on a image tab).

Preferences can be found under "Preferences ImageJ".

Plugins can be installed in the ImageJ plugins folder (see the plugin folder inside of the Eclipse ImageJ plugin). They will be added to the plugin menu automatically.

A right-click on an image tab will open the displayed image in an extra view for, e.g., side by side views (x,y,z views). They must be closed separately. 

Fullscreen in default SWT_AWT mode is supported for the primary monitor (Press F2 to open and Esc to abort).

Drag and drop of images and plugins should work, too (Drag them on the view or the ImageJ toolbar to open or install!)

### First start: 

Open the ImageJ perspective (***Window->Perspective->Open perspective->Other->ImageJ Edit*** - or use the toolbar action).

To create an ImageJ macro first create a general eclipse project (***File->New->Project->General Project*** - Other Project types are allowed, too!) and then create an ImageJ macro in the project folder (***New->Other->ImageJ->ImageJ Macro File***).

The ImageJ toolbar can be opened in the ImageJ-Canvas view menu (***Window->ImageJ-Toolbar***)

Images can be dropped in a folder of Eclipse and can be opened from within Eclipse (special image icon available when image extension is detected) in the Eclipse ImageJ view. Unknown image formats (default extensions are used only) can be opened with (***Open with->Other->ImageJ Image Editor***) or simply by a drag and
drop action.

#### Key Commands (also visible in the editor context menu):

1. Insert text at selected line from Macro Recorder: (CTRL+SHIFT+R or CMD+SHIFT+R)
2. Debug (CTRL+D or CMD+D)
3. Debug Abort (CTRL+X or CMD+X)
4. Debug Step  (CTRL+E or CMD+E)
5. Debug Trace (CTRL+T or CMD+T)
6. Debug Run To (CTRL+SHIFT+E or CMD+SHIFT+E)
7. Debug Run To Completion (CTRL+SHIFT+X or CMD+SHIFT+X)
8. Evaluate Line (CTRL+Y or CMD+Y)


#### MacOSX (increase fonts):

To increase the fonts on MacOSX in Eclipse globally add the following lines to the default MacOSX
theme css file in Eclipse (/Contents/Eclipse/plugins/org.eclipse.ui.themes_xxx/css/e4_default_mac.css or /Contents/Eclipse/plugins/org.eclipse.ui.themes_xxx/css/e4-dark_mac.css):

For the default CSS (the color attribute is for the text color!):

	{ 
		color:  #000000; 
		font-size: 14px;      
	}
	
For the dark CSS:

	{ 	      
		font-size: 14px;      
	}
	
For a fine control  of the general font size of Eclipse and MacOSX in general use the Tinker software available here:

<https://www.bresink.com/osx/TinkerTool-de.html>


Finer control (two steps):

To increase the fonts on MacOSX delete the argument (twice available) in the Eclipse.ini
file (Contents/Eclipse/eclipse.ini - see: https://wiki.eclipse.org/Eclipse.ini -):

-Dorg.eclipse.swt.internal.carbon.smallFonts
 
To increase only the ImageJ and Thumbnails view menus font add the following lines to the default MacOSX
theme css file in Eclipse (/Contents/Eclipse/plugins/org.eclipse.ui.themes_xxx/css/e4_default_mac.css):

	#com-eco-bio7-imagej.MToolBar {   
		font-size: 14px;
	}
	
	#com-eco-bio7-thumbnails.MToolBar {   
		font-size: 14px;		
	}

Swing fonts on Retina displays (if necessary):

To decrease oversized Swing fonts on Retina displays go to the ImageJ preferences (Preferences ImageJ->Font Correction) and add a negative value (-8 works as a good correction)

');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (15, 2, '2020-05-02 01:53:48', 'Merge pull request #9 from mattferderer/patch-1

Fix link typo', 1, '# GitDoc ?

GitDoc is a Visual Studio Code extension that allows you to edit git repos like they''re a multi-file, versioned document. This gives you the simplicity of a Google/Word Doc (creating "snapshots" by saving, not by running `git commit`), but with the richness of git history, and the ability to easily [share](https://github.com) your work. You can enable these auto-commits during specific periods (e.g. when you''re working on a feature branch and want to track the evolution of a change), permanently on [specific branches](#auto-commiting-specific-branches) (e.g. you have a `docs` repo that you want to version like a document), or only for [specific files](#auto-commiting-specific-files) (e.g. auto-commmit `*.md` files, but nothing else). This allows you to easily switch between "versioning modalities", in order to support the diverse set of use cases that can benefit from being stored in a git repo (e.g. team projects, your personal blog, school work, etc.)

Commits are only created for [error-free code](#error-detection), which allows you to author and save changes, knowing you aren''t accidentally commiting invalid states. Additionally, just because you''re auto-commmiting your changes, doesn''t mean you lose control over your version history. When needed, you can easily [restore](#restoring-versions), [undo](#undoing-changes), and/or [squash](#squashing-versions) versions, without needing to memorize the [magic of git](https://sethrobertson.github.io/GitFixUm/fixup.html). Additionally, you can opt into [auto-pushing](#auto-pushing) your changes to a remote, in order to treat your repo as a fully synchronized document source.

<img width="700px" src="https://user-images.githubusercontent.com/116461/79521572-5a3bfe00-800e-11ea-83a0-8e125122fa8f.gif" />

## Getting Started

1. Install this extension
2. Run the `GitDoc: Enable` command, and notice that your status bar now includes a `GitDoc` button. This indicates that `GitDoc` is enabled ?
3. Open a file, make a change, and then save it
4. Open the `Timeline` view on the `Explorer` tab (or run `git log`), and within 30s, notice that a new commit was created on your behalf
5. Select the top item in the `Timeline` view to see the diff of the change you just made
6. Continue to make changes, knowing that they''ll be automatically tracked for you (as long as they don''t contain [errors](#error-detection)) ?
7. At any time, you can [restore](#restoring-versions), [undo](#undoing-changes), and/or [squash](#collapsing-versions) versions from the `Timeline`, in order to "clean" up/manage your history
8. When you''re done, simply click the `GitDoc` button in your status bar, run the `GitDoc: Disable` command, or close/reload VS Code

From here, you can permanently enable auto-commits for an entire repo or [branch](#auto-commiting-specific-branches), customize the [files that are auto-committed](#auto-commiting-specific-files), and even [auto-push your changes](#auto-pushing) in order to keep your repo fully synced.

## Auto-commiting

By default, when you enable `GitDoc`, it will create commits every 30s, whenever there are actually changes. So if you don''t make any changes, than it won''t make any commits. However, if you''re continuously writing code, then it''s only going to capture those edits in 30s intervals. This way, you don''t generate a massive number of commits, depending on how frequently you save files. If you find that 30s is too short or too long, you can customize this by setting the `GitDoc: Auto Commit Delay` setting to the appropriate value.

## Error Detection

By default, auto-commits are only made when a file is changed, and it doesn''t have any pending errors (e.g. issues in the `Problems` panel with an `error` severity). This prevents you from creating commits that represent invalid changes, and allows you to install whatever linting/testing/build extensions you want, knowing that they''ll "gate" your auto-commits. If you want to suppress commits in the presence of warnings, or ignore problems entirely, and simply always auto-commit, then you can set the `Commit Validation Level` setting to `warning` or `none` respectively.

<img width="600px" src="https://user-images.githubusercontent.com/116461/80316927-8c3f2400-87b5-11ea-9e6f-62b28ec3b4ce.gif" />

## Auto-saving

When you enable `GitDoc`, it creates a new commit anytime you save a file. This allows you to control when commits are created, simply be determining when you save. You can save a single file, or multiple files, and all of the changes within a single "save operation" will be committed together.

If you''d like to automatically track your changes, without needing to explicitly save, then simply set the `Files: Auto Save` setting, specifying the exact behavior you''d like (e.g. save every 30s).

## Auto-commiting specific branches

In addition to enabling `GitDoc` during temporary periods, you can also choose to enable it on specific branches, in order to automatically track your work for as long as you''re using the branch. Simply switch to the desired branch and run `GitDoc: Enable (Branch)`.

## Auto-commiting specific files

If you''d like to only enable auto-commiting for specific files, you can set the `GitDoc: File Pattern` setting to a file glob. For example, you could set this to `**/*.md` in order to auto-commit markdown files, but nothing else. By default, this is set to `**/*`, which auto-commits changes to any file.

When this setting is set, the `GitDoc` [status bar](#status-bar) which only appear when you have a file that is matches it. This way, you can easily tell when you''re editing a file that will be auto-committed/pushed.

## Auto-pushing

In addition to automatically created commits, you can also choose to automatically push your changes, by setting the `GitDoc: Auto Push` setting. By default, this setting is set to `off`, but you can set it to `onCommit` in order to push every time a commit is made, or `afterDelay` in order to push on some sort of frequency. If you set it to the later, then you can control the delay duration by setting the `GitDoc: Auto Push Delay` setting.

> Note that when you enable this, GitDoc will actually perform a `git push --force`, since certain operations such as [`squashing`](#squashing-versions) can actually re-write history. So before you enable this, be sure that you''re comfortable with this behavior.

## Squashing versions

Auto-committing is useful for tracking unique versions, however, depending on how frequently you save, you could end up creating a significant number of file versions. If a series of versions represent a single logical change, you can "squash" them together by right-clicking the oldest version in the `Timeline` tree and selecting the `Squash Version(s) Above` command. You can give the new version a name, and when submitted, the selected version, and all versions above it, will be "squashed" into a single version.

<img width="700px" src="https://user-images.githubusercontent.com/116461/79668805-3c84ab00-816c-11ea-9ec9-845650b999b8.gif" />

> Demystification: Behind the scenes, this command performs a `git reset --soft`, starting at the commit _before_ the selected one. It then runs `git commit -m <message>`, where `message` is the string you specified. This preserves your working directory/index, while "rewritting" the commit history.

## Restoring versions

If you''ve made a bunch of changes to a file, and want to restore an older version, simply open up the `Timeline` tree, right-click the desired version, and select `Restore Version`.

> Demystification: Behind the scenes, this command peforms a `git checkout -- <file>` (on the file that''s associated with the selected timeline item), followed by `git commit` (in order to commit the restoration). This way, the restore is a "forward moving" operation.

## Undoing versions

If you made a change, that you want to undo, you can simply open up the `Timeline` view, right-click the version you want to undo, and select `Undo Version`. This will create a new version that undos the changes that were made in the selected version. This way, any undo action is actually a "forward edit", that you can then undo again if needed.

> Demystification: Behind the scenes, this command simply performs a `git revert` on the selcted commit. Because this is a "safe" action, you can generally perform it without any problems (including on shared branches), since it doesn''t re-write history.

## Status Bar

Whenever `GitDoc` is enabled, it will contribute a status bar item to your status bar. This simply indicates that it''s enabled, and makes it easier for you to know which "versioning mode" you''re in (auto-commit vs. manual commit). Additionally, if you enable [auto-pushing](#auto-pushing), then the status bar will indicate when it''s syncing your commits with your repo.

If you click the `GitDoc` status bar item, this will disable `GitDoc`. This allows you to easily enable GitDoc for a period of time, and then quickly turn it off. Note that if you''ve enabled GitDoc on a branch, then clicking the status bar only temporarily disables it, and it will become re-enabled the next time that you reload/open VS Code. If you want to permanently disable GitDoc for the current branch, run the `GitDoc: Disable (Branch)` command.

## Contributed Commands

When you install the `GitDoc` extension, the following commands are contributed to the command palette, and are visible when your open workspace is also a git repository:

- `GitDoc: Enable` - Temporarily enables auto-commits. This command is only visible when GitDoc isn''t already enabled.

- `GitDoc: Enable (Branch)` - Enables auto-commits on the current branch, and therefore, the setting will persist across reloading VS Code. This command is only visible when GitDoc isn''t already enabled.

- `GitDoc: Disabled` - Disables auto-commits. If auto-commits were enabled on this branch, then runnning this command only temporarily disables it, and it will be re-enabled when you reload/re-open VS Code. This is useful if you have a branch that you generally want to auto-commit on, but you want to turn it off for a certain period/editing session. This command is only visible when GitDoc is already enabled.

- `GitDoc: Disabled (Branch)` - Disables auto-commits on the current branch. This command is only visible when GitDoc is already enabled for the current branch.

## Contributed Settings

The following settings enable you to customize the default behavior of `GitDoc`:

- `GitDoc: Auto Commit Delay` - Controls the delay in ms after which any changes are automatically committed. Only applies when `GitDoc: Enabled` is set to `true`. Defaults to `30000` (30s).

- `GitDoc: Autopush` - Specifies whether to automatically push changes to the current remote. Can be set to one of the following values: `afterDelay`, `onCommit` or `off`. Defaults to `off`.

- `GitDoc: Autopush Delay` - Controls the delay in ms after which any commits are automatically pushed. Only applies when `GitDoc: Auto Push` is set to `afterDelay`. Defaults to `30000`.

- `GitDoc: Commit Message Format` - Specifies the [moment.js](https://momentjs.com/) format string to use when generating auto-commit messages. Defaults to `LLL`.

- `GitDoc: Commit Validation Level` - Specifies whether to validate that a file is free of problems, before attempting to commit changes to it. Defaults to `error`.

- `GitDoc: Enabled` - Specifies whether to automatically create a commit each time you save a file.

- `GitDoc: File Pattern` - Specifies a glob that indicates the exact files that should be automatically committed. This is useful if you''d like to only [auto-commiting specific files](#auto-commiting-specific-files), as opposed to an entire branch.
');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (16, 27, '2020-04-19 23:58:43', 'Adding notebook gif', 3, '# CodeTour ??

CodeTour is a Visual Studio Code extension, which allows you to record and playback guided walkthroughs of your codebases. It''s like a virtual brownbag, or table of contents, that can make it easier to onboard (or reboard!) to a new project/feature area, visualize bug reports, or understand the context of a code review/PR change. A "code tour" is simply a series of interactive steps, each of which are associated with a specific file/line, and include a description of the respective code. This allows developers to clone a repo, and then immediately start **learning it**, without needing to refer to a `CONTRIBUTING.md` file and/or rely on help from others. Tours can either be checked into a repo, to enable sharing with other contributors, or [exported](#exporting-tours) to a "tour file", which allows anyone to replay the same tour, without having to clone any code to do it!

<img width="800px" src="https://user-images.githubusercontent.com/116461/76165260-c6c00500-6112-11ea-9cda-0a6cb9b72e8f.gif" />

## Starting a tour

In order to start a tour, simply open up a codebase that has one or more tours. If this is the first time you''ve ever opened this codebase, you''ll be presented with a toast notification asking if you''d like to take a tour of it.

<img width="400px" src="https://user-images.githubusercontent.com/116461/76691619-d0ada080-6609-11ea-81bf-c1f022dbff43.png" />

Otherwise, you can manually start a tour via any of the following methods:

1. Clicking the `> Start CodeTour` button in the [status bar](#status-bar)
2. Selecting a tour (or specific step) in the [`CodeTour` view](#tree-view) in the `Explorer` activity tab

   <img width="250px" src="https://user-images.githubusercontent.com/116461/76164362-8610bd80-610b-11ea-9621-4ba2d47a8a52.png" />

3. Running the `CodeTour: Start Tour` [command](#contributed-commands), and selecting the tour you''d like to take

<img width="800px" src="https://user-images.githubusercontent.com/116461/76151694-7b531b80-606c-11ea-96a6-0655eb6ab4e6.gif" />

If the current workspace only has a single code tour, then any of the above actions will automatically start that tour. Otherwise, you''ll be presented with a list of tours to select from.

### Opening a tour

In addition to taking tours that are part of the currently open workspace, you can also open a tour file that someone else sent you and/or you created yourself. Simply run the `CodeTour: Open Tour File...` command and/or click the folder icon in the title bar of the `CodeTour` tree view.

> Note: The `CodeTour` tree view only appears if the currently opened workspace has any tours and/or you''re currently taking a tour.

Additionally, if someone has [exported](#exporting-tours) a tour, and uploaded it to a publically accessible location, they can send you the URL, and you can open it by running the `CodeTour: Open Tour URL...` command.

### Tour markers

As you explore a codebase, you might encounter a "tour marker", which displays the CodeTour icon in the file gutter. This indicates that a line of code participates in a tour for the open workspace, which makes it easier to discover tours that might be relevant to what you''re currently working on. When you see a marker, simply hover over the line and click the `Start Tour` link in the hover tooltip. This will start the tour that''s associated with this line of code, at the specific step.

<img width="800px" src="https://user-images.githubusercontent.com/116461/78101204-9aa74500-739b-11ea-8a1e-dea923910524.gif" />

If you want to disable tour markers, you can perform one of the following actions:

- Run the `CodeTour: Hide Tour Markers` command
- Click the "eye icon" in the title bar of the `CodeTour` tree view
- Set the `codetour.showMarkers` configuration setting to `false`. _Note that the above two actions do this for you automatically._

### Notebook View

In addition to taking a tour through a series of files, you can also view a tour as a "notebook", which displays the tour''s steps within a single document. Simply right-click a tour in the `CodeTour` tree and select `View Notebook`.

<img width="700px" src="https://user-images.githubusercontent.com/116461/79699658-bd63a580-8245-11ea-81cc-6208e2784acf.gif" />

## Navigating a tour

Once you''ve started a tour, the comment UI will guide you, and includes navigation actions that allow you to perform the following:

- `Move Previous` - Navigate to the previous step in the current tour. This command is visible for step #2 and later.
- `Move Next` - Navigate to the next step in the current tour. This command is visible for all steps but the last one in a tour.
- `Edit Tour` - Begin editing the current tour (see [authoring](#authoring-tours) for details). Note that not all tours are editable, so depending on how you started the tour, you may or may not see this action.
- `End Tour` - End the current tour and remove the comment UI.

<img width="500px" src="https://user-images.githubusercontent.com/116461/76151723-ca00b580-606c-11ea-9bd5-81c1d9352cef.png" />

Additionally, you can use the `ctrl+right` / `ctrl+left` (Windows/Linux) and `cmd+right` / `cmd+left` (macOS) keyboard shortcuts to move forwards and backwards in the tour. The `CodeTour` tree view and status bar is also kept in sync with your current tour/step, to help the developer easily understand where they''re at in the context of the broader tour.

<img width="800px" src="https://user-images.githubusercontent.com/116461/76807453-a1319c00-67a1-11ea-9b88-7e448072f33d.gif" />

If you navigate away from the current step and need to resume, you can do that via any of the following actions:

- Right-clicking the active tour in the `CodeTour` tree and selecting `Resume Tour`
- Clicking the `CodeTour` status bar item
- Running the `CodeTour: Resume Tour` command in the command palette

At any time, you can end the current code tour by means of one of the following actions:

- Click the stop button (the red square) in the current step comment
- Click the stop button next to the active tour in the `CodeTour` tree
- Running the `CodeTour: End Tour` command in the command palette

## Authoring tours

If you''d like to create a code tour for your codebase, you can simply click the `+` button in the `CodeTour` tree view (if it''s visible) and/or run the `CodeTour: Record Tour` command. This will start the "tour recorder", which allows you to begin opening files, clicking the "comment bar" for the line you want to annotate, and then adding the respective description (including markdown!). Add as many steps as you want, and then when done, simply click the stop tour action (the red square button).

While you''re recording, the `CodeTour` [tree view](#tree-view) will display the currently recorded tour, and it''s current set of steps. You can tell which tour is being recorded because it will have a microphone icon to the left of its name.

<img width="800px" src="https://user-images.githubusercontent.com/116461/76165260-c6c00500-6112-11ea-9cda-0a6cb9b72e8f.gif" />

If you need to edit or delete a step while recording, click the `...` menu next to the step''s description, and select the appropriate action.

<img width="500px" src="https://user-images.githubusercontent.com/116461/76168548-1f50cb80-612e-11ea-9aca-8598b9e1c730.png" />

### Workspaces

If you record a tour within a "multi-root workspace", you''ll be asked to select the folder that you''d like to save the tour to. This is neccessary because tours are written as [files](#tour-files) to your workspace, and so we need to disamgiuate which folder the tour should be persisted to. That said, when you''re recording a tour, you can add steps that span any of the folders within the workspace, which allows you to create tours for a specific folder and/or that demonstrate concepts across multiple folders within the workspace.

### Step titles

By default, the `CodeTour` tree displays each tour step using the following display name format: `#<stepNumber> - <filePath>`. However, if you''d like to give the step a more friendly name, you can edit the step''s description and add a markdown heading to the top of it, using whichever heading level you prefer (e.g. `#`, `##`, etc.). For example, if you add a step whose description starts with `### Activation`, the step and tree view would look like the following:

<img width="500px" src="https://user-images.githubusercontent.com/116461/76721235-91ac4780-66fc-11ea-80bf-f6de8bf4b02e.png" />

While you can any heading level for the step title, we''d recommend using `###` or "lower" (e.g. `####`), since that provides a nice look within the step''s comment UI.

> If you''d like to add step titles to a tour, but don''t want to add markdown headings to their descriptions, you can manually set the `title` property for each step in the tour''s underlying `JSON` file (which can be found in the `.vscode/tours` directory). See [tour schema](#tour-schema) for more detials.

### Recording text selection

By default, each step is associated with the line of code you created the comment on (i.e. the line you clicked the `+` on the comment bar for). However, if you want to call out a specific span of code as part of the step, simply highlight the code before you add the step (clicking the `Add Tour to Step` button), and the selection will be captured as part of the step.

<img width="800px" src="https://user-images.githubusercontent.com/116461/76705627-b96cc280-669e-11ea-982a-d754c4f001aa.gif" />

If you need to tweak the selection that''s associated with a step, simply edit the step, reset the selection and then save it.

### Re-arranging steps

While you''re recording a tour, each new step that you add will be appended to the end of the tour. However, you can move existing steps up and down in the order by performing one of the following actions:

- Hover over the step in the `CodeTour` tree and click the up/down arrow icon
- Right-click the step in the `CodeTour` tree and select the `Move Up` or `Move Down` menu items
- Click the `...` menu in the step comment UI and select `Move Up` or `Move Down`

Additionally, if you want to add a new step in the middle of a tour, simply navigate to the step that you want to insert a new step after, and then create the new step.

### Deleting steps

If you no longer need a specific step in a tour, you can delete it by means of one of the following actions:

- Right-clicking the step in the `CodeTour` tree and selecting `Delete Step`
- Navigating to the step in the replay/comment UI, selecting the `...` menu next to the comment description and selecting `Delete Step`

### Editing a tour

If you want to edit an existing tour, simply right-click the tour in the `CodeTour` tree and select `Edit Tour`. Alternatively, you can edit a tour you''re actively viewing by clicking the pencil icon in the current step''s comment bar, or running the `CodeTour: Edit Tour` command.

At any time, you can right-click a tour in the `CodeTour` tree and change it''s title, description or git ref, by selecting the `Change Title`, `Change Description` or `Change Git Ref` menu items. Additionally, you can delete a tour by right-clicking it in the `CodeTour` tree and seelcting `Delete Tour`.

### Shell Commands

In order to add more interactivity to a tour, you can embed shell commands into a step (e.g. to perform a build, run tests, start an app), using the special `>>` synax, followed by the shell command you want to run (e.g. `>> npm run compile`). This will be converted into a hyperlink, that when clicked, will launch a new integrated terminal (called `CodeTour`) and will run the specified command.

<img width="600px" src="https://user-images.githubusercontent.com/116461/78858896-91912600-79e2-11ea-8002-196c12273ebc.gif" />

### Versioning tours

When you record a tour, you''ll be asked which git "ref" to associate it with. This allows you to define how resilient you want the tour to be, as changes are made to the respective codebase.

<img width="600px" src="https://user-images.githubusercontent.com/116461/76692462-3f8ff700-6614-11ea-88a1-6fbded8e8507.png" />

You can choose to associate with the tour with the following ref types:

- `None` - The tour isn''t associated with any ref, and therefore, the file/line numbers in the tour might get out of sync over time. The benefit of this option is that it enables the code to be edited as part of the tour, since the tour will walk the user through whichever branch/commit they have checked out.
- `Current Branch` - The tour is restricted to the current branch. This can have the same resiliency challenges as `None`, but, it allows you to maintain a special branch for your tours that can be versioned seperately. If the end-user has the associated branch checked out, then the tour will enable them to make edits to files as its taken. Otherwise, the tour will replay with read-only files.
- `Current Commit` - The tour is restricted to the current commit, and therefore, will never get out of sync. If the end-user''s `HEAD` points at the specified commit, then the tour will enable them to make edits to files as its taken. Otherwise, the tour will replay with read-only files.
- Tags - The tour is restricted to the selected tag, and therefore, will never get out of sync. The repo''s entire list of tags will be displayed, which allows you to easily select one.

At any time, you can edit the tour''s ref by right-clicking it in the `CodeTour` tree and selecting `Change Git Ref`. This let''s you "rebase" a tour to a tag/commit as you change/update your code and/or codebase.

### Tour Files

Behind the scenes, the tour will be written as a JSON file to the `.tours` directory of the current workspace. This file is pretty simple and can be hand-edited if you''d like. Additionally, you can manually create tour files, by following the [tour schema](#tour-schema). You can then store these files to the `.tours` (or `.vscode/tours`) directory, or you can also create a tour at any of the following well-known locations: `.tour`, `main.tour`

Within the `.tours` (or `.vscode/tours`) directory, you can organize your tour files into arbitrarily deep sub-directories, and the CodeTour player will properly discover them.

### Exporting Tours

By default, when you record a tour, it is written to the currently open workspace. This makes it easy to check-in the tour and share it with the rest of the team. However, there may be times where you want to record a tour for yourself, or a tour to help explain a one-off to someone, and in those situations, you might not want to check the tour into the repo.

So support this, after you finish recording a tour, you can right-click it in the `CodeTour` tree and select `Export Tour...`. This will allow you to save the tour to a new location, and then you can delete the tour file from your repo. Furthermore, when you export a tour, the tour file itself will embed the contents of all files needed by the tour, which ensures that someone can play it back, regardless if the have the respective code available locally. This enables a powerful form of collaboration.

<img width="700px" src="https://user-images.githubusercontent.com/116461/77705325-9682be00-6f7c-11ea-9532-6975b19b8fcb.gif" />

#### GitHub Gists

If you install the [GistPad](https://aka.ms/gistpad) extension, then you''ll see an additional `Export Tour to Gist...` option added to the `CodeTour` tree. This lets you export the tour file to a new/existing gist, which allows you to easily create your own private tours and/or create tours that can be shared with others on your team.

Once a tour is exported as a gist, you can right-click the `main.tour` file in the `GistPad` tree, and select `Copy GitHub URL`. If you send that to someone, and they run the `CodeTour: Open Tour URL...` command, then they''ll be able to take the exact same tour, regardless if they have the code locally available or not.

### Tour Schema

Within the tour file, you need to specify the following required properties:

- `title` - The display name of the tour, which will be shown in the `CodeTour` tree view, quick pick, etc.
- `description` - An optional description for the tour, which will be shown as the tooltip for the tour in the `CodeTour` tree view
- `ref` - An optional "git ref" (branch/tag/commit) that this tour applies to. See [versioning tours](#versioning-tours) for more details.
- `steps` - An array of tour steps
  - `file` - The file path (relative to the workspace root) that this step is associated with
  - `uri` - An absolute URI that this step is associated with. Note that `uri` and `file` are mutually exclusive, so only set one per step
  - `line` - The 1-based line number that this step is associated with
  - `title` - An optional title, which will be displayed as the step name in the `CodeTour` tree view.
  - `description` - The text which explains the current file/line number, and can include plain text and markdown syntax

For an example, refer to the `.tours/tree.tour` file of this repository.

## Tree View

If the currently opened workspace has any code tours, or you''re actively taking/recording a tour, you''ll see a new tree view called `CodeTour`, that''s added to the `Explorer` tab. This view simply lists the set of available code tours, along with their title and number of steps. If you select a tour it will start it, and therefore, this is simply a more convenient alternative to running the `CodeTour: Start Tour` command. However, you can also expand a tour and start it at a specific step, edit/delete steps, re-order steps, and change the tour''s description/title/git ref.

<img width="250px" src="https://user-images.githubusercontent.com/116461/76164362-8610bd80-610b-11ea-9621-4ba2d47a8a52.png" />

Additionally, the tree view will display the tour currently being [recorded](#authoring-tours), which makes it easy to track your status while in the process of creating a new tour.

> The tree view is automatically kept up-to-date, as you add/edit/delete tours within the current workspace. So feel free to [record](#authoring-tours) and/or edit tours, and then navigate them when done.

## Status Bar

In addition to the `CodeTour` tree view, the CodeTour extension also contributes a new status bar item called `Start CodeTour` to your status bar. It''s only visible when the current workspace has one or more tours, and when clicked, it allows you to select a tour and then begin navigating it.

While you''re within a tour, the status bar will update to show the title and step of the current tour. When clicked, it will open the file/line of the current tour step, which allows you to open other files while taking a tour, and then resume the tour when ready. Once you end the current tour, the status bar will transition back to displaying the `Start CodeTour` button.

> If you don''t want to display the status bar item, simply right-click it and select `Hide CodeTour (Extension)`.

## Contributed Commands

In addition to the `CodeTour` tree view and the status bar item, the CodeTour extension also contributes the following commands to the command palette:

- `CodeTour: Open Tour File...` - Allows you to select a tour file that was previously [exported](#exporting-tours).

- `CodeTour: Record Tour` - Starts the [tour recorder](#authoring-tours), which allows you to create a new tour by creating a sequence of steps.

* `CodeTour: Start Tour` - Starts a tour for the currently opened workspace. This command is only visible if the current workspace has one or more code tours.

* `CodeTour: Refresh Tours` - Refreshes the [`CodeTour` view](#tree-view), which can be handy if you''d created/modified/deleted tour files on disk. This command is only visible if the current workspace has one or more code tours.

* `CodeTour: Hide Tour Markers` - Hides [tour markers](#tour-markers). This command is only visible if the current workspace has one or more code tours, and tour markers are currently visible.

* `CodeTour: Show Tour Markers` - Shows [tour markers](#tour-markers). This command is only visible if the current workspace has one or more code tours, and tour markers are currently hidden.

- `CodeTour: Edit Tour` - Puts the currently active tour into edit more. This command is only visible while you''re actively playing a tour, that you''re not already editing.

- `CodeTour: End Tour` - Ends the currently active tour. This command is only visible while you''re actively recording/playing a tour.

- `CodeTour: Resume Current Tour` - Resumse the current tour by navigating to the file/line number that''s associated with the current step. This command is only visible while you''re actively recording/playing a tour.

## Configuration Settings

The `CodeTour` extension contributes the following settings:

- `codetour.showMarkers` - Specifies whether or not to show [tour markers](#tour-markers). Defaults to `true`.

## Keybindings

In addition to the available commands, the Code Tour extension also contributes the following commands, which are active while you''re currently taking a tour:

- `ctrl+right` (Windows/Linux), `cmd+right` (macOS) - Move to the next step in the tour
- `ctrl+left` (Windows/Linux) `cmd+left` (macOS) - Move to the previous step in the tour

## Extension API (Experimental)

In order to enable other extensions to contribute/manage their own code tours, the CodeTour extension exposes an API with the following methods:

- `startTour(tour: CodeTour, stepNumber: number, workspaceRoot: Uri, startInEditMode: boolean = false, canEditTour: boolean): void` - Starts the specified tour, at a specific step, and using a specific workspace root to resolve relative file paths. Additionally, you can specify whether the tour should be started in edit/record mode or not, as well as whether the tour should be editable. Once the tour has been started, the end-user can use the status bar, command palette, key bindings and comment UI to navigate and edit the tour, just like a "normal" tour.

- `endCurrentTour(): void` - Ends the currently running tour (if there is one). Note that this is simply a programatic way to end the tour, and the end-user can also choose to end the tour using either the command palette (running the `CodeTour: End Tour` command) or comment UI (clicking the red square, stop icon) as usual.

- `exportTour(tour: CodeTour): Promise<string>` - Exports a `CodeTour` instance into a fully-embedded tour file, that can then be written to some persistent storage (e.g. a GitHub Gist).
');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (17, 10, '2020-05-01 20:50:22', 'Merge pull request #319 from badsyntax/runtask-api

Add runTask api', 257, '# VS Code Gradle Tasks

[![Marketplace Version](https://vsmarketplacebadge.apphb.com/version-short/richardwillis.vscode-gradle.svg)](https://marketplace.visualstudio.com/items?itemName=richardwillis.vscode-gradle)
[![Build status](https://img.shields.io/github/workflow/status/badsyntax/vscode-gradle/Build)](https://github.com/badsyntax/vscode-gradle/actions?query=workflow%3ABuild)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=badsyntax_vscode-gradle&metric=security_rating)](https://sonarcloud.io/dashboard?id=badsyntax_vscode-gradle)
[![Visual Studio Marketplace Installs](https://img.shields.io/visual-studio-marketplace/i/richardwillis.vscode-gradle)](https://marketplace.visualstudio.com/items?itemName=richardwillis.vscode-gradle)
[![GitHub bug issues](https://img.shields.io/github/issues/badsyntax/vscode-gradle/bug?label=bug%20reports)](https://github.com/badsyntax/vscode-gradle/issues?q=is%3Aissue+is%3Aopen+label%3Abug)

Run Gradle tasks in VS Code.

![Gradle Tasks Screencast](images/screencast.gif)

## Features

This extension provides a visual interface for your Gradle build. You can view Gradle projects and run Gradle tasks.

This extension supports whatever Gradle supports and is language/project agnostic, but it can work nicely alongside other extensions like the [Java language support extension](https://github.com/redhat-developer/vscode-java).

? [All Features](./FEATURES.md)

## Requirements

- [Java >= 8](https://adoptopenjdk.net/) must be installed
- Local Gradle wrapper executables must exist at the root of the workspace folders

## Settings

This extension contributes the following settings:

- `gradle.autoDetect`: Automatically detect Gradle tasks
- `gradle.enableTasksExplorer`: Enable an explorer view for Gradle tasks
- `gradle.taskPresentationOptions`: Task presentation options, see [tasks output behaviour](https://code.visualstudio.com/docs/editor/tasks#_output-behavior)
- `gradle.focusTaskInExplorer`: Focus the task in the explorer when running a task
- `gradle.javaDebug`: Debug JavaExec tasks (see below for usage)
- `gradle.debug`: Show extra debug info in the output panel

This extension supports the following settings:

- `java.home`: Absolute path to JDK home folder used to launch the gradle daemons (Contributed by [vscode-java](https://github.com/redhat-developer/vscode-java))
- `java.import.gradle.user.home`: Setting for GRADLE_HOME (Contributed by [vscode-java](https://github.com/redhat-developer/vscode-java))

## Supported Environment Variables

Most of the standard Java & Gradle environment variables are supported:

- `JAVE_HOME` (overridden by `java.home`)
- `GRADLE_USER_HOME` (overridden by `java.import.gradle.user.home`)

### Setting Project Environment Variables

You can use an environment manager like [direnv](https://direnv.net/) to set project specific environment variables, or set the variables in the terminal settings within `.vscode/settings.json`, for example:

```json
{
  "terminal.integrated.env.osx": {
    "GRADLE_USER_HOME": "${workspaceFolder}/.gradle"
  }
}
```

Note, the VS Code settings take precedence over the environment variables.

## Debugging JavaExec Tasks

![Debug Screencast](images/debug-screencast.gif)

This extension provides an experimental feature to debug [JavaExec](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.JavaExec.html) tasks. Before using this feature you need to install the [Debugger for Java](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-debug) and [Language Support for Java](https://marketplace.visualstudio.com/items?itemName=redhat.java) extensions.

To enable this feature you need to specify which tasks can be debugged within your project `.vscode/settings.json` file:

```json
"gradle.javaDebug": {
    "tasks": [
        "run",
        "test",
        "subproject:customJavaExecTask"
    ]
}
```

You should now see a `debug` command next to the `run` command in the Gradle Tasks view. The `debug` command will start the Gradle task with [jdwp](https://docs.oracle.com/en/java/javase/11/docs/specs/jpda/conninv.html#oracle-vm-invocation-options) `jvmArgs` and start the vscode Java debugger.

### Debugging Limitations

You''ll need to remove any `jdwp` options that might have been set in your task configuration (eg via `jvmArgs`).

## Snippets

This extensions provides snippets for the groovy and kotlin build files:

- `cgt`: Create a new Gradle task

## Extension API

This extension exposes a `runTask` api with the following definition:

```ts
(
  projectFolder: string, // absolute path of root project folder
  taskName: string,
  args?: string[],
  onOutput?: (output: Output) => void
) => Promise<void>;
```

You can use this API to run Gradle tasks. It doesn''t matter when you call this method as it will wait for tasks to be loaded before running the task.

## Troubleshooting

<details><summary>View logs by selecting "Gradle Tasks" in the output panel</summary>

<img src="./images/output.png" width="600" />

</details>

<details><summary>Task output will be shown in the Terminal panel</summary>

<img src="./images/terminal.png" width="600" />

</details>

<details><summary>Set the "gradle.debug" setting to "true" to view debug logs in the output panel</summary>

<img src="./images/debug-output.png" width="600" />

</details>

<details><summary>"No connection to the gradle server. Try restarting the server"</summary>

<img src="./images/no-connection.png" width="500" />

This error means the Gradle Task server has stopped, or there was an error starting it. Click on "Restart Server" to restart it.

If you continue to get this error, view the task error messages by selecting "Gradle Tasks Server" in the Terminal panel.

The task server is started using a [shell script](https://gist.github.com/badsyntax/d71d38b1700325f31c19912ac3428042) generated by [CreateStartScripts](https://docs.gradle.org/current/dsl/org.gradle.jvm.application.tasks.CreateStartScripts.html). The script uses `#!/usr/bin/env sh` and is as portable as the gradle wrapper script. If there are any problems executing the start script then it''s likely an issue either with your `$PATH`, or java was not installed.

### PATH problems

The following error demonstrates a typical issue with your `$PATH`:

```shell
env: sh: No such file or directory
The terminal process terminated with exit code: 127
```

Use the following task to debug your shell environment within vscode:

```json
{
  "version": "2.0.0",
  "tasks": [
    {
      "label": "Print task shell info",
      "type": "shell",
      "command": "echo \\"Path: $PATH \\nShell: $SHELL\\"",
      "problemMatcher": []
    }
  ]
}
```

#### Fixing your `$PATH`

Check your dotfiles (eg `~/.bashrc`, `~/.bash_profile`, `~/.zshrc`) and fix any broken `PATH` exports, or override the `PATH` env var by setting `terminal.integrated.env` in your vscode settings, for example:

```json
"terminal.integrated.env.osx": {
  "PATH": "/put/your:/paths/here",
}
```

### Java path problems

You might see an error like:

```shell
ERROR: JAVA_HOME is not set and no ''java'' command could be found in your PATH.
```

The start script [should find](https://gist.github.com/badsyntax/d71d38b1700325f31c19912ac3428042#file-gradle-tasks-server-sh-L85-L105) the path to Java in the usual locations. If you get this error it suggests an issues with your `$PATH` or you simply haven''t installed Java. Run the gradle wrapper script (eg `./gradlew tasks`) to debug further.

</details>

<details><summary>Incompatibility with other extensions</summary>

This extension is incompatible with the following extensions:

- [spmeesseman.vscode-taskexplorer](https://marketplace.visualstudio.com/items?itemName=spmeesseman.vscode-taskexplorer)

The reason for the incompatibility is due to the extensions providing the same tasks types (`gradle`) with different task definitions.

</details>

## Support

For general support queries, use the [#gradle-tasks](https://vscode-dev-community.slack.com/archives/C011NUFTHLM) channel in the [slack development community workspace](https://aka.ms/vscode-dev-community), or

- ? [Submit a bug report](https://github.com/badsyntax/vscode-gradle/issues/new?assignees=badsyntax&labels=bug&template=bug_report.md&title=)
- ? [Submit a feature request](https://github.com/badsyntax/vscode-gradle/issues/new?assignees=badsyntax&labels=enhancement&template=feature_request.md&title=)

## Contributing

Refer to [CONTRIBUTING.md](./CONTRIBUTING.md) for instructions on how to run the project.

- ? [Architecture Overview](./ARCHITECTURE.md)

## Credits

- Originally forked from [Cazzar/vscode-gradle](https://github.com/Cazzar/vscode-gradle)
- Inspired by the built-in [npm extension](https://github.com/microsoft/vscode/tree/master/extensions/npm)
- Thanks to all who have submitted bug reports and feedback ?

## Release Notes

See [CHANGELOG.md](./CHANGELOG.md).

## License

See [LICENSE.md](./LICENSE.md).
');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (18, 130, '2020-05-02 18:23:37', 'Update CHANGELOG.md', 59, '# vscode-spell-checker

[![Build Status](https://travis-ci.org/streetsidesoftware/vscode-spell-checker.svg?branch=master)](https://travis-ci.org/streetsidesoftware/vscode-spell-checker)
[![](https://vsmarketplacebadge.apphb.com/version-short/streetsidesoftware.code-spell-checker.svg)](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
[![](https://vsmarketplacebadge.apphb.com/installs-short/streetsidesoftware.code-spell-checker.svg)](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
[![](https://vsmarketplacebadge.apphb.com/rating-short/streetsidesoftware.code-spell-checker.svg)](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)

A simple source code spell checker for multiple programming languages.

For the readme on the plugin: [README](./packages/client/README.md).

## Contributions

## Building the extension

1. `npm install`
1. Launch vscode: `code Spell Checker.code-workspace`
1. Run the extension from vscode:
   1. `Debug Tab`
   1. Choose `Launch Extension` configuration.
   1. `F5`

<sup>*</sup> Requires Node >= 10

### Debugging the Client

1. Launch vscode: `code packages/client`
1. Run the extension from vscode: `F5`

### Debugging the Server

1. Launch vscode for the server: `code packages/_server`
1. Launch the client as specified above.
1. Attach to the server: `F5` or `Debug -> Attach Server`

Sometimes the ports get stuck. You can see if they are being used with:

```bash
netstat -anp tcp | grep 60048
```

Use the following command to find the process id (PID):

```bash
lsof -i tcp:60048
```

If anything shows up, then the port is still locked.

## Packages

* `client` - the actual extension running in VS Code.
* `_server` - the extension server that processes spell checker requests
* `_settingsViewer` - a webapp that provides a visual interface to the configuration files.
* `_integrationTests` - a test suite that launches the extension in VS Code.

## Dictionaries / Word List

Improvements to existing word lists and new word lists are welcome.

All dictionaries are being migrated to [cspell-dicts](https://github.com/Jason3S/cspell-dicts).
Some dictionaries are still located at [cspell](https://github.com/Jason3S/cspell)/[dictionaries](https://github.com/streetsidesoftware/cspell/tree/master/packages/cspell-lib/dictionaries).

### Format for Dictionary Word lists

The simplest format is one word per line.

```text
apple
banana
orange
grape
pineapple
```

For programming languages, a word list might look like this:

```php
ZipArchive::addGlob
ZipArchive::addPattern
ZipArchive::close
ZipArchive::deleteIndex
ZipArchive::deleteName
ZipArchive::extractTo
```

The word list complier will convert camelCase and snake_case words into a simple word list.
This is both for speed and predictability.

```php
ZipArchive::deleteIndex
```

becomes:

```text
zip
archive
delete
index
```

Spaces between words in the word list have a special meaning.

```text
New Delhi
New York
Los Angeles
```

becomes in the compiled dictionary:

```text
new delhi
new
delhi
new york
york
los angeles
los
angeles
```

Spaces in the compiled dictionary have a special meaning.
They tell the suggestion algorithm to suggest: ''newYork'', ''new_york'', ''new york'', etc. for ''newyork''.

### Locals

The default language is English: `"cSpell.language": "en"`

cSpell currently has English locals: `en-US` and `en-GB`.

Example words differences: behaviour (en-GB) vs behavior (en-US)

<!---
    cSpell:ignore newyork lsof netstat
    cSpell:words behaviour behavior
-->
');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (19, 0, '2020-04-30 23:15:40', 'Update CHANGELOG.md fix typo', 0, '# Yet Another Syntax Highlighter

This is yet an another syntax highlighter for lex/yacc and flex/bison.

## Features

This extension provides full syntax highlight for these languages and also for the embedded language C/C++.

This extension also supports some basic language features such as:
- Code diagnostic 
- Auto-completion
- Hover feature
- Go to Definition
- Find references
- Rename Symbol

## Preview

### Completion for lex
![](images/lex_define.gif)
  
### Completion for yacc
![](images/yacc_symbol.gif)

### Diagnostic

![](images/redefinition.png)

### More examples

You can find more previews here [previews](images/README.md).

## Notice

Since 1.43.0 VSCode enabled a new feature called Semantic Highlighting, this extension supports it. 

By default, only the built-in themes has semantic highlighting enabled, so if you are using a 3rd party theme that doesn''t support the semantic coloring yet, you have to add these lines to your `settings.json` file to have the feature enabled. 
```json
"editor.tokenColorCustomizations": {
	"[name of the theme]": {
		"semanticHighlighting": true
	}
}
```
For extra information see https://github.com/microsoft/vscode/wiki/Semantic-Highlighting-Overview.

Here is the comparison with and without semantic highlighting. 

On left enabled, on right disabled

![](images/semantic_comparison.png)

## Requirements

VSCode ^1.44

## Contributors

- [@babyraging](https://github.com/babyraging) - creator
- [@sceriffo](https://github.com/Sceriffo) - creator ');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (20, 0, '2020-04-30 23:15:40', 'Update CHANGELOG.md fix typo', 0, '# Yet Another Syntax Highlighter

This is yet an another syntax highlighter for lex/yacc and flex/bison.

## Features

This extension provides full syntax highlight for these languages and also for the embedded language C/C++.

This extension also supports some basic language features such as:
- Code diagnostic 
- Auto-completion
- Hover feature
- Go to Definition
- Find references
- Rename Symbol

## Preview

### Completion for lex
![](images/lex_define.gif)
  
### Completion for yacc
![](images/yacc_symbol.gif)

### Diagnostic

![](images/redefinition.png)

### More examples

You can find more previews here [previews](images/README.md).

## Notice

Since 1.43.0 VSCode enabled a new feature called Semantic Highlighting, this extension supports it. 

By default, only the built-in themes has semantic highlighting enabled, so if you are using a 3rd party theme that doesn''t support the semantic coloring yet, you have to add these lines to your `settings.json` file to have the feature enabled. 
```json
"editor.tokenColorCustomizations": {
	"[name of the theme]": {
		"semanticHighlighting": true
	}
}
```
For extra information see https://github.com/microsoft/vscode/wiki/Semantic-Highlighting-Overview.

Here is the comparison with and without semantic highlighting. 

On left enabled, on right disabled

![](images/semantic_comparison.png)

## Requirements

VSCode ^1.44

## Contributors

- [@babyraging](https://github.com/babyraging) - creator
- [@sceriffo](https://github.com/Sceriffo) - creator ');
INSERT INTO addonis.github_data (github_data_id, issues, lastCommit, lastCommitTitle, pulls, readme) VALUES (21, 0, '2020-04-30 23:15:40', 'Update CHANGELOG.md fix typo', 0, '# Yet Another Syntax Highlighter

This is yet an another syntax highlighter for lex/yacc and flex/bison.

## Features

This extension provides full syntax highlight for these languages and also for the embedded language C/C++.

This extension also supports some basic language features such as:
- Code diagnostic 
- Auto-completion
- Hover feature
- Go to Definition
- Find references
- Rename Symbol

## Preview

### Completion for lex
![](images/lex_define.gif)
  
### Completion for yacc
![](images/yacc_symbol.gif)

### Diagnostic

![](images/redefinition.png)

### More examples

You can find more previews here [previews](images/README.md).

## Notice

Since 1.43.0 VSCode enabled a new feature called Semantic Highlighting, this extension supports it. 

By default, only the built-in themes has semantic highlighting enabled, so if you are using a 3rd party theme that doesn''t support the semantic coloring yet, you have to add these lines to your `settings.json` file to have the feature enabled. 
```json
"editor.tokenColorCustomizations": {
	"[name of the theme]": {
		"semanticHighlighting": true
	}
}
```
For extra information see https://github.com/microsoft/vscode/wiki/Semantic-Highlighting-Overview.

Here is the comparison with and without semantic highlighting. 

On left enabled, on right disabled

![](images/semantic_comparison.png)

## Requirements

VSCode ^1.44

## Contributors

- [@babyraging](https://github.com/babyraging) - creator
- [@sceriffo](https://github.com/Sceriffo) - creator ');